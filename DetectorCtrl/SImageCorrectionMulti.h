/*/------------------------------------------------------------------------
 CSImageCorrectionMulti Class 사용법

#1. Correction 기능

//------------------------------------------------------------------------
// 선언부 ( ~.h )
	CSImageCorrectionMulti m_cCorrection;

//------------------------------------------------------------------------
// 구현부 ( ~.cpp )

 *. 접근 가능한 함수 리스트

	InitCorrectionImage()	: Class 초기화
	AddCorrectionImage()	: Multi Image Correction Set 적용시만 사용 (Air, Off Image Set이 여러장일 경우)
	FlatFieldCorrection()	: Flat Field Correction (DeadPixelCorrection 함수 포함되어 있음)
	DeadPixelCorrection()	: Dead Line 및 Dead Pixel Correction

	// 기포율 측정(for Polaris-D80[제주대학교 기포율측정 장비])
	SetVoidFraction()				: Void Fraction 관련 초기화( InitCorrectionImage 먼저 수행해야함 )
	GetVoidFraction_Mean()			: Void Fraction 측정 (논문 방법 사용)
	GetVoidFraction_Individual()	: Void Fraction 측정 (수정된 방법 사용)


 1. 초기화

	A. 클래스 초기화

		m_cCorrection.InitCorrectionImage(short *pAir, short *pOff, int nWidth, int nHeight, short *pDefectMap = NULL);
			pAir : 원본 Air 영상
			pOff : 원본 Off 영상
			nWidth, nHeight : 영상 가로, 세로 크기
			pDefectMap : DefectMap 설정 (없을경우 전달인자 생략 또는 NULL값 넣어줌)
			return BOOL : 초기화 성공여부 반환

	B. Multi Image Set 설정 ( Air, Off 영상이 2set 이상일 경우 사용함 )

		* 주의사항 : 가장 밝은 영상을 초기화함수(InitCorrectionImage)에서 넣어주고 나머지 영상은 아래 함수(AddCorrectionImage) 통해서 넣어줌
		for (int k=0; k<nSize-1; k++)
		{
 			m_cCorrection.AddCorrectionImage(pAir[k], pOff[k], nWidth, nHeight);
 		}
		* 전달인자 및 리턴값은 InitCorrectionImage 와 동일
		* Air, Off 영상이 1set만 존재할 경우 A루틴만 수행하면 됨 (B루틴은 생략 가능)

	*C. 기포율 측정(for Polaris-D80[제주대학교 기포율측정 장비])

		m_cCorrection.SetVoidFraction(short *pFullAir, short *pFullWater, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);
			pFullAir : 배관에 공기 가득 채운 후 얻은 영상 (Reference 영상이기 때문에 Average 영상 사용, 10장 정도)
			pFullWater : 배관에 물 가득 채운 후 얻은 영상 (Reference 영상이기 때문에 Average 영상 사용, 10장 정도)
			nWidth, nHeight : 영상 가로, 세로 크기
			bIsCorrected : 전달해준 영상이 Correction된 영상인지 여부 (Correction된 영상일 경우 전달인자 생략 또는 TRUE)
			return void;


 2. 사용

	A. Flat Field Correction ( Dead Line 및 Dead Pixel Correction 포함되어 있음 )

		m_cCorrection.FlatFieldCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight, short *pImageOff = NULL);
			pImageSrc : 원본 영상
			pImageDst : Correction 된 영상 저장 (원본영상에 덮어쓸 경우 NULL 값 넣어줌)
			nWidth, nHeight : 영상 가로, 세로 크기
			pImageOff : 원본 영상을 얻기 바로전에 얻은 Off 영상이 있을경우 사용 (없을경우 전달인자 생략 또는 NULL값 넣어줌)
					-> Xmaru Detector와 같이 시간에 따라 Off Level이 변화 할 경우 사용

	B. Dead Line 및 Dead Pixel Correction

		m_cCorrection.DeadPixelCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight);
			pImageSrc : 원본 영상
			pImageDst : Correction 된 영상 저장 (원본영상에 덮어쓸 경우 NULL 값 넣어줌)
			nWidth, nHeight : 영상 가로, 세로 크기

	*C. 기포율 측정(for Polaris-D80[제주대학교 기포율측정 장비])
		
		m_cCorrection.GetVoidFraction_Mean(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);
			pSrc : 기포율 측정할 영상
			nWidth, nHeight : 영상 가로, 세로 크기
			bIsCorrected : 전달해준 영상이 Correction된 영상인지 여부 (Correction된 영상일 경우 전달인자 생략 또는 TRUE)
			* return double : 기포율 반환

		m_cCorrection.GetVoidFraction_Individual(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);
			위 함수와 동일 (두가지 함수 결과 비교 후 실제 기포율에 가까운 함수 사용)

//------------------------------------------------------------------------*/



#pragma once

#include "SMemUtil.h"

#define DEFAULT_BADPIXEL_THRESHOLD	0.2


#define AVERAGE_WINDOW_SIZE			30
#define MEDIAN_WINDOW_SIZE			7
#define MEDIAN_HISTOGRAM_MAX		16384


#ifndef _AFXDLL
	typedef unsigned long long		UINT64;
	typedef int						BOOL;
	#define FALSE	0
	#define TRUE	1
	typedef struct tagRECT
				{ 
					int left;
					int top;
					int right;
					int bottom;
				} RECT;
#endif // _AFXDLL


#include <vector>
using namespace std;

typedef struct _tagCorrectionImageSet
{
	int nWidth, nHeight;
	short *pAir, *pOff, *pDiff;
	double *pdRate;
	double dTotalMean;
	double dDarkMean;

	BOOL bIsValidSet;

	_tagCorrectionImageSet(short *pAirImg, short *pOffImg, double *pdRateImg, int nW, int nH)
	{
		bIsValidSet = FALSE;
		dTotalMean = 0;
		dDarkMean = 0;
		pAir = NULL;
		pOff = NULL;
		pDiff = NULL;
		pdRate = NULL;

		nWidth = nW;
		nHeight = nH;

		if (nWidth>0 && nHeight>0 && pAirImg!=NULL && pOffImg!=NULL)
		{
			pAir = new short[nWidth*nHeight];
			pOff = new short[nWidth*nHeight];
			pDiff = new short[nWidth*nHeight];
			pdRate = new double[nWidth*nHeight];

			memcpy(pAir, pAirImg, sizeof(short)*nWidth*nHeight);
			memcpy(pOff, pOffImg, sizeof(short)*nWidth*nHeight);
			memcpy(pdRate, pdRateImg, sizeof(double)*nWidth*nHeight);

			for (int k=0; k<nWidth*nHeight; k++)
			{
				pDiff[k] = pAir[k] - pOff[k];
				dTotalMean += (pAir[k]-pOff[k])/(double)(nWidth*nHeight);
				dDarkMean += pOff[k]/(double)(nWidth*nHeight);
				//dTotalMean += pAir[k]/(double)(nWidth*nHeight);
			}

			bIsValidSet = TRUE;

			//TRACE("Create Correction Image Set : 0x%x\n", pAir);
		}
	}

	~_tagCorrectionImageSet()
	{
		//TRACE("Delete Correction Image Set : 0x%x\n", pAir);

		memDelete(pAir);
		memDelete(pOff);
		memDelete(pDiff);
		memDelete(pdRate);

		bIsValidSet = FALSE;
	}

} CorrectionImageSet, *pCorrectionImageSet;

typedef struct _tagDeadPixelPoint
{
	int nIdxCur;
	int nIdxL;
	int nIdxR;
	float fRateL;
	float fRateR;
} DeadPixelPoint;

typedef struct _tagVoidFractionCircleInfo
{
	double dCenterX;
	double dr2;
	double dTotArea;
} VoidFractionCircleInfo;

class CSImageCorrectionMulti
{
public:
	CSImageCorrectionMulti(void);
	~CSImageCorrectionMulti(void);

	// Initialize
	BOOL InitCorrectionImage(short *pAir, short *pOff, int nWidth, int nHeight, short *pDefectMap = NULL);
	// Add Image Set (Air, Off)
	BOOL AddCorrectionImage(short *pAir, short *pOff, int nWidth, int nHeight);

	// Correction Function
	void DeadPixelCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight);
	void FlatFieldCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight, short *pImageOff = NULL);

	// Additional Function
	void AddDeadPixelPosition(int nX1, int nY1, int nX2, int nY2);
	void ChangeBadPixelThreshold(double dBadPixelThreshold);

	// Void Fraction (for Polaris-D80[제주대학교 기포율측정 장비])
	void SetVoidFraction(short *pFullAir, short *pFullWater, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);
	double GetVoidFraction_Mean(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);
	double GetVoidFraction_Individual(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected = TRUE);

private:

//	BOOL m_bValidCorrection;
//	BOOL m_bDoingCorrection;
	int m_nWidth, m_nHeight;
	vector<pCorrectionImageSet> m_vecCorImgSet;

	BOOL *m_pbDeadPixelMap;
	double m_dBadPixelThreshold;
	vector<DeadPixelPoint> m_vecDeadPixelList;

	void ClearCorrectionImageSet();

	// Apply Correction Image Set (Finish - Add Image Set)
	void ApplyCorrectionImageSet();

	void CalculatePixelRate(short *pAir, short *pOff, int nWidth, int nHeight, double *pdRate);
	BOOL CreateDeadPixelMap(short *pAir, short *pOff, int nWidth, int nHeight, BOOL *bDeadPixelMap);

	inline double GetValidDistance(BOOL *pImageDeadPxl, int nCurX, int nCurY, int nDir, int nWidth, int nHeight, int *nStart, int *nEnd, double &dDistMin, double &dDistLeft);
	void GetValidNeighborIndex(BOOL *pbDeadPixelMap, int nWidth, int nHeight, int nCurIdx, int &nIdxL, int &nIdxR, float &fRateL, float &fRateR);

	void GetIntegrationImage(short **pSrc, UINT64 **pDst, int nWidth, int nHeight);
	void GetAverageImage(short *pDst, int nWidth, int nHeight, int nWSize);
	static double GetAverage(short *pSrc, int nWidth, int nHeight);

	inline static void CheckImageValueRange(short *pSrc, int nPixelSize);

	template<class AnyType>
	inline static AnyType FindMedian(AnyType *pSrc, const int nSize);
	template<class AnyType>
	static void MedianFilter(AnyType *pImgSrc1d, AnyType *pImgDst1d, int nWidth, int nHeight, int nKernelSize = 3, RECT *rectRoi = NULL);

	template<class AnyType>
	static void MedianFilterFast(AnyType *pImgSrc1d, AnyType *pImgDst1d, int nWidth, int nHeight, int nKernelSize = 3, RECT *rectRoi = NULL);

	static void MM_Dilation(BOOL *pImgSrc1d, BOOL *pImgDst1d, int nWidth, int nHeight, const int nKernelSize = 3, RECT *rectRoi = NULL);
	static void MM_Erosion(BOOL *pImgSrc1d, BOOL *pImgDst1d, int nWidth, int nHeight,	const int nKernelSize = 3, RECT *rectRoi = NULL);


	// Void Fraction (for Polaris-D80[제주대학교 기포율측정 장비])
	double *m_pdVoidFractionCircleRate;
	BOOL *m_pbVoidFractionRoi;
	VoidFractionCircleInfo *m_pVoidFractionCircleInfo;

	double *m_pdFullWater;
	double *m_pdFullAir;
	RECT m_rectRoi;

	void GetROI_VoidFraction(short *pFullAir, short *pFullWater, int nWidth, int nHeight, RECT &rectROI);
};

inline void CSImageCorrectionMulti::CheckImageValueRange(short *pSrc, int nPixelSize)
{
	for (int k=0; k<nPixelSize; k++)
	{
		if (pSrc[k]<0)
		{
			pSrc[k] = 0;
		}
		else if (pSrc[k]>=MEDIAN_HISTOGRAM_MAX)
		{
			pSrc[k] = MEDIAN_HISTOGRAM_MAX-1;
		}
	}
}

template<class AnyType>
inline AnyType CSImageCorrectionMulti::FindMedian(AnyType *pSrc, const int nSize)
{
	for (int x=0; x<=nSize/2; x++)
	{
		for (int y=x+1; y<nSize; y++)
		{
			if (pSrc[x]>pSrc[y])
			{
				AnyType nTemp = pSrc[x];
				pSrc[x] = pSrc[y];
				pSrc[y] = nTemp;
			}
		}
	}

	return pSrc[nSize/2];
}

template<class AnyType>
void CSImageCorrectionMulti::MedianFilter(AnyType *pImgSrc1d, AnyType *pImgDst1d, int nWidth, int nHeight,
	const int nKernelSize /*=3*/, RECT *rectRoi /*=NULL*/)
{
	BOOL bSrcCopyFlag = FALSE;
	if (pImgSrc1d == pImgDst1d)
	{
		bSrcCopyFlag = TRUE;
		AnyType *pImgSrc1dTemp = pImgSrc1d;

		pImgSrc1d = new AnyType[nWidth*nHeight];
		memcpy(pImgSrc1d, pImgSrc1dTemp, sizeof(AnyType)*nWidth*nHeight);
	}

	AnyType **pImgSrc = Get2DPtr(pImgSrc1d, nWidth, nHeight);
	AnyType **pImgDst = Get2DPtr(pImgDst1d, nWidth, nHeight);


	const int nSize = nKernelSize*nKernelSize;
	const int nKSize2 = nKernelSize/2;
	int x,y,x2, y2;

	//------------------------------------------------------------------------
	// ROI 설정
	int nL, nR, nT, nB;
	if (rectRoi != NULL)
	{
		nL = min(rectRoi->left, rectRoi->right);
		nR = max(rectRoi->left, rectRoi->right);
		nT = min(rectRoi->top, rectRoi->bottom);
		nB = max(rectRoi->top, rectRoi->bottom);
	}
	else
	{
		nL = 0;
		nR = nWidth;
		nT = 0;
		nB = nHeight;
	}

	AnyType *pSrc = new AnyType[nSize];

	// 세로 : 왼쪽 끝 예외
	if (nL-nKSize2 < 0)
	{
		for (y=nT; y<nB; y++)
			for (x=nL; x<nKSize2; x++)
			{
				int k=0;
				for (y2=-nKSize2; y2<=nKSize2; y2++)
					for (x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
					pImgDst[y][x] = FindMedian(pSrc, k);
			}

			nL = nKSize2;
	}

	// 세로 : 오른쪽 끝 예외
	if (nR+nKSize2 > nWidth)
	{
		for (y=nT; y<nB; y++)
			for (x=nWidth-nKSize2; x<nR; x++)
			{
				int k=0;
				for (y2=-nKSize2; y2<=nKSize2; y2++)
					for (x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
					pImgDst[y][x] = FindMedian(pSrc, k);
			}

			nR = nWidth-nKSize2;
	}

	// 세로 : 위쪽 끝 예외
	if (nT-nKSize2 < 0)
	{
		for (y=nT; y<nKSize2; y++)
			for (x=nL; x<nR; x++)
			{
				int k=0;
				for (y2=-nKSize2; y2<=nKSize2; y2++)
					for (x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
					pImgDst[y][x] = FindMedian(pSrc, k);
			}

			nT = nKSize2;
	}

	// 가로 : 아래쪽 끝 예외
	if (nB+nKSize2 > nHeight)
	{
		for (y=nHeight-nKSize2; y<nB; y++)
			for (x=nL; x<nR; x++)
			{
				int k=0;
				for (y2=-nKSize2; y2<=nKSize2; y2++)
					for (x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
					pImgDst[y][x] = FindMedian(pSrc, k);
			}

			nB = nHeight-nKSize2;
	}

	// Median filter
	//for (y=nT; y<nB; y++)
	parallel_for(nT, nB, [&](int y)
	{
		AnyType *pSrcLocal = new AnyType[nSize];
		for (int x=nL; x<nR; x++)
		{
			int k=0;
			for (int y2=-nKSize2; y2<=nKSize2; y2++)
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				pSrcLocal[k++] = pImgSrc[y+y2][x+x2];
			}

			//				pImgDst[y][x] = FindMedian(pSrcLocal, k);
			for (int x3=0; x3<k/2+1; x3++)
			for (int y3=x3+1; y3<k; y3++)
			{
				if (pSrcLocal[x3]>pSrcLocal[y3])
				{
					AnyType nTemp = pSrcLocal[x3];
					pSrcLocal[x3] = pSrcLocal[y3];
					pSrcLocal[y3] = nTemp;
				}
			}

			pImgDst[y][x] = pSrcLocal[k/2];
		}
		delete [] pSrcLocal;
	});

	delete [] pSrc;

	Delete2DPtr(pImgSrc);
	Delete2DPtr(pImgDst);

	if (bSrcCopyFlag)
	{
		delete [] pImgSrc1d;
	}
}


template<class AnyType>
void CSImageCorrectionMulti::MedianFilterFast(AnyType *pImgSrc1d, AnyType *pImgDst1d, int nWidth, int nHeight,
	const int nKernelSize /*=3*/, RECT *rectRoi /*=NULL*/)
{
	if (pImgSrc1d==NULL || pImgDst1d==NULL)
	{
		return;
	}

	BOOL bSrcCopyFlag = FALSE;
	if (pImgSrc1d == pImgDst1d)
	{
		bSrcCopyFlag = TRUE;
		AnyType *pImgSrc1dTemp = pImgSrc1d;

		pImgSrc1d = new AnyType[nWidth*nHeight];
		memcpy(pImgSrc1d, pImgSrc1dTemp, sizeof(AnyType)*nWidth*nHeight);
	}

	CheckImageValueRange(pImgSrc1d, nWidth*nHeight);

	AnyType **pImgSrc = Get2DPtr(pImgSrc1d, nWidth, nHeight);
	AnyType **pImgDst = Get2DPtr(pImgDst1d, nWidth, nHeight);

	const int nSize = nKernelSize*nKernelSize;
	const int nKSize2 = nKernelSize/2;

	//------------------------------------------------------------------------
	// ROI 설정
	int nL, nR, nT, nB;
	if (rectRoi != NULL)
	{
		nL = min(rectRoi->left, rectRoi->right);
		nR = max(rectRoi->left, rectRoi->right);
		nT = min(rectRoi->top, rectRoi->bottom);
		nB = max(rectRoi->top, rectRoi->bottom);
	}
	else
	{
		nL = 0;
		nR = nWidth;
		nT = 0;
		nB = nHeight;
	}

	// 	double dImageAverage = GetAverage(pImgSrc1d, nWidth, nHeight);
	// 	const BOOL bIsImageLevelLow = (dImageAverage < MEDIAN_HISTOGRAM_MAX/2);

	AnyType *pSrc = new AnyType[nSize];

	// 세로 : 왼쪽 끝 예외
	if (nL-nKSize2 < 0)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nL; x<nKSize2; x++)
			{
				int k=0;
				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
				}

				pImgDst[y][x] = FindMedian(pSrc, k);
			}
		}

		nL = nKSize2;
	}

	// 세로 : 오른쪽 끝 예외
	if (nR+nKSize2 > nWidth)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nWidth-nKSize2; x<nR; x++)
			{
				int k=0;
				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
				}

				pImgDst[y][x] = FindMedian(pSrc, k);
			}
		}

		nR = nWidth-nKSize2;
	}

	// 세로 : 위쪽 끝 예외
	if (nT-nKSize2 < 0)
	{
		for (int y=nT; y<nKSize2; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				int k=0;
				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
				}

				pImgDst[y][x] = FindMedian(pSrc, k);
			}
		}

		nT = nKSize2;
	}

	// 가로 : 아래쪽 끝 예외
	if (nB+nKSize2 > nHeight)
	{
		for (int y=nHeight-nKSize2; y<nB; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				int k=0;
				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
							pSrc[k++] = pImgSrc[y+y2][x+x2];
					}
				}

				pImgDst[y][x] = FindMedian(pSrc, k);
			}
		}

		nB = nHeight-nKSize2;
	}

	// Median filter
	const int nMedCnt = (nKernelSize*nKernelSize)/2+1;
	//parallel_for(nL, nR, [&](int x){
	for (int x=nL; x<nR; x++)
	{
		int pHist[MEDIAN_HISTOGRAM_MAX] = {0,};
		//double dImageAverage = 0;
		int nSumCnt = 0;
		int nMaxIndex = 0;

		// First Line
		int y = nT;
		for (int y2=-nKSize2; y2<=nKSize2; y2++)
		{
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				++pHist[pImgSrc[y+y2][x+x2]];
				if (nMaxIndex<pImgSrc[y+y2][x+x2])
				{
					nMaxIndex = pImgSrc[y+y2][x+x2];
				}
			}
		}

		for(int idx1=nMaxIndex; idx1>=0; idx1--)
		{
			nSumCnt += pHist[idx1];
			if (nSumCnt>=nMedCnt)
			{
				pImgDst[y][x] = idx1;
				break;
			}
		}

		//
		for (y=nT+1; y<nB; y++)
		{
			int y2=0;
			nMaxIndex = 0;

			y2 = y-nKSize2-1;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				--pHist[pImgSrc[y2][x+x2]];
			}
			y2 = y+nKSize2;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
// 				if ((pImgSrc[y2][x+x2]<0) || (pImgSrc[y2][x+x2]>=MEDIAN_HISTOGRAM_MAX))
// 				{
// 					TRACE(L"Index Error !!, %d\n", pImgSrc[y2][x+x2]);
// 				}

				++pHist[pImgSrc[y2][x+x2]];
			}

			for (int y3=y-nKSize2; y3<= y+nKSize2; y3++)
			{
				for (int x3=x-nKSize2; x3<= x+nKSize2; x3++)
				{
					if (nMaxIndex<pImgSrc[y3][x3])
					{
						nMaxIndex = pImgSrc[y3][x3];
					}
				}
			}

			nSumCnt = 0;

			for(int idx2=nMaxIndex; idx2>=0; idx2--)
			{
				if (pHist[idx2])
				{
					nSumCnt += pHist[idx2];
					if (nSumCnt>=nMedCnt)
					{
						pImgDst[y][x] = idx2;
						break;
					}
				}
			}
		}
	}
	//});


	delete [] pSrc;

	Delete2DPtr(pImgSrc);
	Delete2DPtr(pImgDst);

	if (bSrcCopyFlag)
	{
		delete [] pImgSrc1d;
	}
}