// 7942CKCamera.cpp: implementation of the CVADAVLib class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "VADAVLib.h"
//#include "7942CKCamera.h"
#include <IO.h>

static CVADAVLib* _this = NULL;

#include <ppl.h>
#include <ppl.h>
using namespace Concurrency;
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//Imaq globals

template <typename T>
int FileWrite(CString FullPath, int FileLen, T* MemWrite, int TypeSize)
{
	FILE *fp;
	int errorno = 0;
	if (((fp = _wfopen(FullPath, L"wbc")) == NULL))
	{
		//AfxMessageBox();
		return 99999;
	}

	//결과 영상 파일로 기록
	int WriteCount = 0;
	WriteCount = fwrite(MemWrite, TypeSize, FileLen, fp);
	if (WriteCount < FileLen)
	{
		//	AfxMessageBox(L"File Write Error !!");
		fclose(fp);
		return 99997;
	}
	//  [7/25/2014 Administrator]
	//fflush 호출하도록 변경함.
	fflush(fp);
	fclose(fp);
	return 0;
}

int saveFileCount = 0;

void BufferToFile(short *buffer, int length, int sizeofType)
{
	CString fileName;

	fileName.Format(_T("C:\\DATA\\DATA_%02d"), saveFileCount);

	FileWrite(fileName, length, buffer, sizeofType);

	saveFileCount++;
}

UINT AcquiredImageChecker(LPVOID pParam)
{
	CVADAVLib *pThis = (CVADAVLib*)pParam;
	CString log;

	while (pThis->m_bRunAcquiredImageChecker == true)
	{
		::WaitForSingleObject(pThis->m_pEventWaitAcquireImage->m_hObject, INFINITE);

		if (pThis->m_bRunAcquiredImageChecker == true
			&& pThis->rVIMGACQ_ImageAcquired != NULL)
		{
			log.Format(_T("Call rVIMGACQ_ImageAcquired Proc - CurrentSnapIndex : %d, Width : %d Height : %d"), pThis->m_iCurSnap, pThis->m_nImgWidth, pThis->m_nImgHeight);

			pThis->rVLOG_WriteLogRequested(log);

			pThis->rVIMGACQ_ImageAcquired(pThis->m_ImgBuffer[pThis->m_iCurSnap], pThis->m_nImgWidth, pThis->m_nImgHeight, 2);

			BufferToFile(pThis->m_ImgBuffer[pThis->m_iCurSnap], pThis->m_nImgWidth * pThis->m_nImgHeight, 2);
		} // else
	}

	return 1;
}

CVADAVLib::CVADAVLib()
	:m_pMainWnd(NULL)
	, m_AcqWinWidth(-1)
	, m_AcqWinHeight(-1)
	, m_ImageBuffer(NULL)
	, m_ExposureTime(34)
	, m_bInit(false)
	, Handle(NULL)
	, m_piLookupTable(NULL)
	, m_iWell(-1)
{
	m_nAcqState = 0;

	rACQ_CallBackRec = NULL;

	_this = this;

	m_iCurSnap = -1;

	m_FrameBuffer = NULL;

	for (int i = 0; i<IMG_BUFFER_NUM; i++)
	{
		m_ImgBuffer[i] = NULL;
	}

	m_bOldBinningFlag = m_bBinningFlag = false;

	m_pEventWaitAcquireImage = new CEvent();
	m_bRunAcquiredImageChecker = true;
	CWinThread *p_Thread = ::AfxBeginThread(AcquiredImageChecker, this, THREAD_PRIORITY_NORMAL);
}

CVADAVLib::~CVADAVLib()
{
	CameraClose();

	if (m_FrameBuffer != NULL)
		delete m_FrameBuffer;

	for (int i = 0; i<IMG_BUFFER_NUM; i++)
	{
		if (m_ImgBuffer[i] != NULL)
			delete m_ImgBuffer[i];
	}

	m_bRunAcquiredImageChecker = false;

	if (m_pEventWaitAcquireImage != NULL)
	{
		m_pEventWaitAcquireImage->SetEvent();

		delete m_pEventWaitAcquireImage;

		m_pEventWaitAcquireImage = NULL;
	} // else
}

void CVADAVLib::GetDetectorInfo(char* strFirmWare, char* strR1124, char* strR1125, char* strR1126)
{
	memset(strFirmWare, 0, sizeof(char) * 20);
	memset(strR1124, 0, sizeof(char) * 20);
	memset(strR1125, 0, sizeof(char) * 20);
	memset(strR1126, 0, sizeof(char) * 20);

	sprintf(strFirmWare, "Xmaru-0707_CF");
}

static void _stdcall iCallBackProc_Acquisition(tVDACQ_CallBackRec* AR)
{
	((CVADAVLib*)_this)->On_ACQ_CallBack(AR);
}

bool CVADAVLib::CameraInit(CWnd* pMainWnd, int iWell)
{
	//if(!pMainWnd)
	//	return false;

	//m_pMainWnd = pMainWnd;
	int iExposureTime = 200, iBinning = 1, iWidth, iHigh;
	m_bInit = false;

	//-------------------------------------------------------------------------
	// 32bit MultiByte Dll
#ifdef WIN32MULTIBYTE
	TCHAR strText[] = _T("\Detector\\Win32\\Multibyte\\VADAV_FGM.dll");
#endif
	// 32bit Unicode Dll
#ifdef WIN32UNICODE
	TCHAR strText[] = _T("\Detector\\Win32\\Unicode\\VADAV_FGM.dll");
#endif
	// 64bit Unicode Dll
#ifdef WIN64UNICODE
	TCHAR strText[] = _T("\Detector\\Win64\\VADAV_FGM_64_1215.dll");
#endif

	int	  l_iFind;
	TCHAR szpath[2000];
	GetModuleFileName(NULL, szpath, sizeof(szpath));
	CString strModuelPath;
	CString rootPath;

	strModuelPath.Format(_T("%s"), szpath);
	l_iFind = strModuelPath.ReverseFind(_T('\\'));
	rootPath.Format(_T("%s"), strModuelPath.Left(l_iFind));	// V1\Bin
	l_iFind = rootPath.ReverseFind(_T('\\'));
	rootPath.Format(_T("%s\\"), rootPath.Left(l_iFind));

	CString strVADAVPath = _T("");
	//strVADAVPath.Format(_T("%s\\%s"), rootPath, strText);
	strVADAVPath.Format(_T("%s\\%s"), _T("C:\\METEOR"), strText);

	if (!VADAV_MapDLL(strVADAVPath.GetBuffer(0), &m_Intf))
	{
		strVADAVPath.ReleaseBuffer();

		DWORD dw = ::GetLastError();

		CString log;
		log.Format(_T("X-ray detector library's load error : %d", dw));
		rVLOG_WriteLogRequested(log);

		return FALSE;
	}
	strVADAVPath.ReleaseBuffer();

	m_Intf.rVDACQ_GetFrameDim(&m_nFrameWidth, &m_nFrameHeight);

	m_iNormalFrameWidth = m_nFrameWidth;
	m_iNormalFrameHeight = m_nFrameHeight;
	//m_Intf.rVDC_GetImageDim( &m_nImgWidth, &m_nImgHeight );

	if (m_FrameBuffer == NULL)
		m_FrameBuffer = new short[m_nFrameWidth*m_nFrameHeight];

	memset(m_FrameBuffer, 0, sizeof(short) * m_nFrameWidth * m_nFrameHeight);

	tVDACQ_FGRec  qFGRec;

	m_Intf.rVDACQ_GetFGRec(&qFGRec); // 라이브러리의 framegrabber 설정정보를 얻어옴. 

	if (qFGRec.rType <= cVDACQ_FG_None) // framegrabber가 설정되어 있지 않았을 때..
	{
		return false;
	}
	m_bInit = true;
	//-------------------------------------------------------------------------

	//-------------------------------------------------------------------------
	if (rACQ_CallBackRec)
		CameraClose();

	int iFrameWidth, iFrameHeight;
	m_Intf.rVDACQ_GetFrameDim(&iFrameWidth, &iFrameHeight);

	if (m_bOldBinningFlag != m_bBinningFlag)
	{
		m_bOldBinningFlag = m_bBinningFlag;
		if (m_bBinningFlag == true)
		{
			m_nFrameWidth = iFrameWidth / 2;
			m_nFrameHeight = iFrameHeight / 2;
			m_Intf.rVDACQ_SetFrameDim(m_nFrameWidth, m_nFrameHeight);
		}
		else
		{
			m_Intf.rVDACQ_SetFrameDim(m_iNormalFrameWidth, m_iNormalFrameHeight);
		}
	}
	m_Intf.rVDACQ_GetFrameDim(&m_nFrameWidth, &m_nFrameHeight);
	// ko : 이미지 가로 세로 바뀜
	//m_Intf.rVDC_GetImageDim( &m_nImgWidth, &m_nImgHeight);
	m_nImgWidth = m_nFrameWidth;
	m_nImgHeight = m_nFrameHeight;

	SetSize(m_nImgWidth, m_nImgHeight);

	Sleep(10);
	//rACQ_CallBackRec = m_Intf.rVDACQ_Connect( cVDACQ_FBright,
	//	iCallBackProc_Acquisition, this, m_FrameBuffer, 0 );

	//-------------------------------------------------------------------------

	if (!CameraSet(iExposureTime, iBinning, m_nFrameWidth, m_nFrameHeight, iWell, true))
	{
		CameraClose();
		return false;
	}

	//GoLiveSeq(1, 1, 20, 1, 20, 1);
	m_bInit = true;

	return true;
}

void CVADAVLib::CoAverageImage(short *outImageBuffer, short **inImageBuffer, int inImageCount, int inBufferLength)
{
	parallel_for(0, inBufferLength, [&](int index2)
	{
		int sumValue = 0;
		for (int index1 = 1; index1 < inImageCount - 1; index1++)
		{
			sumValue += inImageBuffer[index1][index2];
		}

		outImageBuffer[index2] = sumValue / (inImageCount - 2);
	}
	);
}

template <typename T>
int FileRead(CString FullPath, T* LoadMem, int TypeSize)
{
	//파일 오픈 및 에러 체크
	FILE *fp;
	int errorno = 0;

	if ((errorno = _wfopen_s(&fp, FullPath, L"rb")))
	{
		//AfxMessageBox(L"Read File Open Error 00!");
		return 0;
	}

	fseek(fp, 0L, SEEK_END);
	long len = ftell(fp);

	fseek(fp, 0L, SEEK_SET);

	//if(LoadMem != NULL)
	//	delete []LoadMem;

	//LoadMem = new T[len];

	//파일 메모리에 적재 및 읽기 에러 체크
	int ReadCount = fread(LoadMem, TypeSize, len / TypeSize, fp);

	if (ReadCount < len / TypeSize)
	{
		TRACE(L"Read Count : %d \n", ReadCount);
		//AfxMessageBox(L"File Read Error!");
		return 0;
	}

	//파일을 메모리에 적재 후 닫는다.
	fclose(fp);

	return ReadCount;
}

template <typename T>
BOOL FileRead(CString FullPath, int widht, int height, T* LoadMem, int TypeSize)
{

	//파일 오픈 및 에러 체크
	FILE *fp;
	int errorno = 0;
	int FileLen = widht * height;
	if ((errorno = _wfopen_s(&fp, FullPath, L"rb")))
	{
		//AfxMessageBox(L"Read File Open Error 00!");
		return FALSE;
	}

	fseek(fp, 0L, SEEK_END);
	long len = ftell(fp);///sizeof(short) - WIDTH*HEIGHT;
	fseek(fp, len - FileLen * TypeSize, SEEK_SET);

	//파일 메모리에 적재 및 읽기 에러 체크
	int ReadCount = fread(LoadMem, TypeSize, FileLen, fp);

	if (ReadCount < FileLen)
	{
		TRACE(L"Read Count : %d \n", ReadCount);
		//AfxMessageBox(L"File Read Error 00!");

		fclose(fp);

		return FALSE;
	}
	//파일을 메모리에 적재 후 닫는다.
	fclose(fp);

	return TRUE;
}

void CVADAVLib::SaveCalibrationData(CString filePath, short *buffer, int length, int sizeofType)
{
	FileWrite(filePath, length, buffer, sizeofType);
}

void CVADAVLib::CorrectionImage(short *outImageBuffer, short *inImageBuffer, int width, int height)
{
	// TODO - KimGilSu - Correct 데이터를 사전에 생성 해놓음으로써 속도 향상 가능(?)

	CString airPath = L"C:\\METEOR\\CAL\\airraw_2";
	CString offPath = L"C:\\METEOR\\CAL\\offset_2";

	int inBufferLength = width * height;

	short* offset = new short[inBufferLength];
	short* airraw = new short[inBufferLength];

	if (FileRead(airPath, airraw, sizeof(short)) <= 0)
	{
		for (int k = 0; k<inBufferLength; k++)
		{
			airraw[k] = 10000;
		}
	}

	if (FileRead(offPath, offset, sizeof(short)) <= 0)
	{
		memset(offset, 0, sizeof(short)*inBufferLength);
	}

	m_correctionMulti.InitCorrectionImage(airraw, offset, width, height);
	m_correctionMulti.FlatFieldCorrection(inImageBuffer, outImageBuffer, width, height, NULL);

	delete[]offset;
	delete[]airraw;
}

void CVADAVLib::EnhanceImage(short *outImageBuffer, short *inImageBuffer, int width, int height)
{
	CMyDIP dip;

	short *t_inv = new short[width * height];

	dip.HybridMedianFilterFast(inImageBuffer, width, height);

	memcpy(t_inv, inImageBuffer, sizeof(short) * width * height);

	dip.UnsharpMask(t_inv, inImageBuffer, width, height);

	delete[]t_inv;

	int cuttingValue = CUTTING_HEIGHT;
	int cutLen = width * (height - cuttingValue);

	short* cutImg = new short[cutLen];

	memset(cutImg, 0x00, sizeof(short)*cutLen);

	int strPos = cuttingValue * width;

	for (int x0 = 0, x1 = strPos; x0 < cutLen; x0++, x1++)
	{
		cutImg[x0] = inImageBuffer[x1];
	}

	memset(inImageBuffer, 0x00, sizeof(short) * width * height);

	memcpy(inImageBuffer, cutImg, sizeof(short) * cutLen);

	int tmpMinVal = SHRT_MAX, tmpMaxVal = SHRT_MIN, tmpAbcVal = 0;
	int reWidth = width;
	int reHeight = height - cuttingValue;

	dip.findMinMaxValue(inImageBuffer, tmpMinVal, tmpMaxVal, reWidth, reHeight, tmpAbcVal);

	memcpy(outImageBuffer, inImageBuffer, sizeof(short) * cutLen);

	delete[]cutImg;
}

void CVADAVLib::DSAMaskImage(short *outImageBuffer, short *inImageBuffer, int width, int height)
{
	int dsa_mask_cnt = 0;	//mask 영상은 20장 average 함, 카운트 
	short* dsa_img = new short[width*height];
	short* dsa_mask = new short[width*height];
	int* dsa_mask_plus = new int[width*height];
	memset(dsa_img, 0x00, sizeof(short)*width*height);
	memset(dsa_mask, 0x00, sizeof(short)*width*height);
	memset(dsa_mask_plus, 0x00, sizeof(int)*width*height);

	if (!FileRead(L"C:\\METEOR\\Temporary Files\\0000.mask", width, height - CUTTING_HEIGHT, &dsa_mask[CUTTING_HEIGHT*width], sizeof(short)))
	{
		// TODO - KimGilSu - mask 생성해야 함을 알림.
	} // else

	int strPos = CUTTING_HEIGHT * width;
	int cutLen = width * (height - CUTTING_HEIGHT);
}

void CVADAVLib::GetImageSize(int &nWidth, int &nHeight)
{
	nWidth = m_nFrameWidth;
	nHeight = m_nFrameHeight;
}

void CVADAVLib::VADAV_SetImageAcquiredProc(tVIMGACQ_CallBackProc IMGAcqProc)
{
	rVIMGACQ_ImageAcquired = IMGAcqProc;
}

void CVADAVLib::VADAV_SetWriteLogRequestedProc(tVLOG_CallBackProc LoggerProc)
{
	rVLOG_WriteLogRequested = LoggerProc;
}

bool CVADAVLib::CallbackInit()
{
	rACQ_CallBackRec = m_Intf.rVDACQ_Connect(cVDACQ_FBright,
		iCallBackProc_Acquisition, this, m_FrameBuffer, 0);

	return true;
}

void CVADAVLib::SetSize(int ImageSizeX, int ImageSizeY)
{
	for (int i = 0; i<IMG_BUFFER_NUM; i++)
	{
		if (m_ImgBuffer[i] != NULL)
		{
			delete m_ImgBuffer[i];
			m_ImgBuffer[i] = NULL;
		}
	}

	for (int i = 0; i<IMG_BUFFER_NUM; i++)
	{
		//if(m_ImgBuffer[i] == NULL)
		m_ImgBuffer[i] = new short[ImageSizeX * ImageSizeY];
		memset(m_ImgBuffer[i], 0, sizeof(short) * ImageSizeX * ImageSizeY);
	}

	//2바이닝모드 적용
	//m_Intf.rVDACQ_SetFrameDim(m_nFrameWidth, m_nFrameHeight);

}

int m_nCount = -1;

void CVADAVLib::OnCaptureRecv()
{
	rVLOG_WriteLogRequested(_T("<-----------Captured Image Recieved----------->"));

	if (m_nCount == -1)
	{
		m_nCount = 0;
	}
	else
	{
		m_nCount = (m_nCount + 1) % IMG_BUFFER_NUM;
	}

	int iBufSize = m_nImgWidth * m_nImgHeight;

	memcpy(m_ImgBuffer[m_nCount], m_FrameBuffer, iBufSize * sizeof(short));
	
	m_iCurSnap = m_nCount;

	if (m_pEventWaitAcquireImage != NULL)
	{
		m_pEventWaitAcquireImage->SetEvent();
	} // else
}

void CVADAVLib::On_ACQ_CallBack(tVDACQ_CallBackRec* AR)
{
	if (m_nAcqState<0 || m_nAcqState>1)
		return;

	CString log;

	switch (AR->rType)
	{
		// ACBR->rType 이 라이브러리로부터 전달되어 옴.

		// errors

		// …..

		// trace messages

	case cVDACQ_ETTrace:

	case cVDACQ_ETTraceT:

		switch (AR->rEvent)
		{

			// ECaptureRecv with CaptureFrames>0 means new frame

			// ECaptureRecv with CapturePercent>100 and CaptureFrames=0 means 'average frame'

			// When FGC_NoAve is ON 'average frame' might contain (or not) internal debugging data

		case cVDACQ_ECaptureRecv:
		{
			OnCaptureRecv();

			break;
		};


		case cVDACQ_ECapturePerc:
			break;

		}; // switch Event

		break;

	}; // switch ACBR->rType
}

bool CVADAVLib::CameraClose()
{
	rVLOG_WriteLogRequested(_T("Camera Close"));

	if (rACQ_CallBackRec)
	{
		m_Intf.rVDACQ_Close(rACQ_CallBackRec);
		rACQ_CallBackRec = NULL;
	}
	return 1;

	Handle = NULL;
	return true;
}

bool CVADAVLib::CameraSet(int& ExposureTime, int iBinning, int& iWidth, int& iHigh, int iWell, bool bInit)
{
	if (!bInit)
		LiveStart(true);

	// ko : 이미지 크기 바뀜
	//iWidth = m_nImgWidth;
	//iHigh = m_nImgHeight;


	TCHAR m_sCmdStart[256] = { 0, }, m_sCmdStart2[256] = { 0 };
	TCHAR qBuf[512];
	if (iBinning == 1)
	{
		m_bBinningFlag = false;
	}
	else
	{
		m_bBinningFlag = true;

		_stprintf(m_sCmdStart, _T("[sps_higg][sps_bin2][sps_ext_][sps_poff]"));
	}

	m_Intf.rVDACQ_GetDetectorAddr(qBuf);
	if (!_tcsstr(qBuf, _T("COM")))
		_tcscpy(qBuf, _T("Library's com-port is not allowed"));
	else { // if detector's address is string "COMx:"
		m_Intf.rVDACQ_SetDetectorAddr(qBuf);
		_tcscpy(qBuf, _T("Requested to open library's com-port"));
	};

	BOOL ret = 0;

	if (m_sCmdStart[0]) // generally you will see command in the library's log
		ret = m_Intf.rVDACQ_SendCommandParam(
			NULL, // serial commands could be sent with ACallBackRec=NULL (don't need explicitly opened connection)
			0, // command code should be zero to indicate that it is not one of library's predefined command
			m_sCmdStart, // command'd data buffer 
			0);

	Sleep(50);

	// 	if (m_sCmdStart2[0]) // generally you will see command in the library's log
	// 		ret = m_Intf.rVDACQ_SendCommandParam(
	// 		NULL, // serial commands could be sent with ACallBackRec=NULL (don't need explicitly opened connection)
	// 		0, // command code should be zero to indicate that it is not one of library's predefined command
	// 		m_sCmdStart2, // command'd data buffer 
	// 		0 );
	// 
	// 	Sleep(50);


	m_Intf.rVDACQ_SetFrameDim(iWidth, iHigh);
	m_Intf.rVDC_GetImageDim(&m_nImgHeight, &m_nImgWidth);

	return true;
}

bool CVADAVLib::pOnCameraSet(int& fps, int iBinning, int& iWidth, int& iHigh, int iWell, bool bInit)
{
	if (!bInit)
		LiveStart(true);

	// ko : 이미지 크기 바뀜
	//iWidth = m_nImgWidth;
	//iHigh = m_nImgHeight;

	int ExposureTime = m_ExposureTime;

	TCHAR m_sCmdStart[256] = { 0, }, m_sCmdStart2[256] = { 0 };
	TCHAR qBuf[512];
	if (iBinning == 1)
	{
		m_bBinningFlag = false;
	}
	else
	{
		m_bBinningFlag = true;

		_stprintf(m_sCmdStart, _T("[sps_higg][sps_bin2][sps_ext_][sps_freq %04d][sps_pon_]"), fps);
	}

	m_Intf.rVDACQ_GetDetectorAddr(qBuf);
	if (!_tcsstr(qBuf, _T("COM")))
		_tcscpy(qBuf, _T("Library's com-port is not allowed"));
	else { // if detector's address is string "COMx:"
		m_Intf.rVDACQ_SetDetectorAddr(qBuf);
		_tcscpy(qBuf, _T("Requested to open library's com-port"));
	};

	BOOL ret = 0;

	if (m_sCmdStart[0]) // generally you will see command in the library's log
		ret = m_Intf.rVDACQ_SendCommandParam(
			NULL, // serial commands could be sent with ACallBackRec=NULL (don't need explicitly opened connection)
			0, // command code should be zero to indicate that it is not one of library's predefined command
			m_sCmdStart, // command'd data buffer 
			0);

	Sleep(50);

	// 	if (m_sCmdStart2[0]) // generally you will see command in the library's log
	// 		ret = m_Intf.rVDACQ_SendCommandParam(
	// 		NULL, // serial commands could be sent with ACallBackRec=NULL (don't need explicitly opened connection)
	// 		0, // command code should be zero to indicate that it is not one of library's predefined command
	// 		m_sCmdStart2, // command'd data buffer 
	// 		0 );
	// 
	// 	Sleep(50);


	m_Intf.rVDACQ_SetFrameDim(iWidth, iHigh);
	m_Intf.rVDC_GetImageDim(&m_nImgHeight, &m_nImgWidth);

	return true;
}

bool CVADAVLib::CameraSnap(int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE pImage)
{
	int intRetCode = 0;

	//	intRetCode= Snap(1, 1, 1);
	//	intRetCode =(int) WaitForSingleObject(Handle, (int)(ExposureTime + 10000));
	//	
	// 	if(intRetCode > 0)
	// 	{
	// 		m_pMainWnd->SendMessage(WM_MESSGEBOX_DISPLAY, (WPARAM)L"Snap failed!");
	// 		return false;
	// 	}
	//	
	// 	if(!pTempImage8Bit)
	// 		return true;
	// 
	// 	int unit = 1;
	// 	int capbuf = GetCapturedBuffer();
	// 	int size = imageXdim() * imageYdim();
	// 
	// 	ReadBufferUchar(unit, capbuf, 0, 0, -1, -1, pTempImage8Bit, size, "Grey");
	// 	TransferDataFromFramGrabber16Bits((LPIMAGETYPE)pTempImage8Bit, (LPIMAGETYPE)pImage);

	int Pre = m_iCurSnap;
	int iCur = Snap(Pre);
	if (iCur == -1)
		return NULL;

	Pre = iCur;
	//TRACE("WriteImageThread  = %d : \n", iCur);

	if (pTempImage8Bit && pImage == NULL)
	{
		memcpy(pTempImage8Bit, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);
	}

	if (pImage)
	{
		memcpy(pImage, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);
	}

	return true;
}

bool CVADAVLib::CameraSnap(int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE32Bits pImage, bool bPlus)
{
	// 	int intRetCode=0;
	// 	intRetCode= Snap(1, 1, 1);
	// 	intRetCode =(int) WaitForSingleObject(Handle, (int)(ExposureTime + 10000));
	// 	if(intRetCode > 0)
	// 	{
	// 		m_pMainWnd->SendMessage(WM_MESSGEBOX_DISPLAY, (WPARAM)L"Snap failed!");
	// 		return false;
	// 	}
	//              
	// 	int unit = 1;
	// 	int capbuf = GetCapturedBuffer();
	// 	int size = imageXdim() * imageYdim();
	// 
	// 	ReadBufferUchar(unit, capbuf, 0, 0, -1, -1, pTempImage8Bit, size, "Grey");
	// 	TransferDataFromFramGrabber32Bits((LPIMAGETYPE)pTempImage8Bit, pImage, bPlus);

	int Pre = m_iCurSnap;
	int iCur = Snap(Pre);
	if (iCur == -1)
		return NULL;

	Pre = iCur;
	//TRACE("WriteImageThread  = %d : \n", iCur);

	if (pTempImage8Bit && pImage == NULL)
	{
		memcpy(pTempImage8Bit, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);
	}

	if (pImage)
	{
		int nBufSize = m_nImgWidth * m_nImgHeight;
		if (bPlus)
		{
			for (int k = 0; k<nBufSize; k++)
			{
				pImage[k] += m_ImgBuffer[iCur][k];
			}
		}
		else
		{
			for (int k = 0; k<nBufSize; k++)
			{
				pImage[k] = m_ImgBuffer[iCur][k];
			}
		}

	}

	return true;
}

int CVADAVLib::Snap(int iPre)
{
	int iCount = 0;
	int iCur = -1;
	int maxCounter = 0;

	while (1)
	{
		//  [8/29/2014 Administrator] wait 또는 Error 시에도 멈춰야함.
		if (*m_statusFlag == 0 || *m_statusFlag >= 3)
			return -1;

		//2가 아니면 디텍터 레디 명령 재전송.
		/*if(m_gbtCtrl->GetStatus() < 2)
		{
		maxCounter++;
		if( (maxCounter % 50) == 0)
		{
		maxCounter = 0;
		m_gbtCtrl->SetDetectorReady();
		}
		}*/

		if (iPre != m_iCurSnap)
		{
			iCur = m_iCurSnap;
			break;
		}
		Sleep(10);
		iCount++;
		//		if(iCount > 50)			break;
	}
	return iCur;
}

int CVADAVLib::pOnSnap(int iPre)
{
	int iCount = 0;
	int iCur = -1;
	while (1)
	{
		if (iPre != m_iCurSnap)
		{
			iCur = m_iCurSnap;
			break;
		}
		Sleep(10);
		iCount++;
		//		if(iCount > 50)			break;
	}
	return iCur;
}

int CVADAVLib::CameraSnapForLive(int iPreCapbuf, int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE pImage, int* statusFlag)
{
	//	int unit = 1;
	//	int size = imageXdim() * imageYdim();

	//	int iCurCapbuf, nCount = 0;
	// 	while((iCurCapbuf = GetCapturedBuffer()) == iPreCapbuf)
	// 	{
	// 		nCount++;
	// 		// sin : Change Limit Count (200 -> ExposureTime*3)
	// 		if(nCount > ExposureTime*3)
	// 			return -1;
	// 		Sleep(1);
	// 	}
	// 
	// 	if(!pTempImage8Bit)
	// 		return iCurCapbuf;
	// 
	// 	ReadBufferUchar(unit, iCurCapbuf, 0, 0, -1, -1, pTempImage8Bit, size, "Grey");
	// 
	// 	if(!pImage)
	// 		return iCurCapbuf;
	// 	TransferDataFromFramGrabber16Bits((LPIMAGETYPE)pTempImage8Bit, pImage);
	m_statusFlag = statusFlag;
	int Pre = m_iCurSnap;
	int iCur = Snap(Pre);
	if (iCur == -1)
		return -1;

	Pre = iCur;

	if (pTempImage8Bit && pImage == NULL)
		memcpy(pTempImage8Bit, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);

	if (pImage)
		memcpy(pImage, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);

	return iCur;
}

int CVADAVLib::CameraSnapForLive(int iPreCapbuf, int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE32Bits pImage, bool bPlus)
{
	// 	int unit = 1;
	// 	int size = imageXdim() * imageYdim();
	// 
	// 	int iCurCapbuf, nCount = 0;
	// 	while((iCurCapbuf = GetCapturedBuffer()) == iPreCapbuf)
	// 	{
	// 		nCount++;
	// 		if(nCount > 2000)
	// 			return -1;
	// 		Sleep(1);
	// 	}
	// 	if(!pTempImage8Bit)
	// 		return iCurCapbuf;
	// 
	// 	ReadBufferUchar(unit, iCurCapbuf, 0, 0, -1, -1, pTempImage8Bit, size, "Grey");
	// 
	// 	if(!pImage)
	// 		return iCurCapbuf;
	// 	TransferDataFromFramGrabber32Bits((LPIMAGETYPE)pTempImage8Bit, pImage, bPlus);

	int Pre = m_iCurSnap;
	int iCur = pOnSnap(Pre);
	if (iCur == -1)
		return NULL;

	Pre = iCur;
	//TRACE("WriteImageThread  = %d : \n", iCur);

	if (pTempImage8Bit && pImage == NULL)
	{
		memcpy(pTempImage8Bit, m_ImgBuffer[iCur], sizeof(short)*m_nImgWidth*m_nImgHeight);
	}

	if (pImage)
	{
		int nBufSize = m_nImgWidth * m_nImgHeight;
		if (bPlus)
		{
			for (int k = 0; k<nBufSize; k++)
			{
				pImage[k] += m_ImgBuffer[iCur][k];
			}
		}
		else
		{
			for (int k = 0; k<nBufSize; k++)
			{
				pImage[k] = m_ImgBuffer[iCur][k];
			}
		}

	}

	return iCur;
}

void CVADAVLib::TransferDataFromFramGrabber16Bits(LPIMAGETYPE pSourceImage16Bits, LPIMAGETYPE pTarketImage16Bits)
{
	// 	short iWidth = m_sensorDescription.sensorX / m_iBining;
	// 	short iHigh = m_sensorDescription.sensorY / m_iBining;
	// 	short iSensor =m_sensorDescription.sensorStrips;
	// 	register short iInterval = m_sensorDescription.stripLength / m_iBining;
	// 
	// 	parallel_for(short(0), iHigh, [&](short i)
	// 	{
	// 		int iRow = i * iWidth;
	// 		for(int j = 0; j < iInterval; j++)
	// 		{
	// 			int iCol = iRow + j * iSensor;
	// 			pTarketImage16Bits[i + j * iHigh] = pSourceImage16Bits[iCol];
	// 			pTarketImage16Bits[i + (iInterval * 1 + j) * iHigh] = pSourceImage16Bits[iCol + 1];
	// 			pTarketImage16Bits[i + (iInterval * 2 + j) * iHigh] = pSourceImage16Bits[iCol + 2];
	// 			pTarketImage16Bits[i + (iInterval * 3 + j) * iHigh] = pSourceImage16Bits[iCol + 3];
	// 			pTarketImage16Bits[i + (iInterval * 4 + j) * iHigh] = pSourceImage16Bits[iCol + 4];
	// 			pTarketImage16Bits[i + (iInterval * 5 + j) * iHigh] = pSourceImage16Bits[iCol + 5];
	// 		}
	// 	});


	memcpy(pTarketImage16Bits, pSourceImage16Bits, sizeof(short)*m_nImgWidth*m_nImgHeight);

	// 	unsigned int iBufSize = m_nImgWidth*m_nImgHeight;
	//   	for (unsigned int k=0; k<iBufSize; k++)
	//   	{
	//   		pTarketImage16Bits[k] = pSourceImage16Bits[k];
	//   	}
}

void CVADAVLib::TransferDataFromFramGrabber32Bits(LPIMAGETYPE pSourceImage16Bits, LPIMAGETYPE32Bits pTarketImage32Bits, bool bPlus)
{
	unsigned int iBufSize = m_nImgWidth * m_nImgHeight;

	if (bPlus)
	{
		for (unsigned int k = 0; k<iBufSize; k++)
		{
			pTarketImage32Bits[k] += pSourceImage16Bits[k];
		}
	}
	else
	{
		for (unsigned int k = 0; k<iBufSize; k++)
		{
			pTarketImage32Bits[k] = pSourceImage16Bits[k];
		}
	}


	// 	if(!Handle)
	// 		return ;
	// 
	// 	short iWidth = m_sensorDescription.sensorX / m_iBining;
	// 	short iHigh = m_sensorDescription.sensorY / m_iBining;
	// 	short iSensor =m_sensorDescription.sensorStrips;
	// 	register short iInterval = m_sensorDescription.stripLength / m_iBining;
	// 	if(bPlus)
	// 	{
	// 		parallel_for(short(0), iHigh, [&](short i)
	// 		{
	// 			int iRow = i * iWidth;
	// 			for(int j = 0; j < iInterval; j++)
	// 			{
	// 				int iCol = iRow + j * iSensor;
	// 				pTarketImage32Bits[i + j * iHigh] += pSourceImage16Bits[iCol];
	// 				pTarketImage32Bits[i + (iInterval * 1 + j) * iHigh] += pSourceImage16Bits[iCol + 1];
	// 				pTarketImage32Bits[i + (iInterval * 2 + j) * iHigh] += pSourceImage16Bits[iCol + 2];
	// 				pTarketImage32Bits[i + (iInterval * 3 + j) * iHigh] += pSourceImage16Bits[iCol + 3];
	// 				pTarketImage32Bits[i + (iInterval * 4 + j) * iHigh] += pSourceImage16Bits[iCol + 4];
	// 				pTarketImage32Bits[i + (iInterval * 5 + j) * iHigh] += pSourceImage16Bits[iCol + 5];
	// 			}
	// 		});
	// 	}else
	// 	{
	// 		parallel_for(short(0), iHigh, [&](short i)
	// 		{
	// 			int iRow = i * iWidth;
	// 			for(int j = 0; j < iInterval; j++)
	// 			{
	// 				int iCol = iRow + j * iSensor;
	// 				pTarketImage32Bits[i + j * iHigh] = pSourceImage16Bits[iCol];
	// 				pTarketImage32Bits[i + (iInterval * 1 + j) * iHigh] = pSourceImage16Bits[iCol + 1];
	// 				pTarketImage32Bits[i + (iInterval * 2 + j) * iHigh] = pSourceImage16Bits[iCol + 2];
	// 				pTarketImage32Bits[i + (iInterval * 3 + j) * iHigh] = pSourceImage16Bits[iCol + 3];
	// 				pTarketImage32Bits[i + (iInterval * 4 + j) * iHigh] = pSourceImage16Bits[iCol + 4];
	// 				pTarketImage32Bits[i + (iInterval * 5 + j) * iHigh] = pSourceImage16Bits[iCol + 5];
	// 			}
	// 		});
	// 	}
}

void CVADAVLib::EnqueueDemoImageBuffer(byte* pDemoImageBuffer, int nWidth, int nHeight, int nBytesPerPixel)
{
	if (m_FrameBuffer == NULL)
	{
		return;
	} // else

	m_nFrameWidth = nWidth;
	m_nFrameHeight = nHeight;

	m_nImgWidth = nWidth;
	m_nImgHeight = nHeight;
	
	memcpy(m_FrameBuffer, pDemoImageBuffer, nWidth * nHeight * nBytesPerPixel);

	OnCaptureRecv();
}