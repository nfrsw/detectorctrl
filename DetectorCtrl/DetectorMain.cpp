#include "stdafx.h"
#include "DetectorMain.h"

namespace DetectorCtrl
{
	DetectorMain::DetectorMain()
	{
		this->m_vadavLib = NULL;

		this->avgBuffer = NULL;
	}

	static GCHandle imageAcquiredHandler;
	static GCHandle WriteLogRequestedHandler;

	// 디텍터 연동 초기화
	bool DetectorMain::DetectorInit()
	{
		if (this->m_vadavLib == NULL)
		{
			this->m_vadavLib = new CVADAVLib();
		}

		bool init = true;

		this->startFrameMode = BRIGHT;
		
		RegisterImageAcquiredProc();

		RegisterWriteLogRequestedProc();

		this->m_vadavLib->m_bBinningFlag = true;

		this->m_vadavLib->CameraInit(NULL, 0);

		return init;
	}

	void DetectorMain::RegisterImageAcquiredProc()
	{
		CallerImageAcquiredProc^ imageAcquiredDelegatePointer = gcnew CallerImageAcquiredProc(this, &DetectorMain::ImageAcquiredProc);
		imageAcquiredHandler = GCHandle::Alloc(imageAcquiredDelegatePointer);
		IntPtr imageAcquiredManagedFunction = Marshal::GetFunctionPointerForDelegate(imageAcquiredDelegatePointer);
		tVIMGACQ_CallBackProc imageAcquiredCallback = static_cast<tVIMGACQ_CallBackProc>(imageAcquiredManagedFunction.ToPointer());

		this->m_vadavLib->VADAV_SetImageAcquiredProc(imageAcquiredCallback);
	}

	void DetectorMain::RegisterWriteLogRequestedProc()
	{
		CallerWriteLogRequestedProc^ WriteLogRequestedDelegatePointer = gcnew CallerWriteLogRequestedProc(this, &DetectorMain::WriteLogRequestedProc);
		WriteLogRequestedHandler = GCHandle::Alloc(WriteLogRequestedDelegatePointer);
		IntPtr WriteLogRequestedManagedFunction = Marshal::GetFunctionPointerForDelegate(WriteLogRequestedDelegatePointer);
		tVLOG_CallBackProc WriteLogRequestedCallback = static_cast<tVLOG_CallBackProc>(WriteLogRequestedManagedFunction.ToPointer());

		this->m_vadavLib->VADAV_SetWriteLogRequestedProc(WriteLogRequestedCallback);
	}

	void DetectorMain::NotifyWriteLogRequested(String ^logText)
	{
		WriteLogRequested(logText);
	}

	void DetectorMain::WriteLogRequestedProc(CString logText)
	{
		NotifyWriteLogRequested(gcnew String(logText));
	}

	void DetectorMain::DetectorClose()
	{
		imageAcquiredHandler.Free();

		if (this->m_vadavLib != NULL)
		{
			delete this->m_vadavLib;

			this->m_vadavLib = NULL;
		}

		FreeAVGBuffer();
	}

	bool DetectorMain::DetectorReady()
	{
		this->startFrameMode = BRIGHT;

		this->m_vadavLib->CameraClose();

		int width = this->m_vadavLib->GetImageSizeX();
		int height = this->m_vadavLib->GetImageSizeY();
		int exposureTime = this->exposureTime;

		this->m_vadavLib->CameraSet(exposureTime, 2, width, height, 1, false);

		this->m_vadavLib->CallbackInit();

		return true;
	}

	bool DetectorMain::GetDarkOffset()
	{
		this->startFrameMode = DARK;
		this->currentDarkFrameStep = READY;

		this->m_vadavLib->CameraClose();

		int width = this->m_vadavLib->GetImageSizeX();
		int height = this->m_vadavLib->GetImageSizeY();
		int exposureTime = this->exposureTime;

		this->m_vadavLib->pOnCameraSet(exposureTime, 2, width, height, 1, false);

		this->m_vadavLib->CallbackInit();

		return true;
	}

	bool DetectorMain::GetDarkAir()
	{
		this->startFrameMode = DARK;

		this->m_vadavLib->CameraClose();

		int width = this->m_vadavLib->GetImageSizeX();
		int height = this->m_vadavLib->GetImageSizeY();
		int exposureTime = this->exposureTime;

		this->m_vadavLib->CameraSet(exposureTime, 2, width, height, 1, false);

		this->m_vadavLib->CallbackInit();

		return true;
	}

	bool DetectorMain::DetectorAbort()
	{
		this->m_vadavLib->CameraClose();

		return true;
	}

	void DetectorMain::SetCaptureMode(int mode)
	{
		this->captureMode = (CAPTURE_MODE)mode;
	}

	void DetectorMain::AverageImage(short *outImageBuffer, int width, int height)
	{
		int bufferLength = width * height;

		this->m_vadavLib->CoAverageImage(outImageBuffer, this->avgBuffer, this->avgNumber, bufferLength);
	}

	void DetectorMain::CorrrectionImage(short *outImageBuffer, int width, int height)
	{
		int bufferLength = width * height;

		short *inImageBuffer = new short[bufferLength];

		memcpy(inImageBuffer, outImageBuffer, bufferLength * sizeof(short));

		memset(outImageBuffer, 0, bufferLength * sizeof(short));

		this->m_vadavLib->CorrectionImage(outImageBuffer, inImageBuffer, width, height);

		delete inImageBuffer;

		inImageBuffer = NULL;
	}

	void DetectorMain::EnhanceImage(short *outImageBuffer, int width, int height)
	{
		int bufferLength = width * height;

		short *inImageBuffer = new short[bufferLength];

		memcpy(inImageBuffer, outImageBuffer, bufferLength * sizeof(short));

		memset(outImageBuffer, 0, bufferLength * sizeof(short));

		this->m_vadavLib->EnhanceImage(outImageBuffer, inImageBuffer, width, height);
		
		delete inImageBuffer;

		inImageBuffer = NULL;
	}

	void DetectorMain::DSAMaskImage(short *outImageBuffer, int width, int height)
	{
		int bufferLength = width * height;

		short *inImageBuffer = new short[bufferLength];

		memcpy(inImageBuffer, outImageBuffer, bufferLength * sizeof(short));

		memset(outImageBuffer, 0x00, bufferLength * sizeof(short));

		this->m_vadavLib->DSAMaskImage(outImageBuffer, inImageBuffer, width, height);

		delete inImageBuffer;

		inImageBuffer = NULL;
	}

	void DetectorMain::PreProcessImage(short *outImageBuffer, int width, int height)
	{
		if (this->captureMode == RADIOGRAPHY)
		{
			AverageImage(outImageBuffer, width, height);

			CorrrectionImage(outImageBuffer, width, height);

			EnhanceImage(outImageBuffer, width, height);
		}
		else
		{
			CorrrectionImage(outImageBuffer, width, height);

			// DSA MASK 생성 시점인지에 따라 MASK 영상 획득
			// DSAMaskImage(outImageBuffer, width, height);

			// DSA 모드이냐에 따라 DSA MASK 적용
		}
	}

	void DetectorMain::NotifyImageAcquired(short *finalImageBuffer, int width, int height, int bytesPerPixel)
	{
		int size = width * height * bytesPerPixel;
		cli::array<byte>^ imageBufferTemp = gcnew cli::array<byte>(size);

		Marshal::Copy((IntPtr)finalImageBuffer, imageBufferTemp, 0, size);

		ImageAcquired(imageBufferTemp, width, height, bytesPerPixel);
	}

	void DetectorMain::ImageAcquiredProcForBright(short* imageBuffer, int width, int height, int bytesPerPixel)
	{
		bool acquisitionCompleted = false;
		int originalBufferBytes = width * height * bytesPerPixel;

		if (this->captureMode == RADIOGRAPHY)
		{
			SetAVGBuffer(this->avgIndex, imageBuffer, originalBufferBytes);

			this->avgIndex++;

			if (this->avgIndex >= this->avgNumber)
			{
				acquisitionCompleted = true;

				this->m_vadavLib->CameraClose();
			} // else
		}
		else
		{
			acquisitionCompleted = true;
		}

		if (acquisitionCompleted == true)
		{
			short *processedImageBuffer = new short[width * height];

			if (this->captureMode == FLOUROSCOPY)
			{
				memcpy(processedImageBuffer, imageBuffer, originalBufferBytes);
			} // else

			PreProcessImage(processedImageBuffer, width, height);

			NotifyImageAcquired(processedImageBuffer, width, height, bytesPerPixel);

			delete processedImageBuffer;

			processedImageBuffer = NULL;

			FreeAVGBuffer();
		}
	}

	void DetectorMain::ImageAcquiredProcForDark(short* imageBuffer, int width, int height, int bytesPerPixel)
	{
		bool acquisitionCompleted = false;
		int originalBufferBytes = width * height * bytesPerPixel;

		SetAVGBuffer(this->avgIndex, imageBuffer, originalBufferBytes);

		this->avgIndex++;

		if (this->avgIndex >= this->avgNumber)
		{
			acquisitionCompleted = true;

			this->m_vadavLib->CameraClose();
		} // else

		if (acquisitionCompleted == true)
		{
			short *outImageBuffer = new short[width * height];

			AverageImage(outImageBuffer, width, height);

			if (this->currentDarkFrameStep == READY)
			{
				this->m_vadavLib->SaveCalibrationData(_T("C:\\METEOR\\CAL\\offset_2"), outImageBuffer, width * height, sizeof(short));
				
				this->currentDarkFrameStep = OFFSET_DONE;

				// TODO - KimGilSu - Notify Offset Done
			}
			else if (this->currentDarkFrameStep == OFFSET_DONE)
			{
				this->m_vadavLib->SaveCalibrationData(_T("C:\\METEOR\\CAL\\airraw_2"), outImageBuffer, width * height, sizeof(short));

				this->currentDarkFrameStep = AIR_DONE;

				// TODO - KimGilSu - Notify Air Done
			}

			delete outImageBuffer;

			outImageBuffer = NULL;

			FreeAVGBuffer();
		}
	}

	void DetectorMain::ImageAcquiredProc(short* imageBuffer, int width, int height, int bytesPerPixel)
	{
		if (this->startFrameMode == BRIGHT)
		{
			ImageAcquiredProcForBright(imageBuffer, width, height, bytesPerPixel);
		}
		else
		{
			ImageAcquiredProcForDark(imageBuffer, width, height, bytesPerPixel);
		}
	}

	void DetectorMain::SetAVGBuffer(int avgIndex, short* imageBuffer, int bufferLength)
	{
		if (this->avgBuffer != NULL
			&& imageBuffer != NULL
			&& avgIndex < this->avgNumber
			&& this->avgBuffer[avgIndex] != NULL)
		{
			int bufferLengthInShort = bufferLength / 2;

			for (int k = 0; k < bufferLengthInShort; k++)
			{
				this->avgBuffer[avgIndex][k] = imageBuffer[bufferLengthInShort - k - 1];
			}
		} // else
	}

	bool DetectorMain::AllocAVGBuffer(int avgNumber, int bufferLength)
	{
		if (this->avgBuffer != NULL)
		{
			FreeAVGBuffer();
		} // else

		this->avgBuffer = new short *[avgNumber];

		for (int index = 0; index < avgNumber; index++)
		{
			this->avgBuffer[index] = new short[bufferLength];
		}

		return true;
	}

	void DetectorMain::FreeAVGBuffer()
	{
		if (this->avgBuffer != NULL)
		{
			for (int index = 0; index < this->avgNumber; index++)
			{
				delete this->avgBuffer[index];

				this->avgBuffer[index] = NULL;
			}

			delete this->avgBuffer;

			this->avgBuffer = NULL;

			this->avgNumber = 0;

			this->avgIndex = 0;
		} // else
	}

	void DetectorMain::SetAVGNumber(int avgNumber)
	{
		if (avgNumber < 1)
		{
			return;
		} // else

		int imageWidth = 0;
		int imageHeight = 0;

		this->m_vadavLib->GetImageSize(imageWidth, imageHeight);

		if (imageWidth > 0
			&& imageHeight > 0
			&& AllocAVGBuffer(avgNumber, imageWidth * imageHeight) == true)
		{
			this->avgNumber = avgNumber;
			this->avgIndex = 0;
		} // else
	}

	void DetectorMain::SetDutyNumber(int dutyNumber)
	{
		this->dutyNumber = dutyNumber;
	}

	void DetectorMain::SetExposureTime(int exposureTime)
	{
		this->exposureTime = exposureTime;
	}

	void DetectorMain::EnqueueDemoImageBuffer(cli::array<byte> ^imageBuffer, int width, int height, int nBytesPerPixel)
	{
		if (this->m_vadavLib == nullptr)
		{
			return;
		} // else

		pin_ptr<byte> pNativeMem = &imageBuffer[0];

		this->m_vadavLib->EnqueueDemoImageBuffer(pNativeMem, width, height, nBytesPerPixel);
		
		pNativeMem = nullptr;
	}
}
