#include "StdAfx.h"

#include "MyDIP.h"

#include <vector>
#include <algorithm>
using namespace std;

#include <ppl.h>
using namespace Concurrency;

//메모리 누수 체크
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CMyDIP::CMyDIP(void)
	: m_pColorLut(NULL)
	, m_histo(NULL)
{
	InitLUT();
	
	TRACE("DIP Create!!!\n");
	
	if(m_histo == NULL)
		m_histo = new int[16384];
	
	memset(m_histo, 0x00, sizeof(int)*16384);
}


CMyDIP::~CMyDIP(void)
{
	if(m_pColorLut != NULL)
	{
		delete []m_pColorLut;
		TRACE("DIP Delete \n");
	}
	m_pColorLut = NULL;

	if(m_histo != NULL)
		delete []m_histo;
	m_histo = NULL;

}
void CMyDIP::InitLUT()
{
	//룩업 테이블
	m_nColorLutSize = SHRT_MAX - SHRT_MIN;
	if(m_pColorLut == NULL)
		m_pColorLut = new BYTE[m_nColorLutSize];
	
	memset(m_pColorLut, 0x00, sizeof(BYTE)*m_nColorLutSize);
}

void CMyDIP::freeLUT()
{
	if(m_pColorLut != NULL)
		delete []m_pColorLut;

	m_pColorLut = NULL;
}

/************************************************************************/
/* downSampling                                                       */
/************************************************************************/
int CMyDIP::DownSampling(short* src, short* dst, int w, int h)
{
	//2D로 변경
	short **tmpSrc = new short *[h];
	for(int idy=0; idy < h; idy++)
		tmpSrc[idy] = &src[idy*w];

	// 축소영상의 가로 세로 길이를계산
	int reHeight = h/2;
	int reWidth = w/2;

	
	// 축소영상을위한메모리할당
 
	//parallel_for( 0, h, [&](int idy){
	#pragma omp parallel for schedule(guided)	
	for(int idy=0; idy < h-2; idy +=2)
	{
		for(int idx = 0 ; idx < w-2; idx+=2)
		{
			int avg = 0, sum = 0, tx = 0, ty = 0, counter = 0;
			
			
			sum += tmpSrc[idy][idx];
			sum += tmpSrc[idy][idx+1];
			sum += tmpSrc[idy+1][idx];				
			sum += tmpSrc[idy+1][idx+1];

			/*for(int y = 0; y <= 1; y++)
				for(int x = 0; x <= 1; x++)
				{
					ty = idy + y;
					tx = idx + x;

					if(ty < 0 || ty > h || tx < 0 || tx > w)
						continue;

					sum += tmpSrc[ty][tx];

					counter++;
				}*/

			avg = sum / 4;

			dst[(idy/2)*reWidth + (idx/2)] = avg;

		}	
	}

	delete []tmpSrc;

	return 0;
}

/************************************************************************/
/* Thumbnail	                                                       */
/************************************************************************/
void CMyDIP::CreateThumbnail(short* src, short* dst, int W, int H, int ratio)
{
	// 축소영상의 가로 세로 길이를계산
	int reHeight = H/ratio;
	int reWidth = W/ratio;

	// 축소영상을위한메모리할당
	parallel_for(0, reHeight, [&](int i){
		for(int j=0 ; j<reWidth ; j++)
			dst[i*reWidth + j] = src[(i*ratio*W)+ratio*j];

	});
	//    for(int i=0 ; i<reHeight ; i++)

}

void CMyDIP::findImageWWL(CString strImgPath, int &minValue, int &maxValue, int width, int height, double dMinRatio, double dMaxRatio)
{
	//------------------------------------------------------------------------
	// Find Image Min, Max
	FILE *fp = NULL;
	_wfopen_s(&fp, strImgPath, L"rb");

	if (fp != NULL)
	{
		int nPixelNum = width * height;
		short *img = new short[nPixelNum];

		fread(img, sizeof(short)*nPixelNum, 1, fp);
		fclose(fp);
		
		findImageWWL(img, minValue, maxValue, width, height, dMinRatio, dMaxRatio);
		delete [] img;
	}
}

void CMyDIP::findImageWWL(short* img, int &minValue, int &maxValue, int width, int height, double dMinRatio, double dMaxRatio)
{
	const int nHistSize = 16384;
	const int nHistMax = 16383;

	int *pHisto = new int[nHistSize];
	memset(pHisto, 0x00, sizeof(int)*nHistSize);

	int nPixelNum = width * height;
	for (int k=0; k<nPixelNum; k++)
	{
		int nCurValue = min(max(0, img[k]), nHistMax);
		pHisto[nCurValue]++;
	}

	int nSum = 0;
	int nMinLimit = nPixelNum * dMinRatio;
	int nMaxLimit = nPixelNum * dMaxRatio;
	minValue = nHistMax;
	maxValue = 0;

	//min value
	for(int idx=0; idx <= nHistMax; idx++)
	{
		nSum += pHisto[idx];

		if(nSum >= nMinLimit)
		{
			minValue = idx;
			break;
		}
	}

	//max value
	nSum = 0;
	for(int idx=nHistMax-1; idx >= 0; idx--)
	{
		nSum += pHisto[idx];

		if(nSum >= nMaxLimit)
		{
			maxValue = idx;
			break;
		}
	}

	if (minValue > maxValue)
	{
		int nTemp = maxValue;
		maxValue = minValue;
		minValue = nTemp;
	}

	delete [] pHisto;
}

void CMyDIP::findMinMaxValue(short* img, int &minValue, int &maxValue, int width, int height, int &abcVal)
{
	//------------------------------------------------------------------------
	// abcVal = "nMinThreshold" 값 이하인 픽셀의 수가 전체 영상의 dRegionRate 이상이면 1, 아니면 0
	int nMinThreshold = 1000;
	double dRegionRate = 0.25;
	
	int nTotalLen = width * height;
	int nRegionCount = nTotalLen * dRegionRate;
	int nLowPixelCount = 0;


	for(int k=0; k < nTotalLen; k++)
	{
		if (img[k]<=nMinThreshold)
			nLowPixelCount++;
	}

	if (nLowPixelCount >= nRegionCount)
	{
		abcVal = 1;
	}
	else
	{
		abcVal = 0;
	}

	minValue = 16383;
	maxValue = 0;
	//------------------------------------------------------------------------
	// Find Min, Max
	// 필요 없음 => Client에서 처리하는것으로 바꿈

	minValue = 0;
	maxValue = 16383;

	return;



/*
	int totalLen = width * height;
	int sum = 0;
	
	if(m_histo != NULL)
	{
		memset(m_histo, 0x00, sizeof(int)*16384);
	}
	else
	{
		m_histo = new int[16384];
		memset(m_histo, 0x00, sizeof(int)*16384);
	}

	for(int idx =0; idx < totalLen; idx++)
	{
		if(img[idx] > 16383)
		{
			m_histo[16383]++;
		}
		else if(img[idx] < 0)
		{
			m_histo[0]++;
		}
		else
		{
			m_histo[img[idx]]++;
		}
	}

	//min value
	for(int idx=0; idx < 16384; idx++)
	{
		sum += m_histo[idx];

		if(sum > 1731)
		{
			minValue = idx;
			break;
		}
		
	}

	//max value
	sum = 0;
	for(int idx=16383; idx >= 0; idx--)
	{
		sum += m_histo[idx];

		if(sum > 1731)
		{
			maxValue = idx;
			break;
		}

	}

	//abc value
	double dRegionRate = 0.25;
	unsigned int nMinimumValue = 2000;

	unsigned int nRegionCount = width * height * dRegionRate;
	sum = 0;
	abcVal = 0;
	for(int idx=0; idx < 16384; idx++)
	{
		sum += m_histo[idx];

		if(sum > nRegionCount)
		{
			if (idx < nMinimumValue)
			{
				abcVal = 1;
			}
			break;
		}
	}

	//delete []histo;
	
	/*
	int nBufSize = width * height;

	minValue = maxValue = img[0];

	for (int k=0; k<nBufSize; k++)
	{
		if (minValue > img[k])
		{
			minValue = img[k];
		}

		if (maxValue < img[k])
		{
			maxValue = img[k];
		}
	}
	* /
	//minValue += ( minValue * 0.1);
	//maxValue -= (maxValue * 0.1);

*/
}

void CMyDIP::CreateLutTable(int m_nWidth, int m_nHeight, short minVal, short maxVal)
{
	int m_nMinValue = minVal;
	int m_nMaxValue = maxVal;

	int m_nLevel = (m_nMaxValue+m_nMinValue)/2;
	int m_nLWidth = m_nMaxValue-m_nMinValue;

	float term = m_nMaxValue - (m_nMinValue);

	float fTemp;
	if(term > 0)
		fTemp = 255.0f / term;
	else
		fTemp = 255.0f / m_nMaxValue;

	//------------------------------------------------------------------------
	// Update 8Bit Image

	int nStartIdx = (m_nLevel-(m_nLWidth/2)) - SHRT_MIN;
	int nEndIdx = (m_nLevel+(m_nLWidth/2)) - SHRT_MIN;

	double dMapingRate = 255.0 / m_nLWidth;
	double dGrayValue = 0;

	//clock_t before = clock();
	memset(m_pColorLut, 0, nStartIdx);
	memset(m_pColorLut+nEndIdx, 255, m_nColorLutSize-nEndIdx);

	for (int i=nStartIdx+1; i<nEndIdx; i++)
	{
		m_pColorLut[i] = (BYTE)dGrayValue;
		dGrayValue += dMapingRate;
	}
}

int CMyDIP::AutoLevelWidth(short* pRawData, unsigned char * pViewData, int m_nWidth, int m_nHeight, short minVal, short maxVal)
{
	/*int m_nMinValue = minVal;
	int m_nMaxValue = maxVal;
	
	int m_nLevel = (m_nMaxValue+m_nMinValue)/2;
	int m_nLWidth = m_nMaxValue-m_nMinValue;

	float term = m_nMaxValue - (m_nMinValue);

	float fTemp;
	if(term > 0)
		fTemp = 255.0f / term;
	else
		fTemp = 255.0f / m_nMaxValue;

	//------------------------------------------------------------------------
	// Update 8Bit Image

	int nStartIdx = (m_nLevel-(m_nLWidth/2)) - SHRT_MIN;
	int nEndIdx = (m_nLevel+(m_nLWidth/2)) - SHRT_MIN;

	double dMapingRate = 255.0 / m_nLWidth;
	double dGrayValue = 0;

	//clock_t before = clock();
	memset(m_pColorLut, 0, nStartIdx);
	memset(m_pColorLut+nEndIdx, 255, m_nColorLutSize-nEndIdx);

	for (int i=nStartIdx+1; i<nEndIdx; i++)
	{
		m_pColorLut[i] = (BYTE)dGrayValue;
		dGrayValue += dMapingRate;
	}
	*/
	memset(pViewData, 0x00, m_nWidth*m_nHeight);

	//int nWidth = DETECTOR_WIDTHBYTES(m_nWidth*8);
	//int iPosCount01 = 0, iPosCount = 0;
	int len = m_nHeight*m_nWidth;

	#pragma omp parallel for schedule(guided)
	for(int idx=0; idx<len; idx++)
	{
		pViewData[idx] = GetGrayValueIn16BitLut(m_pColorLut, pRawData[idx]);
	}
// 	parallel_for( 0, len, [&](int idx){
// 		pViewData[idx] = GetGrayValueIn16BitLut(m_pColorLut, pRawData[idx]);
// 	});
	/*for (int y=0; y<m_nHeight; y++)
		for (int x=0; x<m_nWidth;  x++)
		{
			iPosCount = y * m_nWidth + x;
			iPosCount01 = (m_nHeight - y - 1) * nWidth + x;
			pViewData[iPosCount01] = GetGrayValueIn16BitLut(m_pColorLut, pRawData[iPosCount]);
		}*/

		return 0;
}

int CMyDIP::AutoLevelWidth(short* pRawData, unsigned char * pViewData, int m_nWidth, int m_nHeight)
{
	int m_nMinValue = SHRT_MAX;
	int m_nMaxValue = SHRT_MIN;
	int x, y;
	
	for (y=0; y<m_nHeight; y++)
	for (x=0; x<m_nWidth;  x++)
	{
		m_nMinValue = min(m_nMinValue, pRawData[y*m_nWidth+x]);
		m_nMaxValue = max(m_nMaxValue, pRawData[y*m_nWidth+x]);
	}

	//findMinMaxValue(pRawData, m_nMinValue, m_nMaxValue)

	int m_nLevel = (m_nMaxValue+m_nMinValue)/2;
	int m_nLWidth = m_nMaxValue-m_nMinValue;

	float term = m_nMaxValue - (m_nMinValue);

	float fTemp;
	if(term > 0)
		fTemp = 255.0f / term;
	else
		fTemp = 255.0f / m_nMaxValue;

	//------------------------------------------------------------------------
	// Update 8Bit Image

	int nStartIdx = (m_nLevel-(m_nLWidth/2)) - SHRT_MIN;
	int nEndIdx = (m_nLevel+(m_nLWidth/2)) - SHRT_MIN;

	double dMapingRate = 255.0 / m_nLWidth;
	double dGrayValue = 0;

	//clock_t before = clock();
	memset(m_pColorLut, 0, nStartIdx);
	memset(m_pColorLut+nEndIdx, 255, m_nColorLutSize-nEndIdx);

	for (int i=nStartIdx+1; i<nEndIdx; i++)
	{
		m_pColorLut[i] = (BYTE)dGrayValue;
		dGrayValue += dMapingRate;
	}

	memset(pViewData, 0x00, m_nWidth*m_nHeight);

	int nWidth = DETECTOR_WIDTHBYTES(m_nWidth*8);
	int iPosCount01 = 0, iPosCount = 0;
	
	for (int y=0; y<m_nHeight; y++)
	for (int x=0; x<m_nWidth;  x++)
	{
		iPosCount = y * m_nWidth + x;
		iPosCount01 = (m_nHeight - y - 1) * nWidth + x;
		pViewData[iPosCount01] = GetGrayValueIn16BitLut(m_pColorLut, pRawData[iPosCount]);
		//pViewData[y*nWidth+x] = m_pColorLut[(int)(pRawData[y*m_nWidth+x]*fTemp)-SHRT_MIN];
	}

		return 0;
}

void CMyDIP::MedianFilter(short * Img, int Width, int Height, int iteration)
{
	int w, h, counter=0;
	vector<short> TempArray(10);

	short **Tmp2dImg = new short*[Height];

	short *TmpOrgCopy = new short[Width*Height];
	memset(TmpOrgCopy, 0x00, sizeof(short)*Width*Height);
	memcpy(TmpOrgCopy, Img, sizeof(short)*Width*Height);
	
	for(int i=0; i < Height; i++)
		Tmp2dImg[i] = &Img[i*Width];

	short tmp = 0;
	for(h=1; h < Height-1; h++)
	{
		for(w=1; w < Width-1 ; w++)
		{
			int tmp_iter = 0;
			for(int sub_idy = -1; sub_idy <= 1; sub_idy++)
				for(int sub_idx = -1; sub_idx <= 1; sub_idx++)
					TempArray[tmp_iter++] = Tmp2dImg[h+sub_idy][w+sub_idx];
			sort(TempArray.begin(), TempArray.end());

			TmpOrgCopy[h*Width+w] = TempArray[5];
		}
	}
	
	memcpy(Img, TmpOrgCopy, sizeof(short)*Width*Height);

	delete []TmpOrgCopy;
	delete []Tmp2dImg;
}

void CMyDIP::HybridMedianFilter(short *Img, int Width, int Height)
{
	vector<short> TempArray01(5);	//대각선 정렬용 x
	vector<short> TempArray02(5);	//십자 정렬 +
	short center_pixel = 0;			//가운데 픽셀

	short **Tmp2dImg = new short*[Height];
	for(int i=0; i < Height; i++)
		Tmp2dImg[i] = &Img[i*Width];

	short *TmpOrgCopy = new short[Width*Height];		//원본 복사
	memset(TmpOrgCopy, 0x00, sizeof(short)*Width*Height);
	memcpy(TmpOrgCopy, Img, sizeof(short)*Width*Height);

	int w, h;
	for(h=1; h < Height-1; h++)
	for(w=1; w < Width-1 ; w++)
	{
		//대각선								 // (h, x)
		TempArray01[0] = Tmp2dImg[h-1][w-1]; // (-1,-1)
		TempArray01[1] = Tmp2dImg[h-1][w+1]; // (-1,+1)
		TempArray01[2] = center_pixel = Tmp2dImg[h][w]; // (0,0)
		TempArray01[3] = Tmp2dImg[h+1][w-1]; // (+1,-1)
		TempArray01[4] = Tmp2dImg[h+1][w+1]; // (+1,+1)

		//십자
		TempArray02[0] = Tmp2dImg[h-1][w];  // (-1,0)
		TempArray02[1] = Tmp2dImg[h][w-1];	// (0,-1)
		TempArray02[2] = center_pixel;		// (0,0)
		TempArray02[3] = Tmp2dImg[h][w+1];	// (0,+1)
		TempArray02[4] = Tmp2dImg[h+1][w];	// (+1,0)

		sort(TempArray01.begin(), TempArray01.end());
		sort(TempArray02.begin(), TempArray02.end());

		short mid01 = TempArray01[2];
		short mid02 = TempArray02[2];

		if(mid01 < mid02)
			TmpOrgCopy[h*Width+w] =	mid02 < center_pixel ? mid02 : ( mid01 < center_pixel ? center_pixel : mid01 );
		else
			TmpOrgCopy[h*Width+w] = mid01 < center_pixel ? mid01 : ( mid02 < center_pixel ? center_pixel : mid02 ); 
		
	}
	
	memcpy(Img, TmpOrgCopy, sizeof(short)*Width*Height);

	delete []TmpOrgCopy;
	delete []Tmp2dImg;
}

void CMyDIP::HybridMedianFilterFast(short *pSrc, const int nWidth, const int nHeight)
{
	//	timeCheck_Init;
	//	timeCheck_Start;

	const int nPixelNum = nWidth*nHeight;

	short *pDst = pSrc;
	pSrc = new short[nPixelNum];
	memcpy(pSrc, pDst, sizeof(short)*nPixelNum);

	const int pIndex1[5] = {-nWidth-1, -nWidth+1, 0, nWidth-1, nWidth+1};	// 대각선 인덱스
	const int pIndex2[5] = {-nWidth, -1, 0, 1, nWidth};						// 십자가 인덱스

	// Old : 485ms (HybridMedianFilter)
	parallel_for(int(1), nHeight-1, [&](int y)		// 105ms
		//for (int y=1; y<nHeight-1; y++)				// 431ms
	{
		int pMedSrc1[5] = {0,};		// 대각선 정렬용 x
		int pMedSrc2[5] = {0,};		// 십자 정렬 +
		int pMed[3] = {0,};

		int nCurIndex = y*nWidth;
		for (int x=1; x<nWidth-1; x++)
		{
			nCurIndex++;

			for (int k=0; k<5; k++)
			{
				pMedSrc1[k] = pSrc[nCurIndex+pIndex1[k]];
				pMedSrc2[k] = pSrc[nCurIndex+pIndex2[k]];
			}

			// 			short *pSubSrc = &pSrc[nCurIndex-nWidth-1];
			// 			pMedSrc1[0] = *pSubSrc;
			// 			pMedSrc2[0] = *(++pSubSrc);
			// 			pMedSrc1[1] = *(++pSubSrc);
			// 			
			// 			pSubSrc += (nWidth-2);
			// 			pMedSrc2[1] = *pSubSrc;
			// 			pMedSrc2[2] = *(++pSubSrc);
			// 			pMedSrc2[3] = *(++pSubSrc);
			// 
			// 			pSubSrc += (nWidth-2);
			// 			pMedSrc1[3] = *pSubSrc;
			// 			pMedSrc2[4] = *(++pSubSrc);
			// 			pMedSrc1[4] = *(++pSubSrc);
			// 			
			// 			pMed[0] = pMedSrc1[2] = pMedSrc2[2];

			pMed[0] = pSrc[nCurIndex];
			pMed[1] = FindMedian(pMedSrc1, 5);
			pMed[2] = FindMedian(pMedSrc2, 5);

			pDst[nCurIndex] = FindMedian(pMed, 3);
		}
	}
	);

	if (pSrc)
		delete [] pSrc;


	//	timeCheck_EndMsg(L"HybridMedianFilterFast");
}

int CMyDIP::FindMedian(int *pSrc, const int nSize)
{
	for (int x=0; x<=nSize/2; x++)
	{
		for (int y=x+1; y<nSize; y++)
		{
			if (pSrc[x]>pSrc[y])
			{
				int nTemp = pSrc[x];
				pSrc[x] = pSrc[y];
				pSrc[y] = nTemp;

				//std::swap(pSrc[x], pSrc[y]);

				//pSrc[x] ^= pSrc[y];
				//pSrc[y] ^= pSrc[x];
				//pSrc[x] ^= pSrc[y];
			}
		}
	}

	return pSrc[nSize/2];
}

/************************************************************************/
/* Edge Enhancement                                                     */
/************************************************************************/
bool CMyDIP::UnsharpMask(short *pSrc, short *pDst, int nWidth, int nHeight,
	float radius /*=5.0*/, float amount /*=2.0*/, int threshold /*=0*/)
{

	short **tmpSrc = new short *[nHeight];
	for(int idx=0; idx < nHeight; idx++)
		tmpSrc[idx] = &pSrc[idx*nWidth];

	short **tmpDst = new short *[nHeight];
	for(int idx=0; idx < nHeight; idx++)
		tmpDst[idx] = &pDst[idx*nWidth];

	if (!GaussianBlur(tmpSrc, tmpDst, nWidth, nHeight, radius))
		return false;

	int bypp = 1;

	// merge the source and destination (which currently contains
	// the blurred version) images
#pragma omp parallel for
	for (long y=0; y<nHeight; y++)
		//parallel_for(0, nHeight, [&](int y)
	{
		// get source row
		short* cur_row = tmpSrc[y];
		// get dest row
		short* dest_row = tmpDst[y];
		// combine the two
		for (long x=0; x<nWidth; x++)
			for (long b=0, z=x*bypp; b<bypp; b++, z++)
			{
				int diff = cur_row[z] - dest_row[z];

				// do tresholding
				if (abs(diff) < threshold)
				{
					dest_row[z] = cur_row[z];
				}
				else
				{
					dest_row[z] = (short)min(SHRT_MAX, max(0,(int)(cur_row[z] + amount * diff)));
				}
			}
	}//);

	delete []tmpSrc;
	delete []tmpDst;

	return true;
}

bool CMyDIP::GaussianBlur(short **pSrc, short **pDst, int nWidth, int nHeight,
	float radius /*= 1.0f*/)
{
	// generate convolution matrix and make sure it's smaller than each dimension
	double *cmatrix = NULL;
	int cmatrix_length = gen_convolve_matrix(radius, &cmatrix);
	// generate lookup table
	double *ctable = gen_lookup_table(cmatrix, cmatrix_length);

	short **pDst_x = NULL;
	mem2dAlloc(pDst_x, nWidth, nHeight);

	// blur the rows
#pragma omp parallel for        
	for (int y=0;y<nHeight;y++)
		//parallel_for(0,nHeight, [&](int y)
	{
		blur_line(ctable, cmatrix, cmatrix_length, pSrc[y], pDst_x[y], nWidth);
	}//);

	// blur the cols
	short* cur_col = new short[nHeight];
	short* dest_col = new short[nHeight];

	int x, y;

	//parallel_for(0,nWidth, [&](int x)
	//#pragma omp parallel for
	for (x=0; x<nWidth; x++)
	{
		for (y=0; y<nHeight; y++)
		{
			cur_col[y] = pDst_x[y][x];
			dest_col[y] = pDst[y][x];
		}

		blur_line(ctable, cmatrix, cmatrix_length, cur_col, dest_col, nHeight);

		for (y=0; y<nHeight; y++)
		{
			pDst[y][x] = dest_col[y];
		}
	}

	memDelete(cur_col);
	memDelete(dest_col);

	mem2dDelete(pDst_x, nHeight);

	delete [] cmatrix;
	delete [] ctable;

	return true;
}

////////////////////////////////////////////////////////////////////////////////
/** 
* generates a 1-D convolution matrix to be used for each pass of 
* a two-pass gaussian blur.  Returns the length of the matrix.
* \author [nipper]
*/
int CMyDIP::gen_convolve_matrix (double radius, double **cmatrix_p)
{
	int matrix_length;
	int matrix_midpoint;
	double* cmatrix;
	int i,j;
	double std_dev;
	double sum;

	/* we want to generate a matrix that goes out a certain radius
	* from the center, so we have to go out ceil(rad-0.5) pixels,
	* including the center pixel.  Of course, that's only in one direction,
	* so we have to go the same amount in the other direction, but not count
	* the center pixel again.  So we double the previous result and subtract
	* one.
	* The radius parameter that is passed to this function is used as
	* the standard deviation, and the radius of effect is the
	* standard deviation * 2.  It's a little confusing.
	* <DP> modified scaling, so that matrix_lenght = 1+2*radius parameter
	*/
	radius = (double)fabs(0.5*radius) + 0.25f;

	std_dev = radius;
	radius = std_dev * 2;

	/* go out 'radius' in each direction */
	matrix_length = int (2 * ceil(radius-0.5) + 1);
	if (matrix_length <= 0) matrix_length = 1;
	matrix_midpoint = matrix_length/2 + 1;
	*cmatrix_p = new double[matrix_length];
	cmatrix = *cmatrix_p;

	/*  Now we fill the matrix by doing a numeric integration approximation
	* from -2*std_dev to 2*std_dev, sampling 50 points per pixel.
	* We do the bottom half, mirror it to the top half, then compute the
	* center point.  Otherwise asymmetric quantization errors will occur.
	*  The formula to integrate is e^-(x^2/2s^2).
	*/

	/* first we do the top (right) half of matrix */
	for (i = matrix_length/2 + 1; i < matrix_length; i++)
	{
		double base_x = i - (float)floor((double)(matrix_length/2)) - 0.5f;
		sum = 0;
		for (j = 1; j <= 50; j++)
		{
			if ( base_x+0.02*j <= radius ) 
				sum += (double)exp (-(base_x+0.02*j)*(base_x+0.02*j) / 
				(2*std_dev*std_dev));
		}
		cmatrix[i] = sum/50;
	}

	/* mirror the thing to the bottom half */
	for (i=0; i<=matrix_length/2; i++) {
		cmatrix[i] = cmatrix[matrix_length-1-i];
	}

	/* find center val -- calculate an odd number of quanta to make it symmetric,
	* even if the center point is weighted slightly higher than others. */
	sum = 0;
	for (j=0; j<=50; j++)
	{
		sum += (double)exp (-(0.5+0.02*j)*(0.5+0.02*j) /
			(2*std_dev*std_dev));
	}
	cmatrix[matrix_length/2] = sum/51;

	/* normalize the distribution by scaling the total sum to one */
	sum=0;
	for (i=0; i<matrix_length; i++) sum += cmatrix[i];
	for (i=0; i<matrix_length; i++) cmatrix[i] = cmatrix[i] / sum;

	return matrix_length;
}

////////////////////////////////////////////////////////////////////////////////
/**
* generates a lookup table for every possible product of 0-255 and
* each value in the convolution matrix.  The returned array is
* indexed first by matrix position, then by input multiplicand (?)
* value.
* \author [nipper]
*/
double* CMyDIP::gen_lookup_table (double *cmatrix, int cmatrix_length)
{
	double* lookup_table = new double[cmatrix_length * IMAGE_VALUE_LEN];
	double* lookup_table_p = lookup_table;
	double* cmatrix_p      = cmatrix;

	for (int i=0; i<cmatrix_length; i++)
	{
		for (int j=IMAGE_VALUE_MIN; j<IMAGE_VALUE_MAX; j++)
		{
			*(lookup_table_p++) = *cmatrix_p * (double)j;
		}
		cmatrix_p++;
	}

	return lookup_table;
}
////////////////////////////////////////////////////////////////////////////////
/**
* this function is written as if it is blurring a column at a time,
* even though it can operate on rows, too.  There is no difference
* in the processing of the lines, at least to the blur_line function.
* \author [nipper]
*/
void CMyDIP::blur_line (double *ctable, double *cmatrix, int cmatrix_length, short* cur_col, short* dest_col, int y)
{
	double	scale;
	double	sum;
	int i=0, j=0;
	int row;
	int cmatrix_middle = cmatrix_length/2;

	double		*cmatrix_p;
	short	*cur_col_p;
	short	*cur_col_p1;
	short	*dest_col_p;
	double		*ctable_p;

	/* this first block is the same as the non-optimized version --
	* it is only used for very small pictures, so speed isn't a
	* big concern.
	*/
	if (cmatrix_length > y)
	{
		for (row = 0; row < y ; row++)
		{
			scale=0;
			/* find the scale factor */
			for (j = 0; j < y ; j++)
			{
				/* if the index is in bounds, add it to the scale counter */
				if ((j + cmatrix_middle - row >= 0) &&
					(j + cmatrix_middle - row < cmatrix_length))
					scale += cmatrix[j + cmatrix_middle - row];
			}

			sum = 0;
			for (j = 0; j < y; j++)
			{
				if ((j >= row - cmatrix_middle) &&
					(j <= row + cmatrix_middle))
					sum += cur_col[j + i] * cmatrix[j];
			}
			dest_col[row + i] = (short)(0.5f + sum / scale);
		}
	}
	else
	{
		/* for the edge condition, we only use available info and scale to one */
		for (row = 0; row < cmatrix_middle; row++)
		{
			/* find scale factor */
			scale=0;
			for (j = cmatrix_middle - row; j<cmatrix_length; j++)
				scale += cmatrix[j];

			sum = 0;
			for (j = cmatrix_middle - row; j<cmatrix_length; j++)
			{
				sum += cur_col[(row + j-cmatrix_middle) + i] * cmatrix[j];
			}
			dest_col[row + i] = (short)(0.5f + sum / scale);

		}
		/* go through each pixel in each col */
		dest_col_p = dest_col + row;
		for (; row < y-cmatrix_middle; row++)
		{
			cur_col_p = (row - cmatrix_middle) + cur_col;

			sum = 0;
			cmatrix_p = cmatrix;
			ctable_p = ctable;

			for (j = cmatrix_length; j>0; j--)
			{
				sum += *(ctable_p + (*cur_col_p - SHRT_MIN));
				cur_col_p++;
				ctable_p += IMAGE_VALUE_LEN;
			}
			
			*(dest_col_p++) = (short)(0.5f + sum);
		}

		/* for the edge condition , we only use available info, and scale to one */
		for (; row < y; row++)
		{
			/* find scale factor */
			scale=0;
			for (j = 0; j< y-row + cmatrix_middle; j++)
				scale += cmatrix[j];

			sum = 0;
			for (j = 0; j<y-row + cmatrix_middle; j++)
			{
				sum += cur_col[(row + j-cmatrix_middle) + i] * cmatrix[j];
			}
			dest_col[row + i] = (short)(0.5f + sum / scale);
		}
	}
}


//------------------------------------------------------------------------
// Memory Util
template <class AnyType>
void CMyDIP::mem2dAlloc(AnyType **&pAnyType, int nWidth, int nHeight)
{
	pAnyType = new AnyType*[nHeight];

	for (int i=0; i<nHeight; i++)
	{
		pAnyType[i] = new AnyType[nWidth];
		memset(pAnyType[i], 0, sizeof(AnyType)*nWidth);
	}
}
template <class AnyType>
void CMyDIP::mem2dClear(AnyType **&pAnyType, int nWidth, int nHeight)
{
	for (int i=0; i<nHeight; i++)
		memset(pAnyType[i], 0, sizeof(AnyType)*nWidth);
}
template <class AnyType>
void CMyDIP::mem2dCopy(AnyType **&pDst, AnyType **&pSrc, int nWidth, int nHeight)
{
	for (int i=0; i<nHeight; i++)
		memcpy(pDst[i], pSrc[i], sizeof(AnyType)*nWidth);
}
template <class AnyType>
void CMyDIP::mem2dDelete(AnyType **&pAnyType, int nHeight)
{
	if (pAnyType!=NULL)
	{
		for (int i=0; i<nHeight; i++)
			delete [] pAnyType[i];

		delete [] pAnyType;
		pAnyType = NULL;
	}
}
template <class AnyType>
void CMyDIP::memDelete(AnyType **&pAnyType)
{
	if (pAnyType!=NULL)
	{
		delete [] pAnyType;
		pAnyType = NULL;
	}
}
template <class AnyType>
void CMyDIP::memDelete(AnyType *&pAnyType)
{
	if (pAnyType!=NULL)
	{
		delete [] pAnyType;
		pAnyType = NULL;
	}
}