#pragma once

#define IMAGE_VALUE_LEN (SHRT_MAX - SHRT_MIN + 1)
#define IMAGE_VALUE_MIN SHRT_MIN
#define IMAGE_VALUE_MAX SHRT_MAX


class CMyDIP
{
public:
	CMyDIP(void);
	~CMyDIP(void);

public:
	BYTE *m_pColorLut;				//Auto Level Width 조절 룩업 테이블
	int m_nColorLutSize;			//룩업테이블 크기
	int *m_histo;					//histogram

public:
	int DownSampling(short* src, short* dst, int w, int h);
	void CreateThumbnail(short* src, short* dst, int W, int H, int ratio);
	void findMinMaxValue(short* img, int &minValue, int &maxValue, int width, int height, int &abcVal);
	static void findImageWWL(short* img, int &minValue, int &maxValue, int width, int height, double dMinRatio = 0.007, double dMaxRatio = 0.007);
	static void findImageWWL(CString strImgPath, int &minValue, int &maxValue, int width, int height, double dMinRatio = 0.007, double dMaxRatio = 0.007);
	void CreateLutTable(int m_nWidth, int m_nHeight, short minVal, short maxVal);
	int AutoLevelWidth(short* pRawData, unsigned char * pViewData, int m_nWidth, int m_nHeight);
	int AutoLevelWidth(short* pRawData, unsigned char * pViewData, int m_nWidth, int m_nHeight, short minVal, short maxVal);
	void initJPGMem(int w, int h);
	void freeJPGMem();
	void InitLUT();
	void freeLUT();

	/************************************************************************/
	/* Noise Reudction                                                      */
	/************************************************************************/
	void CMyDIP::MedianFilter(short * Img, int Width, int Height, int iteration);
	void CMyDIP::HybridMedianFilter(short *Img, int Width, int Height);
	void CMyDIP::HybridMedianFilterFast(short *pSrc, const int nWidth, const int nHeight);
	inline int CMyDIP::FindMedian(int *pSrc, const int nSize);
	/************************************************************************/
	/* Edge Enhancement                                                     */
	/************************************************************************/
	/*** Unsharp Mask ***/
	bool	UnsharpMask(short *pSrc, short *pDst, int nWidth, int nHeight,
		float radius = 3.0, float amount = 1.7, int threshold = 0);

	/*** Gussian Blue ***/
	bool	GaussianBlur(short **pSrc, short **pDst, int nWidth, int nHeight,
		float radius /*= 1.0f*/);

protected:
	int		gen_convolve_matrix (double radius, double **cmatrix_p);
	double*	gen_lookup_table (double *cmatrix, int cmatrix_length);
	void blur_line (double *ctable, double *cmatrix, int cmatrix_length, short* cur_col, short* dest_col, int y);

	// Memory Util
	template <class AnyType>
	void mem2dAlloc(AnyType **&pAnyType, int nWidth, int nHeight);
	template <class AnyType>
	void mem2dClear(AnyType **&pAnyType, int nWidth, int nHeight);
	template <class AnyType>
	void mem2dCopy(AnyType **&pDst, AnyType **&pSrc, int nWidth, int nHeight);
	template <class AnyType>
	void mem2dDelete(AnyType **&pAnyType, int nHeight);
	template <class AnyType>
	void memDelete(AnyType **&pAnyType);
	template <class AnyType>
	void memDelete(AnyType *&pAnyType);
};


#define DETECTOR_WIDTHBYTES(bits) (((bits) + 31) / 32 * 4)
#define GetGrayValueIn16BitLut(pColorLut, p16BitIndex) pColorLut[p16BitIndex-SHRT_MIN]