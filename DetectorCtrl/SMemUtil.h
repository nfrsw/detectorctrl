#pragma once

#ifndef __SMEMUTIL_H
#define __SMEMUTIL_H

#include <math.h>
#define _USE_MATH_DEFINES

//------------------------------------------------------------------------------
// 시간 측정 1
#define TCHECK			time_t tEnd;
#define TCHECK_S		tEnd = clock();
#define TCHECK_E(str)	{tEnd = clock() - tEnd;		if (str!="") TRACE(" %4d (ms)-%s", tEnd, str);}


//------------------------------------------------------------------------------
// 시간 측정 2
#define timeCheck_Init			\
	LONGLONG timeCheck_valFreq, timeCheck_valStart, timeCheck_valEnd;	\
	QueryPerformanceFrequency((PLARGE_INTEGER)&timeCheck_valFreq);

#define timeCheck_Start			\
	{ QueryPerformanceCounter((PLARGE_INTEGER)&timeCheck_valStart); };

#define timeCheck_End			\
	{ QueryPerformanceCounter((PLARGE_INTEGER)&timeCheck_valEnd); };

#define timeCheck_msCost		\
	( ((DOUBLE)(timeCheck_valEnd - timeCheck_valStart) / timeCheck_valFreq) * 1000)

#define timeCheck_EndMsg(strMsg)	\
	{ QueryPerformanceCounter((PLARGE_INTEGER)&timeCheck_valEnd); CString strText; strText.Format(L"%s : %lf", strMsg, timeCheck_msCost); AfxMessageBox(strText); }


//------------------------------------------------------------------------
// Memory Util
template <class AnyType>
void mem4dAlloc(AnyType ****&pAnyType, int x, int y, int z, int k)
{
	pAnyType = new AnyType***[k];

	for (int n=0; n<k; n++)
	{
		pAnyType[n] = new AnyType**[z]

		for (int i=0; i<z; i++)
		{
			pAnyType[n][i] = new AnyType*[y];

			for (int j=0; j<y; j++)
			{
				pAnyType[n][i][j] = new AnyType[x];
				memset(pAnyType[n][i][j], 0, sizeof(AnyType)*x);
			}
		}
	}
}
template <class AnyType>
void mem4dDelete(AnyType ****&pAnyType, int k, int z, int y)
{
	for (int n=0; n<k; n++)
	{
		for (int i=0; i<z; i++)
		{
			for (int j=0; j<y; j++)
			{
				delete [] pAnyType[n][i][j];
			}

			delete pAnyType[n][i];
		}

		delete [] pAnyType[n];
	}

	delete [] pAnyType;
}


template <class AnyType>
void mem3dAlloc(AnyType ***&pAnyType, int x, int y, int z)
{
	pAnyType = new AnyType**[z];

	//for (int i=0; i<z; i++)
	parallel_for(0, z, [&](int i)
	{
		pAnyType[i] = new AnyType*[y];

		for (int j=0; j<y; j++)
		{
			pAnyType[i][j] = new AnyType[x];
			memset(pAnyType[i][j], 0, sizeof(AnyType)*x);
		}
	});
}

template <class AnyType>
void mem3dClear(AnyType ***&pAnyType, int x, int y, int z)
{
	for (int i=0; i<z; i++)
	{
		for (int j=0; j<y; j++)
		{
			memset(pAnyType[i][j], 0, sizeof(AnyType)*x);
		}
	}
}

template <class AnyType>
void mem3dCopy(AnyType ***&pDst, AnyType ***&pSrc, int x, int y, int z)
{
	for (int i=0; i<z; i++)
	{
		for (int j=0; j<y; j++)
		{
			memcpy(pDst[i][j], pSrc[i][j], sizeof(AnyType)*x);
		}
	}
}

template <class AnyType>
void mem3dDelete(AnyType ***&pAnyType, int y, int z)
{
	//for (int i=0; i<z; i++)
	parallel_for(0, z, [&](int i)
	{
		for (int j=0; j<y; j++)
		{
			delete [] pAnyType[i][j];
		}

		delete pAnyType[i];
	});

	delete [] pAnyType;
}

template <class AnyType>
void mem2dAlloc(AnyType **&pAnyType, int nWidth, int nHeight)
{
	pAnyType = new AnyType*[nHeight];

	for (int i=0; i<nHeight; i++)
	{
		pAnyType[i] = new AnyType[nWidth];
		//memset(pAnyType[i], 0, sizeof(AnyType)*nWidth);
	}
}
template <class AnyType>
void mem2dClear(AnyType **&pAnyType, int nWidth, int nHeight)
{
	for (int i=0; i<nHeight; i++)
		memset(pAnyType[i], 0, sizeof(AnyType)*nWidth);
}
template <class AnyType>
void mem2dCopy(AnyType **&pDst, AnyType **&pSrc, int nWidth, int nHeight)
{
	for (int i=0; i<nHeight; i++)
		memcpy(pDst[i], pSrc[i], sizeof(AnyType)*nWidth);
}
template <class AnyType>
void mem2dDelete(AnyType **&pAnyType, int nHeight)
{
	if (pAnyType!=NULL)
	{
		for (int i=0; i<nHeight; i++)
			delete [] pAnyType[i];

		delete [] pAnyType;
		pAnyType = NULL;
	}
}
template <class AnyType>
void memDelete(AnyType **&pAnyType)
{
	if (pAnyType!=NULL)
	{
		delete [] pAnyType;
		pAnyType = NULL;
	}
}
template <class AnyType>
void memDelete(AnyType *&pAnyType)
{
	if (pAnyType!=NULL)
	{
		delete [] pAnyType;
		pAnyType = NULL;
	}
}

template<class AnyType>
AnyType*** Get3DPtr(AnyType **&pImage, int nWidth, int nHeight, int nImageNum)
{
	AnyType ***p3dPtr = NULL;
	p3dPtr = new AnyType**[nImageNum];

	for (int k=0; k<nImageNum; k++)
	{
		p3dPtr[k] = new AnyType * [nHeight];
		for (int y=0; y<nHeight; y++)
		{
			p3dPtr[k][y] = &pImage[k][nWidth*y];
		}
	}

	return p3dPtr;
}

template<class AnyType>
void Delete3DPtr(AnyType ***&p3dPtr, int nImageNum)
{
	for (int k=0; k<nImageNum; k++)
	{
		delete [] p3dPtr[k];
	}

	delete [] p3dPtr;
}

template<class AnyType>
AnyType** Get2DPtr(AnyType *&pImage, int nWidth, int nHeight)
{
	AnyType **p2dPtr = NULL;
	p2dPtr = new AnyType*[nHeight];

	for (int y=0; y<nHeight; y++)
	{
		p2dPtr[y] = &pImage[nWidth*y];
	}

	return p2dPtr;
}

template<class AnyType>
void Delete2DPtr(AnyType **&p2dPtr)
{
	delete [] p2dPtr;
}

template<class AnyType>
void ImageCopy(AnyType *pImageDst, AnyType *pImageSrc, int nWidth, int nHeight)
{
	if (pImageDst!=NULL && pImageSrc!=NULL)
	{
		memcpy(pImageDst, pImageSrc, nWidth*nHeight*sizeof(AnyType));
	}
}


#endif