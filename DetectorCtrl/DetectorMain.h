#pragma once

#include "VADAVLib.h"
#include <msclr\marshal_cppstd.h>

using namespace msclr::interop;
using namespace System;
using namespace System::Runtime::InteropServices;

namespace DetectorCtrl
{
	enum CAPTURE_MODE
	{
		RADIOGRAPHY = 0,
		FLOUROSCOPY = 1
	};

	enum START_FRAME_MODE
	{
		DARK,
		BRIGHT
	};

	enum DARK_FRAME_STEP
	{
		READY,
		OFFSET_DONE,
		AIR_DONE
	};

	public ref class DetectorMain
	{
	protected:
		CVADAVLib * m_vadavLib;

		int avgNumber;
		int avgIndex;
		short **avgBuffer;
		int dutyNumber;
		int exposureTime;
		CAPTURE_MODE captureMode;
		START_FRAME_MODE startFrameMode;
		DARK_FRAME_STEP currentDarkFrameStep;

	public:
		DetectorMain();

		bool DetectorInit();
		void DetectorClose();
		bool DetectorReady();
		bool GetDarkOffset();
		bool GetDarkAir();
		bool DetectorAbort();

		void RegisterImageAcquiredProc();
		void RegisterWriteLogRequestedProc();

		void SetCaptureMode(int mode);
		
		void SetAVGNumber(int avgNumber);
		void SetDutyNumber(int dutyNumber);
		void SetExposureTime(int exposureTime);

		void EnqueueDemoImageBuffer(cli::array<byte> ^imageBuffer, int width, int height, int nBytesPerPixel);

		void PreProcessImage(short *outImageBuffer, int width, int height);
		void AverageImage(short *outImageBuffer, int width, int height);
		void CorrrectionImage(short *outImageBuffer, int width, int height);
		void EnhanceImage(short *outImageBuffer, int width, int height);
		void DSAMaskImage(short *outImageBuffer, int width, int height);

		delegate void CallerImageAcquiredProc(short *imageBuffer, int width, int height, int bytesPerPixel);
		delegate void _ImageAcquired(cli::array<byte> ^imageBuffer, int width, int height, int bytesPerPixel);
		event _ImageAcquired^ ImageAcquired;
		void ImageAcquiredProc(short* imageBuffer, int width, int height, int bytesPerPixel);
		void ImageAcquiredProcForBright(short* imageBuffer, int width, int height, int bytesPerPixel);
		void ImageAcquiredProcForDark(short* imageBuffer, int width, int height, int bytesPerPixel);
		void NotifyImageAcquired(short *finalImageBuffer, int width, int height, int bytesPerPixel);

		delegate void CallerWriteLogRequestedProc(CString logText);
		delegate void _WriteLogRequested(String ^logText);
		event _WriteLogRequested^ WriteLogRequested;
		void WriteLogRequestedProc(CString logText);
		void NotifyWriteLogRequested(String ^logText);

		void SetAVGBuffer(int avgIndex, short* imageBuffer, int bufferLength);
		bool AllocAVGBuffer(int avgNumber, int bufferLength);
		void FreeAVGBuffer();
	};
}