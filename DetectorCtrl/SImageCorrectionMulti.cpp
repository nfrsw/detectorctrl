#include "StdAfx.h"
#include "SImageCorrectionMulti.h"

#include "SMemUtil.h"
// #include "SImageFilter.h"
//#include "SImageIO.h"

#include <math.h>


CSImageCorrectionMulti::CSImageCorrectionMulti(void)
{
	m_pbDeadPixelMap = NULL;
	m_dBadPixelThreshold = DEFAULT_BADPIXEL_THRESHOLD;

	m_nWidth = -1;
	m_nHeight = -1;

	m_pdVoidFractionCircleRate = NULL;
	m_pbVoidFractionRoi = NULL;
	m_pVoidFractionCircleInfo = NULL;

	m_pdFullWater = NULL;
	m_pdFullAir = NULL;

}

CSImageCorrectionMulti::~CSImageCorrectionMulti(void)
{
	ClearCorrectionImageSet();
}

void CSImageCorrectionMulti::ClearCorrectionImageSet()
{
	memDelete(m_pbDeadPixelMap);

	for (int k=0; k<m_vecCorImgSet.size();k++)
	{
		delete m_vecCorImgSet[k];
	}

	m_vecCorImgSet.clear();
	m_vecDeadPixelList.clear();

	memDelete(m_pdVoidFractionCircleRate);
	memDelete(m_pbVoidFractionRoi);
	memDelete(m_pVoidFractionCircleInfo);

	memDelete(m_pdFullWater);
	memDelete(m_pdFullAir);
}

#include <ppl.h>
using namespace Concurrency;
void CSImageCorrectionMulti::GetIntegrationImage(short **pSrc, UINT64 **pDst, int nWidth, int nHeight)
{
	// 가로 Integration
	parallel_for(0, nHeight, [&](int y){
	//for (int y=0; y<nHeight; y++)
	{
		pDst[y][0] = pSrc[y][0];

		for (int x=1; x<nWidth;  x++)
		{
			pDst[y][x] = pDst[y][x-1] + pSrc[y][x];
		}
	}
	});

	// 세로 Integration
	parallel_for(1, nWidth, [&](int x){
	//for (int x=0; x<nWidth;  x++)
	{
		for (int y=1; y<nHeight; y++)
		{
			pDst[y][x] += pDst[y-1][x];
		}
	}
	});
}

void CSImageCorrectionMulti::GetAverageImage(short *pDst, int nWidth, int nHeight, int nWSize)
{
	short **pDst2d = Get2DPtr(pDst, nWidth, nHeight);

	UINT64 **pIntegration2d = NULL;
	mem2dAlloc(pIntegration2d, nWidth, nHeight);

	// Get Integration Image
	GetIntegrationImage(pDst2d, pIntegration2d, nWidth, nHeight);

	nWSize = nWSize / 2;
	int nWCnt = (nWSize*2)*(nWSize*2);

	// Get Average Image
	//parallel_for(nWSize, nHeight-nWSize, [&](int y){
	for (int y=nWSize; y<nHeight-nWSize; y++)
	{
		int nY1 = y-nWSize;
		int nY2 = y+nWSize;

		for (int x=nWSize; x<nWidth-nWSize; x++)
		{
			int nX1 = x-nWSize;
			int nX2 = x+nWSize;
			pDst2d[y][x] = (pIntegration2d[nY2][nX2] - pIntegration2d[nY1][nX2] - pIntegration2d[nY2][nX1] + pIntegration2d[nY1][nX1])/nWCnt;
		}
	}
	//});

	// 예외처리
	for (int y=0; y<nWSize; y++)
	{
		int nY1 = 0;
		int nY2 = y+nWSize;

		int nX1 = 0;
		for (int x=0; x<nWSize; x++)
		{
			int nX2 = x+nWSize;

			nWCnt = nX2*nY2;
			pDst2d[y][x] = (pIntegration2d[nY2][nX2] - pIntegration2d[nY1][nX2] - pIntegration2d[nY2][nX1] + pIntegration2d[nY1][nX1])/nWCnt;
		}

		int nX2 = nWidth-1;
		for (int x=nWidth-nWSize; x<nWidth; x++)
		{
			int nX1 = x-nWSize;

			nWCnt = (nX2-nX1)*nY2;
			pDst2d[y][x] = (pIntegration2d[nY2][nX2] - pIntegration2d[nY1][nX2] - pIntegration2d[nY2][nX1] + pIntegration2d[nY1][nX1])/nWCnt;
		}
	}

	for (int y=nHeight-nWSize; y<nHeight; y++)
	{
		int nY1 = y-nWSize;
		int nY2 = nHeight-1;

		int nX1 = 0;
		for (int x=0; x<nWSize; x++)
		{
			int nX2 = x+nWSize;

			nWCnt = nX2*(nY2-nY1);
			pDst2d[y][x] = (pIntegration2d[nY2][nX2] - pIntegration2d[nY1][nX2] - pIntegration2d[nY2][nX1] + pIntegration2d[nY1][nX1])/nWCnt;
		}

		int nX2 = nWidth-1;
		for (int x=nWidth-nWSize; x<nWidth; x++)
		{
			int nX1 = x-nWSize;

			nWCnt = (nX2-nX1)*(nY2-nY1);
			pDst2d[y][x] = (pIntegration2d[nY2][nX2] - pIntegration2d[nY1][nX2] - pIntegration2d[nY2][nX1] + pIntegration2d[nY1][nX1])/nWCnt;
		}
	}

	Delete2DPtr(pDst2d);
	mem2dDelete(pIntegration2d, nHeight);
}

double CSImageCorrectionMulti::GetAverage(short *pSrc, int nWidth, int nHeight)
{
	int nBufSize = nWidth*nHeight;
	double dBufSize = nBufSize;

	double dAvg = 0;

	for (int k=0; k<nBufSize; k++)
	{
		dAvg += pSrc[k]/dBufSize;
	}

	return dAvg;
}

BOOL CSImageCorrectionMulti::CreateDeadPixelMap(short *pAir, short *pOff, int nWidth, int nHeight, BOOL *bDeadPixelMap)
{
// 	1. pAir2 = Median Filter 5x5 (pAir)
// 	2. CalculatePixelRate(pAir2, pdRate);
// 	3. Get Average Image (double) { Get Integration Image (pdRate) 포함 }
// 	4. Make Dead Pixel Map
// 	5. Make Dead Pixel Vector : GetValidNeighborIndex()

	short *pMedAir = new short [nWidth*nHeight];
	short *pMedOff = new short [nWidth*nHeight];

//	timeCheck_Init;

//	timeCheck_Start;
	MedianFilterFast(pAir, pMedAir, nWidth, nHeight, MEDIAN_WINDOW_SIZE);
	MedianFilterFast(pOff, pMedOff, nWidth, nHeight, MEDIAN_WINDOW_SIZE);
//	timeCheck_EndMsg(L"MedianFilter-7x7 x2");

	//CSImageIO::SaveImage(L"C:\\NFRTemp\\20131122_MedAir_Fast.raw", pMedAir, nWidth*nHeight);
	//CSImageIO::SaveImage(L"C:\\NFRTemp\\20131122_MedOff_Fast.raw", pMedOff, nWidth*nHeight);



//	timeCheck_Start;
	GetAverageImage(pMedAir, nWidth, nHeight, AVERAGE_WINDOW_SIZE);
	GetAverageImage(pMedOff, nWidth, nHeight, AVERAGE_WINDOW_SIZE);
//	timeCheck_EndMsg(L"AverageImage-30x30 x 2");

	const int nBufSize = nWidth*nHeight;
	for (int k=0; k<nBufSize; k++)
	{
		double dPixelRateAir = fabs(pAir[k]/(double)pMedAir[k] - 1.0);
		double dPixelRateOff = fabs(pOff[k]/(double)pMedOff[k] - 1.0);

		if (dPixelRateAir > m_dBadPixelThreshold)
		{
			bDeadPixelMap[k] = TRUE;
		}
		else if (dPixelRateOff > m_dBadPixelThreshold)
		{
			bDeadPixelMap[k] = TRUE;
		}
	}

	delete [] pMedAir;
	delete [] pMedOff;

	m_vecDeadPixelList.clear();

//	timeCheck_Start;
	for (int k=0; k<nBufSize; k++)
	{
		if (bDeadPixelMap[k])
		{
			DeadPixelPoint tmpDP;
			tmpDP.nIdxCur = k;
			GetValidNeighborIndex(bDeadPixelMap, nWidth, nHeight, tmpDP.nIdxCur, tmpDP.nIdxL, tmpDP.nIdxR, tmpDP.fRateL, tmpDP.fRateR);
			m_vecDeadPixelList.push_back(tmpDP);
		}
	}
//	timeCheck_EndMsg(L"GetValidNeighborIndex");

//	CSImageIO::SaveImage(L"M:\\CorrectionTest\\Blood\\DeadPixelMap.raw", bDeadPixelMap, nBufSize);
	
	return TRUE;
}

void CSImageCorrectionMulti::DeadPixelCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight)
{
// 	if (!m_bValidCorrection)
// 	{
// 		return;
// 	}

	int nImageSize = nWidth*nHeight;

	if (pImageDst==NULL)
	{
		pImageDst = pImageSrc;
	}

	// Dead pixel correction
	if (m_vecDeadPixelList.size()>0)
	{
		vector<DeadPixelPoint>::iterator iterDeadPxl = m_vecDeadPixelList.begin();
		vector<DeadPixelPoint>::iterator iterDeadPxlEnd = m_vecDeadPixelList.end();

		while (iterDeadPxl != iterDeadPxlEnd)
		{
			pImageDst[(*iterDeadPxl).nIdxCur] = (*iterDeadPxl).fRateL*pImageSrc[(*iterDeadPxl).nIdxL] + (*iterDeadPxl).fRateR*pImageSrc[(*iterDeadPxl).nIdxR];
			++iterDeadPxl;
		}
	}
}

void CSImageCorrectionMulti::FlatFieldCorrection(short *pImageSrc, short *pImageDst, int nWidth, int nHeight, short *pImageOff /*= NULL*/)
{


	if (pImageSrc==NULL)
	{
		return;
	}

	UINT nImageSize = nWidth*nHeight;

	if (m_vecCorImgSet.size()<=0)
	{
		if (pImageDst!=NULL)
			memcpy(pImageDst, pImageSrc, nImageSize*sizeof(short));

		return;
	}



	if (pImageDst==NULL)
	{
		pImageDst = pImageSrc;
	}

	int nVecSize = m_vecCorImgSet.size();
	

	if (nVecSize>0)
	{
		if (pImageOff==NULL)
		{
			pImageOff = m_vecCorImgSet[0]->pOff;
		}

		//for (int k=0; k<nImageSize; k++)
		parallel_for((UINT)0, nImageSize, [&](UINT k){
		{
			int nCurDiffValue = pImageSrc[k] - pImageOff[k];
			double dUpRate = m_vecCorImgSet[nVecSize-1]->pdRate[k];
		
			for (int x=0; x<nVecSize; x++)
			{
				//if (m_pnVectorIdxMap[x] > nCurDiffValue)
				if (m_vecCorImgSet[x]->pDiff[k] > nCurDiffValue)
				{
					dUpRate = m_vecCorImgSet[x]->pdRate[k];

					if (x>0)
					{
						//double dUpValue = m_vecCorImgSet[x]->dTotalMean;
						//double dDnValue = m_vecCorImgSet[x-1]->dTotalMean;

						double dUpValue = m_vecCorImgSet[x]->pDiff[k];
						double dDnValue = m_vecCorImgSet[x-1]->pDiff[k];

						dUpValue = dUpValue - dDnValue;
						dDnValue = nCurDiffValue - dDnValue;

						dUpValue = dDnValue/dUpValue;

						double dDnRate = m_vecCorImgSet[x-1]->pdRate[k];

						dUpRate = dUpRate*dUpValue + dDnRate*(1.0-dUpValue);
					}

					break;
				}
			}
		
			pImageDst[k] = min(max(0, nCurDiffValue * dUpRate), 16383);
		}
		});
	}

	DeadPixelCorrection(pImageDst, NULL, nWidth, nHeight);


}

void CSImageCorrectionMulti::CalculatePixelRate(short *pAir, short *pOff, int nWidth, int nHeight, double *pdRate)
{
	UINT nImageSize = nWidth*nHeight;
	double dMean = 0;

	for (int k=0; k<nImageSize; k++)
	{
		dMean += (pAir[k] - pOff[k])/(double)nImageSize;
	}

	for (int k=0; k<nImageSize; k++)
	{
		pdRate[k] = dMean / (pAir[k] - pOff[k]);
	}
}

BOOL CSImageCorrectionMulti::InitCorrectionImage(short *pAir, short *pOff, int nWidth, int nHeight, short *pDefectMap /*= NULL*/)
{
	if (pAir==NULL || pOff==NULL)
	{
		return FALSE;
	}



	m_nWidth = nWidth;
	m_nHeight = nHeight;

	//------------------------------------------------------------------------


	ClearCorrectionImageSet();

	m_pbDeadPixelMap = new BOOL[nWidth*nHeight];
	double *pdRate = new double [nWidth*nHeight];
	memset(m_pbDeadPixelMap, 0, sizeof(BOOL)*nWidth*nHeight);

	//pDefectMap = NULL;
	if (pDefectMap!=NULL)
	{
		for (int y=0; y<nHeight; y++)
		for (int x=0; x<nWidth; x++)
		{
			if (pDefectMap[y*nWidth+x]>0)
			{
				m_pbDeadPixelMap[y*nWidth+(nWidth-x-1)] = TRUE;
			}
		}
	}


//	timeCheck_Init;
//	timeCheck_Start;
	CreateDeadPixelMap(pAir, pOff, nWidth, nHeight, m_pbDeadPixelMap);
//	timeCheck_EndMsg(L"Create Dead Pixel Map");

	//------------------------------------------------------------------------	
	//double *pdRate = new double [nWidth*nHeight];
//	timeCheck_Start;
	DeadPixelCorrection(pAir, NULL, nWidth, nHeight);
	DeadPixelCorrection(pOff, NULL, nWidth, nHeight);
//	timeCheck_EndMsg(L"Dead Pixel Correction x2");

// 	CSImageIO::SaveImage(L"C:\\NFRTemp\\Air_DP_20131121.raw", pAir, nWidth*nHeight);
// 	CSImageIO::SaveImage(L"C:\\NFRTemp\\Off_DP_20131121.raw", pOff, nWidth*nHeight);

	CalculatePixelRate(pAir, pOff, nWidth, nHeight, pdRate);
	
	CorrectionImageSet *tempImgSet = new CorrectionImageSet(pAir, pOff, pdRate, nWidth, nHeight);
	m_vecCorImgSet.push_back(tempImgSet);


	memDelete(pdRate);


	return TRUE;
}

void CSImageCorrectionMulti::AddDeadPixelPosition(int nX1, int nY1, int nX2, int nY2)
{
	if (m_nWidth <= 0 || m_nHeight <= 0)
	{
		return;
	}

	int nMin, nMax;
	nMin = min(nX1, nX2);
	nMax = max(nX1, nX2);
	nX1 = min(nMin, m_nWidth-1);
	nX2 = max(nMax, 0);

	nMin = min(nY1, nY2);
	nMax = max(nY1, nY2);
	nY1 = min(nMin, m_nHeight-1);
	nY2 = max(nMax, 0);

	for (int y=nY1; y<=nY2; y++)
	{
		for (int x=nX1; x<=nX2; x++)
		{
			m_pbDeadPixelMap[y*m_nWidth + x] = TRUE;
		}
	}

	m_vecDeadPixelList.clear();

	//	timeCheck_Start;
	int nBufSize = m_nWidth*m_nHeight;

	for (int k=0; k<nBufSize; k++)
	{
		if (m_pbDeadPixelMap[k])
		{
			DeadPixelPoint tmpDP;
			tmpDP.nIdxCur = k;
			GetValidNeighborIndex(m_pbDeadPixelMap, m_nWidth, m_nHeight, tmpDP.nIdxCur, tmpDP.nIdxL, tmpDP.nIdxR, tmpDP.fRateL, tmpDP.fRateR);
			m_vecDeadPixelList.push_back(tmpDP);
		}
	}
	//	timeCheck_EndMsg(L"GetValidNeighborIndex");
}

BOOL CSImageCorrectionMulti::AddCorrectionImage(short *pAir, short *pOff, int nWidth, int nHeight)
{
	if (m_nWidth!=nWidth || m_nHeight!=nHeight)
	{
		TRACE(L"Invalid image size !!");
		return FALSE;
	}

	double *pdRate = new double [nWidth*nHeight];
	DeadPixelCorrection(pAir, NULL, nWidth, nHeight);
	DeadPixelCorrection(pOff, NULL, nWidth, nHeight);
	CalculatePixelRate(pAir, pOff, nWidth, nHeight, pdRate);

	CorrectionImageSet *tempImgSet = new CorrectionImageSet(pAir, pOff, pdRate, nWidth, nHeight);
	m_vecCorImgSet.push_back(tempImgSet);

	
	memDelete(pdRate);

	ApplyCorrectionImageSet();

	return TRUE;
}

void CSImageCorrectionMulti::ApplyCorrectionImageSet()
{
	for (int k=0; k<m_vecCorImgSet.size()-1; k++)
	for (int x=k+1; x<m_vecCorImgSet.size(); x++)
	{
		if (m_vecCorImgSet[k]->dTotalMean > m_vecCorImgSet[x]->dTotalMean)
		{
			pCorrectionImageSet pTemp = m_vecCorImgSet[k];
			m_vecCorImgSet[k] = m_vecCorImgSet[x];
			m_vecCorImgSet[x] = pTemp;
		}
	}

// 	memDelete(m_pnVectorIdxMap);
// 
// 	m_pnVectorIdxMap = new int[m_vecCorImgSet.size()];
// 
// 	for (int k=0; k<m_vecCorImgSet.size(); k++)
// 	{
// 		m_pnVectorIdxMap[k] = m_vecCorImgSet[k]->dTotalMean;
// 	}


// 	int nSize = 35;
// 	if (m_vecCorImgSet.size()==nSize)
// 	{
// 		TRACE(L"\nMeanValue ");
// 		for (int k=0; k<nSize; k++)
// 		{
// 			TRACE(L"%.2lf ", m_vecCorImgSet[k]->dTotalMean);
// 		}
// 		TRACE(L"\nDarkValue ");
// 		for (int k=0; k<nSize; k++)
// 		{
// 			TRACE(L"%.2lf ", m_vecCorImgSet[k]->dDarkMean);
// 		}
// 
// 		int nX[] = { 893,  894,  895,  896,  897,  898,  899};
// 		int nY[] = {1595, 1595, 1595, 1595, 1595, 1595, 1595};
// 
// 		for (int y=0; y<7; y++)
// 		{
// 			TRACE(L"\nPixelGain%d ", y);
// 			for (int k=0; k<nSize; k++)
// 			{
// 				TRACE(L"%.4lf ", m_vecCorImgSet[k]->pdRate[nY[y]*1536+nX[y]]);
// 			}
// 		}
// 	}
}

void CSImageCorrectionMulti::ChangeBadPixelThreshold(double dBadPixelThreshold)
{
	m_dBadPixelThreshold = dBadPixelThreshold;
}

inline double CSImageCorrectionMulti::GetValidDistance(BOOL *pImageDeadPxl, int nCurX, int nCurY, int nDir, int nWidth, int nHeight, int *nStart, int *nEnd, double &dDistMin, double &dDistLeft)
{
	int nCurStart[2] = {-1, -1};
	int nCurEnd[2] = {-1, -1};


	int nTermX, nTermY;
	int nLoopCount1 = 0, nLoopCount2 = 0;
	double dDistance = 0;

	switch (nDir)
	{
		case 0: nTermX = -1; nTermY = 1;	dDistance = 1.4142135;	break;
		case 1: nTermX =  1; nTermY = 1;	dDistance = 1.4142135;	break;
		case 2: nTermX = -1; nTermY = 0;	dDistance = 1.0;		break;
		case 3: nTermX =  0; nTermY = 1;	dDistance = 1.0;		break;
	}

	int nNextX = nCurX + nTermX;
	int nNextY = nCurY + nTermY;
	while (nNextX >= 0 && nNextX < nWidth && nNextY >= 0 && nNextY < nHeight && nLoopCount1*dDistance <= dDistMin)
	{
		nLoopCount1++;

		if (!pImageDeadPxl[nNextY*nWidth+nNextX])
		{
			nCurStart[0] = nNextX;
			nCurStart[1] = nNextY;
			break;
		}

		nNextX = nNextX + nTermX;
		nNextY = nNextY + nTermY;
	}

//	if (nCurStart[0]<0)
// 	{
// 		nLoopCount1 = 0;
// 	}

	nTermX *= -1;
	nTermY *= -1;

	nNextX = nCurX + nTermX;
	nNextY = nCurY + nTermY;
	while (nNextX >= 0 && nNextX < nWidth && nNextY >= 0 && nNextY < nHeight && nLoopCount2*dDistance <= dDistMin)
	{
		nLoopCount2++;

		if (!pImageDeadPxl[nNextY*nWidth+nNextX])
		{
			nCurEnd[0] = nNextX;
			nCurEnd[1] = nNextY;
			break;
		}

		nNextX = nNextX + nTermX;
		nNextY = nNextY + nTermY;
	}

//	if (nCurEnd[0]<0)
// 	{
// 		nLoopCount2 = 0;
// 	}

	if (dDistance*(nLoopCount1+nLoopCount2) < dDistMin && dDistance*(nLoopCount1+nLoopCount2) > 0)
	{
		nStart[0] = nCurStart[0];
		nStart[1] = nCurStart[1];
		nEnd[0] = nCurEnd[0];
		nEnd[1] = nCurEnd[1];

		dDistMin = dDistance*(nLoopCount1+nLoopCount2);
		dDistLeft = dDistance*nLoopCount1;
		return TRUE;
	}

	return FALSE;
}

// 유효값을 가지는 직선들 중 가장 가까운 거리에 있는 두점 index 반환
inline void CSImageCorrectionMulti::GetValidNeighborIndex(BOOL *pbDeadPixelMap, int nWidth, int nHeight, int nCurIdx, int &nIdxL, int &nIdxR, float &fRateL, float &fRateR)
{
	if (pbDeadPixelMap[nCurIdx])
	{
		int nImageSize = nWidth*nHeight;

		int nStartPos[4][2] = {{-1,-1},{-1,-1},{-1,-1},{-1,-1}};
		int nEndPos[4][2] = {{-1,-1},{-1,-1},{-1,-1},{-1,-1}};

		int nCurX = nCurIdx % nWidth;
		int nCurY = nCurIdx / nWidth;

		double dDistMin = 20;
		double dDistLeft = 0;
		int nDistMinIdx = 0;

		for (int nDir=0; nDir<4; nDir++)
		{
			if ( GetValidDistance(pbDeadPixelMap, nCurX, nCurY, nDir, nWidth, nHeight, nStartPos[nDir], nEndPos[nDir], dDistMin, dDistLeft) )
			{
				nDistMinIdx = nDir;
			}
		}

		nIdxL = -1;
		nIdxR = -1;
		if (nStartPos[nDistMinIdx][0]>=0 && nStartPos[nDistMinIdx][1]>=0)
		{
			nIdxL = nStartPos[nDistMinIdx][1]*nWidth+nStartPos[nDistMinIdx][0];
		}

		if (nEndPos[nDistMinIdx][0]>=0 && nEndPos[nDistMinIdx][1]>=0)
		{
			nIdxR = nEndPos[nDistMinIdx][1]*nWidth+nEndPos[nDistMinIdx][0];
		}

		fRateL = (dDistMin-dDistLeft)/dDistMin;
		fRateR = dDistLeft/dDistMin;
		//return nLValue * (dDistMin-dDistLeft)/dDistMin + nRValue * dDistLeft/dDistMin;

		if (nIdxL<0)
		{
			nIdxL = nIdxR;
			fRateL = fRateR = 0.5;
		}
		
		if (nIdxR<0)
		{
			nIdxR = nIdxL;
			fRateL = fRateR = 0.5;
		}

		if (nIdxL<0 && nIdxR<0)
		{
			nIdxL = nIdxR = 0;
			fRateL = fRateR = 0.5;
		}
	}
}


//------------------------------------------------------------------------
// Void Fraction (for Polaris-D80[제주대학교 기포율측정 장비])

void CSImageCorrectionMulti::SetVoidFraction(short *pFullAir, short *pFullWater, int nWidth, int nHeight, BOOL bIsCorrected /*= TRUE*/)
{
	if (m_nWidth!=nWidth || m_nHeight!=nHeight)
	{
		TRACE(L"Invalid image size !!");
		return;
	}

	memDelete(m_pdVoidFractionCircleRate);
	memDelete(m_pbVoidFractionRoi);
	memDelete(m_pVoidFractionCircleInfo);
	memDelete(m_pdFullWater);
	memDelete(m_pdFullAir);

	if (!bIsCorrected)
	{
		//DeadPixelCorrection(pFullAir, NULL, nWidth, nHeight);
		//DeadPixelCorrection(pFullWater, NULL, nWidth, nHeight);
		FlatFieldCorrection(pFullAir, NULL, nWidth, nHeight);
		FlatFieldCorrection(pFullWater, NULL, nWidth, nHeight);
	}
	
	m_pdVoidFractionCircleRate = new double [nWidth*nHeight];
	memset(m_pdVoidFractionCircleRate, 0, sizeof(double)*nWidth*nHeight);
	m_pbVoidFractionRoi = new BOOL [nWidth*nHeight];
	memset(m_pbVoidFractionRoi, 0, sizeof(BOOL)*nWidth*nHeight);
	m_pVoidFractionCircleInfo = new VoidFractionCircleInfo [nHeight];

	m_pdFullWater = new double [nWidth*nHeight];
	memset(m_pdFullWater, 0, sizeof(double)*nWidth*nHeight);
	m_pdFullAir = new double [nWidth*nHeight];
	memset(m_pdFullAir, 0, sizeof(double)*nWidth*nHeight);
	
	// Get Void Fraction ROI : m_pbVoidFractionRoi, m_pVoidFractionCircleInfo
	GetROI_VoidFraction(pFullAir, pFullWater, nWidth, nHeight, m_rectRoi);


	for (int y=m_rectRoi.top; y<=m_rectRoi.bottom; y++)
	{
		for (int x=m_rectRoi.left; x<=m_rectRoi.right; x++)
		{
			int k = nWidth*y + x;

			if (m_pbVoidFractionRoi[k])
			{
				m_pdFullAir[k] = pFullAir[k];
				m_pdFullWater[k] = pFullWater[k];

				double dLogBtm = log(m_pdFullAir[k]/m_pdFullWater[k]);

				double dCurX = fabs(m_pVoidFractionCircleInfo[y].dCenterX-x);
				double dCurY = m_pdVoidFractionCircleRate[k];//sqrt(m_pVoidFractionCircleInfo[y].dr2-dCurX*dCurX) * 2.0;
				double dCurAreaRate = dCurY / m_pVoidFractionCircleInfo[y].dTotArea;

				if (m_pdFullAir[k]/m_pdFullWater[k] <= 1.0)
				{
					m_pdVoidFractionCircleRate[k] = 0;
				}
				else
				{
					m_pdVoidFractionCircleRate[k] = dCurAreaRate / dLogBtm;
				}
			}
		}
	}

	return;
}

void CSImageCorrectionMulti::GetROI_VoidFraction(short *pFullAir, short *pFullWater, int nWidth, int nHeight, RECT &rectROI)
{
// 	rectROI.left = 175;		rectROI.right = 446;
// 	rectROI.top = 976;		rectROI.bottom = 977;

	int nPixelNum = nWidth*nHeight;

	for (int k=0; k<nPixelNum; k++)
	{
		m_pbVoidFractionRoi[k] = ((pFullAir[k]-pFullWater[k]) > 350);
	}

	MM_Erosion(m_pbVoidFractionRoi, m_pbVoidFractionRoi, nWidth, nHeight, 25);
	MM_Dilation(m_pbVoidFractionRoi, m_pbVoidFractionRoi, nWidth, nHeight, 25);

	int nLeft=nWidth-1, nRight=0, nTop=nHeight-1, nBtm=0;

	for (int y=0; y<nHeight; y++)
	{
		for (int x=0; x<nWidth; x++)
		{
			if (m_pbVoidFractionRoi[nWidth*y + x])
			{
				rectROI.top = y;
				y = nHeight;
				break;
			}
		}
	}

	for (int y=nHeight-1; y>=0; y--)
	{
		for (int x=0; x<nWidth; x++)
		{
			if (m_pbVoidFractionRoi[nWidth*y + x])
			{
				rectROI.bottom = y;
				y = 0;
				break;
			}
		}
	}

	for (int x=0; x<nWidth; x++)
	{
		for (int y=0; y<nHeight; y++)
		{
			if (m_pbVoidFractionRoi[nWidth*y + x])
			{
				rectROI.left = x;
				x = nWidth;
				break;
			}
		}
	}

	for (int x=nWidth-1; x>=0; x--)
	{
		for (int y=0; y<nHeight; y++)
		{
			if (m_pbVoidFractionRoi[nWidth*y + x])
			{
				rectROI.right = x;
				x = 0;
				break;
			}
		}
	}

 //	CString strPath = L"D:\\sin\\NanoFocusRay\\ProjectData\\제주대학교\\실험영상\\20131127_제주대\\SampleTest";
 //	CSImageIO::SaveImage(strPath + L"\\zVoid_Flag_NoCor.raw", m_pbVoidFractionRoi, nPixelNum);

	for (int y=0; y<nHeight; y++)
	{
		int nStartX = 0;
		int nEndX = 0;
		BOOL bOldState = FALSE;
		BOOL bCurState = FALSE;

		for (int x=0; x<nWidth; x++)
		{
			bCurState = m_pbVoidFractionRoi[nWidth*y + x];

			if (bCurState)
			{
				if (!bOldState)
				{
					nStartX = x;
				}
				else if (bOldState)
				{
					nEndX = x;
				}
			}
			else if (bOldState)
			{
				break;
			}

			bOldState = bCurState;
		}

		double dr = (nEndX - nStartX)/2.0;
		m_pVoidFractionCircleInfo[y].dr2 = dr*dr;
		m_pVoidFractionCircleInfo[y].dCenterX = (nEndX + nStartX)/2.0;

		//m_pVoidFractionCircleInfo[y].dTotArea = M_PI * m_pVoidFractionCircleInfo[y].dr2;
		double dTotArea = 0;
		for (int x=nStartX; x<=nEndX; x++)
		{
			int k = nWidth*y + x;
			double dCurX = fabs(m_pVoidFractionCircleInfo[y].dCenterX-x);
			m_pdVoidFractionCircleRate[k] = sqrt(m_pVoidFractionCircleInfo[y].dr2-dCurX*dCurX) * 2.0;

			dTotArea += m_pdVoidFractionCircleRate[k];
		}
		m_pVoidFractionCircleInfo[y].dTotArea = dTotArea;
	}
	
}

double CSImageCorrectionMulti::GetVoidFraction_Mean(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected /*= TRUE*/)
{
	if (pSrc==NULL)
	{
		return 0;
	}

	if (m_nWidth!=nWidth || m_nHeight!=nHeight)
	{
		TRACE(L"Invalid image size !!");
		return 0;
	}

	if (!bIsCorrected)
	{
		//DeadPixelCorrection(pSrc, NULL, nWidth, nHeight);
		FlatFieldCorrection(pSrc, NULL, nWidth, nHeight);
	}

	int nRoiHeight = m_rectRoi.bottom - m_rectRoi.top + 1;
	int nRoiWidth = m_rectRoi.right - m_rectRoi.left;

	double dAvgAir=0, dAvgWater=0, dAvgCur=0, dTotArea=nRoiWidth*nRoiHeight;
	double dTotVoidFraction = 0;

	for (int y=m_rectRoi.top; y<=m_rectRoi.bottom; y++)
	{
		for (int x=m_rectRoi.left; x<=m_rectRoi.right; x++)
		{
			int k = nWidth*y + x;

			if (m_pbVoidFractionRoi[k])
			{
				dAvgAir += m_pdFullAir[k]/dTotArea;
				dAvgWater += m_pdFullWater[k]/dTotArea;
				dAvgCur += pSrc[k]/dTotArea;
			}
		}
	}

	dTotVoidFraction = log(dAvgCur/dAvgWater) / log(dAvgAir/dAvgWater);

	if (dTotVoidFraction < 0)
	{
		dTotVoidFraction = 0.0;
	}
	else if (dTotVoidFraction > 1.0)
	{
		dTotVoidFraction = 1.0;
	}

	return dTotVoidFraction;
}

double CSImageCorrectionMulti::GetVoidFraction_Individual(short *pSrc, int nWidth, int nHeight, BOOL bIsCorrected /*= TRUE*/)
{
	if (pSrc==NULL)
	{
		return 0;
	}

	if (!bIsCorrected)
	{
		//DeadPixelCorrection(pSrc, NULL, nWidth, nHeight);
		FlatFieldCorrection(pSrc, NULL, nWidth, nHeight);
	}
	
	int nRoiHeight = m_rectRoi.bottom - m_rectRoi.top + 1;
	//int nRoiWidth = m_rectRoi.right - m_rectRoi.left;

	double dTotVoidFraction = 0;
	for (int y=m_rectRoi.top; y<=m_rectRoi.bottom; y++)
	{
		double dVoidFraction = 0;

		for (int x=m_rectRoi.left; x<=m_rectRoi.right; x++)
		{
			int k = nWidth*y + x;

			if (m_pbVoidFractionRoi[k])
			{
				double dCurTop = pSrc[k] / m_pdFullWater[k];
				//double dCurBtm = m_pdFullAir[k]/m_pdFullWater[k];

				if (dCurTop >= 1.0)// && dCurBtm > 1.0 )
				{
					dVoidFraction += ( log(dCurTop) * m_pdVoidFractionCircleRate[k] );
				}
			}
		}

		dTotVoidFraction += (dVoidFraction/nRoiHeight);

		//TRACE(L"%d : %.3lf\n", y, dVoidFraction);
	}
	
	if (dTotVoidFraction < 0)
	{
		dTotVoidFraction = 0.0;
	}
	else if (dTotVoidFraction > 1.0)
	{
		dTotVoidFraction = 1.0;
	}

	return dTotVoidFraction;
}

void CSImageCorrectionMulti::MM_Dilation(BOOL *pImgSrc1d, BOOL *pImgDst1d, int nWidth, int nHeight,
	const int nKernelSize /*=3*/, RECT *rectRoi /*=NULL*/)
{
	BOOL bSrcCopyFlag = FALSE;
	if (pImgSrc1d == pImgDst1d)
	{
		bSrcCopyFlag = TRUE;
		BOOL *pImgSrc1dTemp = pImgSrc1d;

		pImgSrc1d = new BOOL[nWidth*nHeight];
		memcpy(pImgSrc1d, pImgSrc1dTemp, sizeof(BOOL)*nWidth*nHeight);
	}

	BOOL **pImgSrc = Get2DPtr(pImgSrc1d, nWidth, nHeight);
	BOOL **pImgDst = Get2DPtr(pImgDst1d, nWidth, nHeight);


	const int nSize = nKernelSize*nKernelSize;
	const int nKSize2 = nKernelSize/2;

	//------------------------------------------------------------------------
	// ROI 설정
	int nL, nR, nT, nB;
	if (rectRoi != NULL)
	{
		nL = min(rectRoi->left, rectRoi->right);
		nR = max(rectRoi->left, rectRoi->right);
		nT = min(rectRoi->top, rectRoi->bottom);
		nB = max(rectRoi->top, rectRoi->bottom);
	}
	else
	{
		nL = 0;
		nR = nWidth;
		nT = 0;
		nB = nHeight;
	}

	// 	double dImageAverage = GetAverage(pImgSrc1d, nWidth, nHeight);
	// 	const BOOL bIsImageLevelLow = (dImageAverage < MEDIAN_HISTOGRAM_MAX/2);

	// 세로 : 왼쪽 끝 예외
	if (nL-nKSize2 < 0)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nL; x<nKSize2; x++)
			{
				BOOL bTrueFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bTrueFlag = pImgSrc[y+y2][x+x2];
						}

						if (bTrueFlag)
							break;
					}
					if (bTrueFlag)
						break;
				}

				pImgDst[y][x] = bTrueFlag;
			}
		}

		nL = nKSize2;
	}

	// 세로 : 오른쪽 끝 예외
	if (nR+nKSize2 > nWidth)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nWidth-nKSize2; x<nR; x++)
			{
				BOOL bTrueFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bTrueFlag = pImgSrc[y+y2][x+x2];
						}

						if (bTrueFlag)
							break;
					}
					if (bTrueFlag)
						break;
				}

				pImgDst[y][x] = bTrueFlag;
			}
		}

		nR = nWidth-nKSize2;
	}

	// 세로 : 위쪽 끝 예외
	if (nT-nKSize2 < 0)
	{
		for (int y=nT; y<nKSize2; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				BOOL bTrueFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bTrueFlag = pImgSrc[y+y2][x+x2];
						}

						if (bTrueFlag)
							break;
					}
					if (bTrueFlag)
						break;
				}

				pImgDst[y][x] = bTrueFlag;
			}
		}

		nT = nKSize2;
	}

	// 가로 : 아래쪽 끝 예외
	if (nB+nKSize2 > nHeight)
	{
		for (int y=nHeight-nKSize2; y<nB; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				BOOL bTrueFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bTrueFlag = pImgSrc[y+y2][x+x2];
						}

						if (bTrueFlag)
							break;
					}
					if (bTrueFlag)
						break;
				}

				pImgDst[y][x] = bTrueFlag;
			}
		}

		nB = nHeight-nKSize2;
	}

	// Median filter
	const int nMedCnt = (nKernelSize*nKernelSize)/2;
	parallel_for(nL, nR, [&](int x){
	//for (int x=nL; x<nR; x++)
	{
		int nTrueCnt = 0;

		// First Line
		int y = nT;
		for (int y2=-nKSize2; y2<=nKSize2; y2++)
		{
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nTrueCnt += pImgSrc[y+y2][x+x2];
			}
		}

		pImgDst[y][x] = nTrueCnt > 0;

		//
		int y2=0;
		for (y=nT+1; y<nB; y++)
		{
			y2 = y-nKSize2-1;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nTrueCnt -= pImgSrc[y2][x+x2];
			}
			y2 = y+nKSize2;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nTrueCnt += pImgSrc[y2][x+x2];
			}

			pImgDst[y][x] = nTrueCnt > 0;
		}
	}
	});


	Delete2DPtr(pImgSrc);
	Delete2DPtr(pImgDst);

	if (bSrcCopyFlag)
	{
		delete [] pImgSrc1d;
	}
}

void CSImageCorrectionMulti::MM_Erosion(BOOL *pImgSrc1d, BOOL *pImgDst1d, int nWidth, int nHeight,
	const int nKernelSize /*=3*/, RECT *rectRoi /*=NULL*/)
{
	BOOL bSrcCopyFlag = FALSE;
	if (pImgSrc1d == pImgDst1d)
	{
		bSrcCopyFlag = TRUE;
		BOOL *pImgSrc1dTemp = pImgSrc1d;

		pImgSrc1d = new BOOL[nWidth*nHeight];
		memcpy(pImgSrc1d, pImgSrc1dTemp, sizeof(BOOL)*nWidth*nHeight);
	}

	BOOL **pImgSrc = Get2DPtr(pImgSrc1d, nWidth, nHeight);
	BOOL **pImgDst = Get2DPtr(pImgDst1d, nWidth, nHeight);


	const int nSize = nKernelSize*nKernelSize;
	const int nKSize2 = nKernelSize/2;

	//------------------------------------------------------------------------
	// ROI 설정
	int nL, nR, nT, nB;
	if (rectRoi != NULL)
	{
		nL = min(rectRoi->left, rectRoi->right);
		nR = max(rectRoi->left, rectRoi->right);
		nT = min(rectRoi->top, rectRoi->bottom);
		nB = max(rectRoi->top, rectRoi->bottom);
	}
	else
	{
		nL = 0;
		nR = nWidth;
		nT = 0;
		nB = nHeight;
	}

	// 	double dImageAverage = GetAverage(pImgSrc1d, nWidth, nHeight);
	// 	const BOOL bIsImageLevelLow = (dImageAverage < MEDIAN_HISTOGRAM_MAX/2);

	// 세로 : 왼쪽 끝 예외
	if (nL-nKSize2 < 0)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nL; x<nKSize2; x++)
			{
				BOOL bFalseFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bFalseFlag = !pImgSrc[y+y2][x+x2];
						}

						if (bFalseFlag)
							break;
					}
					if (bFalseFlag)
						break;
				}

				pImgDst[y][x] = !bFalseFlag;
			}
		}

		nL = nKSize2;
	}

	// 세로 : 오른쪽 끝 예외
	if (nR+nKSize2 > nWidth)
	{
		for (int y=nT; y<nB; y++)
		{
			for (int x=nWidth-nKSize2; x<nR; x++)
			{
				BOOL bFalseFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bFalseFlag = !pImgSrc[y+y2][x+x2];
						}

						if (bFalseFlag)
							break;
					}
					if (bFalseFlag)
						break;
				}

				pImgDst[y][x] = !bFalseFlag;
			}
		}

		nR = nWidth-nKSize2;
	}

	// 세로 : 위쪽 끝 예외
	if (nT-nKSize2 < 0)
	{
		for (int y=nT; y<nKSize2; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				BOOL bFalseFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bFalseFlag = !pImgSrc[y+y2][x+x2];
						}

						if (bFalseFlag)
							break;
					}
					if (bFalseFlag)
						break;
				}

				pImgDst[y][x] = !bFalseFlag;
			}
		}

		nT = nKSize2;
	}

	// 가로 : 아래쪽 끝 예외
	if (nB+nKSize2 > nHeight)
	{
		for (int y=nHeight-nKSize2; y<nB; y++)
		{
			for (int x=nL; x<nR; x++)
			{
				BOOL bFalseFlag = FALSE;

				for (int y2=-nKSize2; y2<=nKSize2; y2++)
				{
					for (int x2=-nKSize2; x2<=nKSize2; x2++)
					{
						if (y+y2>0 && y+y2<nHeight && x+x2>0 && x+x2<nWidth)
						{
							bFalseFlag = !pImgSrc[y+y2][x+x2];
						}

						if (bFalseFlag)
							break;
					}
					if (bFalseFlag)
						break;
				}

				pImgDst[y][x] = !bFalseFlag;
			}
		}

		nB = nHeight-nKSize2;
	}

	// Median filter
	const int nMedCnt = (nKernelSize*nKernelSize)/2;
	parallel_for(nL, nR, [&](int x){
	//for (int x=nL; x<nR; x++)
	{
		int nFalseCnt = 0;

		// First Line
		int y = nT;
		for (int y2=-nKSize2; y2<=nKSize2; y2++)
		{
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nFalseCnt += !pImgSrc[y+y2][x+x2];
			}
		}

		pImgDst[y][x] = (nFalseCnt == 0);

		//
		int y2=0;
		for (y=nT+1; y<nB; y++)
		{
			y2 = y-nKSize2-1;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nFalseCnt -= !pImgSrc[y2][x+x2];
			}
			y2 = y+nKSize2;
			for (int x2=-nKSize2; x2<=nKSize2; x2++)
			{
				nFalseCnt += !pImgSrc[y2][x+x2];
			}

			pImgDst[y][x] = (nFalseCnt == 0);
		}
	}
	});


	Delete2DPtr(pImgSrc);
	Delete2DPtr(pImgDst);

	if (bSrcCopyFlag)
	{
		delete [] pImgSrc1d;
	}
}


/*
{
	// LinearLeastSquares
	double a[2] = {0,0};
	double b[2] = {0,0};

	// Coefficient - Left Line
	LinearLeastSquares(a[0], b[0], nSampleSize, &pTempPatchSum[by][nCurX-nSampleSize-1]);

	// Coefficient - Right Line
	LinearLeastSquares(a[1], b[1], nSampleSize, &pTempPatchSum[by][nCurX+2]);

	double YL = a[0]*(nSampleSize+2) + b[0];
	double YR = a[1]*(-1) + b[1];

	dBlockDiffX[by][bx] = YR - YL;

	if (fabs(dBlockDiffX[by][bx])>10)
	{
		dBlockDiffX[by][bx] = 0;
	}
}


void LinearLeastSquares(double &a, double&b, int nSampleSize, double *dArrSrcY, double *dArrSrcX /*=NULL* /)
{
	double dAvgX = 0;

	if (dArrSrcX != NULL)
	{
		dAvgX = 0;
		for (int x=0; x<nSampleSize; x++)
		{
			dAvgX += dArrSrcX[x] / nSampleSize;
		}
	}
	else
	{
		dAvgX = (nSampleSize+1)/2.0;
	}

	double dAvgY = 0;
	for (int x=0; x<nSampleSize; x++)
	{
		dAvgY += dArrSrcY[x] / nSampleSize;
	}

	double dSumMulDiffXY = 0;
	double dSumSquareX = 0;

	if (dArrSrcX != NULL)
	{
		for (int x=0; x<nSampleSize; x++)
		{
			dSumMulDiffXY += (dArrSrcY[x]-dAvgY)*(dArrSrcX[x]-dAvgX);
			dSumSquareX += (dArrSrcX[x]-dAvgX)*(dArrSrcX[x]-dAvgX);
		}
	}
	else
	{
		int nX = 1;
		for (int x=0; x<nSampleSize; x++, nX++)
		{
			dSumMulDiffXY += (dArrSrcY[x]-dAvgY)*(nX-dAvgX);
			dSumSquareX += (nX-dAvgX)*(nX-dAvgX);
		}
	}

	if (dSumSquareX!=0)
	{
		a = dSumMulDiffXY / dSumSquareX;
	}
	else
	{
		a = 0;
	}

	b = dAvgY - a*dAvgX;
}
*/