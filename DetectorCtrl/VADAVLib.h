// 7942CKCamera.h: interface for the CVADAVLib class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_7942CKCAMERA_H__66D85FF8_A626_4907_9F61_C4526106DF58__INCLUDED_)
#define AFX_7942CKCAMERA_H__66D85FF8_A626_4907_9F61_C4526106DF58__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxmt.h>
#include "VADAVIntf.h"
#include "SImageCorrectionMulti.h"
#include "MyDIP.h"

#define DETECTOR_1215
#define WIN64UNICODE

#define IMG_BUFFER_NUM	30
#define CUTTING_HEIGHT	20

typedef void(_stdcall *tVIMGACQ_CallBackProc)(short*, int, int, int);
typedef void(_stdcall *tVLOG_CallBackProc)(CString);

class CVADAVLib
{

public:
	inline bool IsInit();
	inline void LiveStart(bool bStart);
	inline unsigned int GetOriginSize();
	inline int GetImageSizeX();
	inline int GetImageSizeY();

	inline void ResetDetector(int iWell);
	void GetDetectorInfo(char* strFirmWare, char* strR1124, char* strR1125, char* strR1126);
	//	inline void SetInternalTrigger();

	CVADAVLib();
	virtual ~CVADAVLib();

	bool CameraInit(CWnd* pMainWnd, int iWell);
	bool CameraClose(void);
	// ko : ����
	bool CallbackInit();
	//ko ����
	bool pOnCameraSet(int& ExposureTime, int iBinning, int& iWidth, int& iHigh, int iWell, bool bInit);

	bool CameraSet(int& ExposureTime, int iBinning, int& iWid, int& iHigh, int iWell, bool bInit = false);
	void LiveEnd();

	bool CameraSnap(int ExposureTime, LPBYTE pTempImage8Bit = NULL, LPIMAGETYPE pImage = NULL);
	bool CameraSnap(int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE32Bits pImage, bool bPlus = false);
	int CameraSnapForLive(int iPreCapbuf, int ExposureTime, LPBYTE pTempImage8Bit = NULL, LPIMAGETYPE pImage = NULL, int* statusFlag = NULL);
	int CameraSnapForLive(int iPreCapbuf, int ExposureTime, LPBYTE pTempImage8Bit, LPIMAGETYPE32Bits pImage, bool bPlus = false);

	void TransferDataFromFramGrabber16Bits(LPIMAGETYPE pSourceImage16Bits, LPIMAGETYPE pTarketImage16Bits);
	void TransferDataFromFramGrabber32Bits(LPIMAGETYPE pSourceImage16Bits, LPIMAGETYPE32Bits pTarketImage32Bits, bool bPlus);



	tVADAV_InterfaceRec m_Intf;              // the library's interface record

	tVDACQ_CallBackRec *rACQ_CallBackRec;
	void On_ACQ_CallBack(tVDACQ_CallBackRec* AR);
	void OnCaptureRecv();
	void SetSize(int ImageSizeX, int ImageSizeY);
	int Snap(int iPre);
	int pOnSnap(int iPre);

	int      m_nFrameWidth,       // width of received frames
		m_nFrameHeight,      // height of received frames
		m_nImgWidth,         // width of saved images (projections)
		m_nImgHeight;        // height of saved images
	short   *m_FrameBuffer,       // frame's buffer
		*m_ImgBuffer[IMG_BUFFER_NUM];         // image's buffer
	short*	m_pAddress;
	int     *m_AverageBuffer,     // buffer to calculate average
		m_nAverageCount;     // count of averaged frames
	int      m_nAcqState,         // 1:have received at least one frame, 2:complete, <0:error
		m_nAcqFrameStart,    // frame's counter of queue's tail at the moment "Save ON"
		m_nAcqFrameStop,     // frame's counter of queue's tail at the moment "Save OFF"
		m_nAcqSaveProjTotal, // total number of saved projections
		m_nAcqTermination;   // -1:request, 1:termination starts, 1:termination is complete
	BOOL     m_bAcqSaveCmd,       // triggered by EventSave (Save ON/OFF)
		m_bAcqSave,          // actual current state of recording (Save ON/OFF)
		m_bEventSaveInt;     // TRUE if the event is internally created
	TCHAR    m_sSaveDir[256],     // directory to save dataseries
		m_sCmdStart[256],    // serial command<s> before aquisition start 
		m_sCmdFinish[256];   // serial command<s> after aquisition finish 

	HANDLE   m_hServiceThread,    // service thread (it receives events)
		m_hEventSave,        // from caller app or from menu: toggle Save ON/OFF
		m_hEventExit,        // from caller app or from menu: terminate
		m_hEventCnfm;        // out: confirm termination
	DWORD    m_nStartTimeTicks;   // moment of the first frame (to estimate fps)
	CRITICAL_SECTION    m_UpdatingStatus;    // prevents simultaneous read/write access to data-buffers

	int m_iCurSnap;
	int m_iNormalFrameWidth, m_iNormalFrameHeight;
	bool m_bBinningFlag, m_bOldBinningFlag;

	int* m_statusFlag;
	/*CPHION_Control *m_gbtCtrl;
	void setGBTCtrl(CPHION_Control *gbtCtrl);*/

	void VADAV_SetImageAcquiredProc(tVIMGACQ_CallBackProc IMGAcqProc);
	tVIMGACQ_CallBackProc rVIMGACQ_ImageAcquired;

	void VADAV_SetWriteLogRequestedProc(tVLOG_CallBackProc LoggerProc);
	tVLOG_CallBackProc rVLOG_WriteLogRequested;

	CSImageCorrectionMulti m_correctionMulti;
	CEvent *m_pEventWaitAcquireImage;
	bool m_bRunAcquiredImageChecker;
	void EnqueueDemoImageBuffer(byte* pDemoImageBuffer, int nWidth, int nHeight, int nBytesPerPixel);
	void GetImageSize(int &nWidth, int &nHeight);
	void CoAverageImage(short *outImageBuffer, short **inImageBuffer, int inImageCount, int inBufferLength);
	void CorrectionImage(short *outImageBuffer, short *inImageBuffer, int width, int height);
	void EnhanceImage(short *outImageBuffer, short *inImageBuffer, int width, int height);
	void DSAMaskImage(short *outImageBuffer, short *inImageBuffer, int width, int height);
	void SaveCalibrationData(CString filePath, short *buffer, int length, int sizeofType);

protected:
	CWnd * m_pMainWnd;
	int* m_piLookupTable;
	HANDLE Handle;
	bool m_bInit;
	int m_AcqWinWidth, m_AcqWinHeight;
	int m_ExposureTime, m_iBining;
	int m_iWell;
	LPIMAGETYPE m_ImageBuffer;
};


inline bool CVADAVLib::IsInit()
{
	return m_bInit;
}

inline void CVADAVLib::LiveStart(bool bStart)
{
	//	m_memoryforimageDims.depth = 20;
	// 	if(bStart)
	// 		GoLiveSeq(1, 1, 20, 1, 0, 1);
	// 	else
	// 		GoUnLive(1);
}

inline unsigned int CVADAVLib::GetOriginSize()
{
	m_Intf.rVDC_GetImageDim(&m_nImgWidth, &m_nImgHeight);
	return (sizeof(short)*m_nImgWidth*m_nImgHeight);
}
inline int CVADAVLib::GetImageSizeX()
{
	//m_Intf.rVDC_GetImageDim( &m_nImgWidth, &m_nImgHeight );
	m_Intf.rVDC_GetImageDim(&m_nImgHeight, &m_nImgWidth);
	return m_nImgWidth;
}
inline int CVADAVLib::GetImageSizeY()
{
	//m_Intf.rVDC_GetImageDim( &m_nImgWidth, &m_nImgHeight );
	m_Intf.rVDC_GetImageDim(&m_nImgHeight, &m_nImgWidth);
	return m_nImgHeight;
}

inline void CVADAVLib::ResetDetector(int iWell)
{
	m_iWell = iWell;
}
/*

inline void CVADAVLib::SetInternalTrigger()
{
if(SetTriggerSource(Internal_Software) != SUCCESS)
return ;

Sleep(2000);
}*/

#endif // !defined(AFX_7942CKCAMERA_H__66D85FF8_A626_4907_9F61_C4526106DF58__INCLUDED_)
