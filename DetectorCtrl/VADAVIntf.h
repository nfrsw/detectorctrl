/** *************************************************************** **
 **          Radiography Detector: application interface            **
 **  (c) VATech & E-Woo                                 by S.B.     **
 **  initial release: Jul-2007                                      **
 **  recent edition : Feb-2010                                      **
 ** *************************************************************** **/

#ifndef VADAVINTF_H_INCLUDED
#define VADAVINTF_H_INCLUDED

  ////// acquisition //////
  #pragma pack (4)
  // acquisition flags tVDACQ_CallBackRec.rFlags
  #define cVDACQ_FBright       1      // acquire bright frame (dark otherwise)
  #define cVDACQ_FTestPat      2      // acquire test pattern  (TFT only)
  #define cVDACQ_FRecentFrame  4      // retrieve recent frame (TFT only)
  #define cVDACQ_FBrightIgn   (1<<31) // ignore state of the bit 'Bright' for machine control (affects some TFT only)
  // acquisition event types tVDACQ_CallBackRec.rType
  #define cVDACQ_ETTrace       0      // information message from acquistion code
  #define cVDACQ_ETTraceT      1      // time-stamp and information message
  #define cVDACQ_ETErr         2      // an error from the library's acquisition code 
  #define cVDACQ_ETLastErr     3      // an error from Windows API, described by 'GetLastError'
  #define cVDACQ_ETWSAErr      4      // an error from Windows ocket 
  // acquisition events tVDACQ_CallBackRec.rEvent
  #define cVDACQ_EAbort        1      // application invokes VDACQ_Abort
  #define cVDACQ_EClose        2      // application invokes VDACQ_Close
  #define cVDACQ_ECapture      20     // a message from the library's acquisition code
  #define cVDACQ_ECapturePerc  21     // acquisition code tells percentage of completeness
  #define cVDACQ_ECaptureRecv  22     // acquisition code informs about new received data
  // framegrabber acquisition control flags tVDACQ_CallBackRec.rFGControl
  #define cVDACQ_FFGCStop      (1<<0) // FG control: stop acquisition
  #define cVDACQ_FFGCXOn       (1<<1) // FG control: XRay is 'On' 
                                      // keep this frame or add it to result when FGC_NoAve is OFF
  // acquisition feedback record   
  typedef struct {
    int    rFlags,             // combination of cVDACQ_Fxxxx
           rType,              // cVDACQ_ETxxx
           rEvent,             // cVDACQ_Exxx
           rSocket;            // 0:no relation to a 'socket'; otherwise socket's ID>0 (event's source ID) 
    TCHAR  rMsg[256];          // message (trace, wrn, err)
    int    rFrameWidth,        // full frame width
           rFrameHeight;       // full frame height
    short *rFrameBuffer;       // user supplied frame buffer "AFrameBuffer"
    union {
    int    rCaptureRows;       // # of received rows (for single frame acquisition)
    int    rCaptureFrames;     // # of received full frames (for framegrabber)
    };
    int    rCapturePercent;    // received data in percents
    void  *rUserCallBackProc,  // user supplied "ACallBackProc"
          *rUserParam;         // user supplied "AUserParam"
    int    rAborted;           // 0:none, 1:VDACQ_Abort, -1:internally
    void  *rPacketData;        // pointer to received packet; usually it is nil  
    int    rFGControl;         // frame-grabber's control flags
  } tVDACQ_CallBackRec;
    // 'socket' ID  #1: control-send (respective thread or real socket for Ethernet)
    //              #2: control-receive
    //              #3: data-receive
    // When callback is triggered by received message from detector it has non-nil field
    // PacketData. Normally an OEM application does not check received packets directly.
    
  // callback procedure  
  typedef void (_stdcall *tVDACQ_CallBackProc)( tVDACQ_CallBackRec* );

  // framegrabber parameters for 'VDACQ_SetFGRec', 'VDACQ_GetFGRec'
  #define cVDACQ_FG_None     0        // default acquisition (via Ethernet)
  #define cVDACQ_FG_NIIMAQ   1        // Nat Instr IMAQ
  #define cVDACQ_FG_AnyGrab  2        // VATech AnyGrabber
  #define cVDACQ_FG_FRM11    3        // FRM11 of DAQ systems
  #define cVDACQ_FGC_NoUIXOn (1<<0)   // disable UI XOn controls (when UI is used)
  #define cVDACQ_FGC_NoAve   (1<<1)   // disable calculation of the average frame
  #define cVDACQ_FGC_NoDesp  (1<<2)   // use simple median 3x3 when VDC_FDespeckle is set
  typedef struct {
    int    rType,              // one of VDACQ_FG_xxx 
           rFG_Control,        // one of VDACQ_FGC_xxx
           rCalFlags,          // combination of basic flags 'cVDC_Fxxxx'
           rRingBufs;          // number of ring-buffers (0:continuous memory queue)
    TCHAR  rInterfaceName[60];
  } tVDACQ_FGRec;

  ////// calibration //////
  // calibration flags
  #define cVDC_FCalOffs      1        // apply offset/dark calibration
  #define cVDC_FCalGain      2        // apply gain/bright calibration
  #define cVDC_FBadPixMap    4        // request application of bad pixels map
  #define cVDC_FDespeckle    8        // apply despeckle
  #define cVDC_FOptDist     16        // apply optical distortion correction
  #define cVDC_FAuxLnRecal  32        // apply auxiliary lines re-calibration
  #define cVDC_FTempDirIP   (1<<31)   // save temporary images in default image direcotory

  // calibration event types
  #define cVDC_ETTrace       0        // information message from calibration code
  #define cVDC_ETTraceT      1        // time-stamp and information message
  #define cVDC_ETErr         2        // an error from the library's calibration code 
  // calibration events
  #define cVDC_EAbort        1        // application invokes VDC_Abort
  #define cVDC_EClose        2        // application invokes VDC_Close
  #define cVDC_ECalib        20       // a message from the library's calibration code
  #define cVDC_ECalibPerc    21       // calibration code tells percentage of completeness
  // calibration feedback record   
  typedef struct {
    int    rFlags,             // combination of cVDC_Fxxxx
           rType,              // cVDC_ETxxx
           rEvent;             // cVDC_Exxx
    TCHAR  rMsg[256];          // message (trace, wrn, err)
    int    rFrameWidth,        // full frame width
           rFrameHeight,       // full frame height
           rStoredWidth,       // stored image width
           rStoredHeight,      // stored image height
           rCalibPercent;      // amount of processed data in %
    short *rFrameBuffer,       // received frame data
          *rImageBuffer;       // cropped image
    void  *rUserCallBackProc,  // user supplied "ACallBackProc"
          *rUserParam;         // user supplied "AUserParam"
    int    rAborted;           // 1: set by VDC_Abort; -1:internally
  } tVDC_CallBackRec;
  // callback procedure  
  typedef void (_stdcall *tVDC_CallBackProc)( tVDC_CallBackRec* );

  typedef struct {
    int  rImgCutLeft,          // cut left after rotation & flip 
         rImgCutTop,           // cut top after rotation & flip
         rImgCutRight,         // cut right after rotation & flip
         rImgCutBottom,        // cut bottom after rotation & flip
         rRotation,            // 0:none, 1:90 deg CCW, 2:90 deg CW
         rFlip;                // bit #0: flip horz, bit #1: flip vert 
  } tVDC_ImgCut;

  ///// image process /////
  // img process flags
  #define cVDIP_FDespeckle    1       // apply despeckle
  #define cVDIP_F3PCurve      2       // apply 3-points curve
  #define cVDIP_FUSMask       4       // apply all unsharpen filters
  // img process event types
  #define cVDIP_ETTrace       0       // information message from 'enhancement' code
  #define cVDIP_ETTraceT      1       // time-stamp and information message
  #define cVDIP_ETErr         2       // an error from the library's 'enhancement' code 
  // img process events
  #define cVDIP_EAbort        1       // application invokes VDIP_Abort 
  #define cVDIP_EClose        2       // application invokes VDIP_Close
  #define cVDIP_EEnh          20      // information message from 'enhancement' code
  #define cVDIP_EEnhPerc      21      // 'enhancement' code tells percentage of completeness
  // img process feedback record   
  typedef struct {
    int    rFlags,             // combination of cVDC_Fxxxx
           rType,              // cVDC_ETxxx
           rEvent;             // cVDC_Exxx
    TCHAR  rMsg[256];          // message (trace, wrn, err)
    int    rStoredWidth,       // stored image width
           rStoredHeight,      // stored image height
           rEnhPercent,        // processed data in %
           rModeNumber;        // img process mode
    short *rImageBuffer;       // image buffer
    void  *rUserCallBackProc,  // user supplied "ACallBackProc"
          *rUserParam;         // user supplied "AUserParam"
    int    rAborted;           // 1: set by VDIP_Abort; -1:internally
  } tVDIP_CallBackRec;
  // callback procedure  
  typedef void (_stdcall *tVDIP_CallBackProc)( tVDIP_CallBackRec* );
  #pragma pack ()

  #define cVD_IntParamNone 0x80000000  // for VD_IntParam

  #ifdef INTERNAL_VATECH_DAVINCI_LIBRARY // procedures' declarations ('as is' in the DLL)

    // general UI and logs //
    // returns TRUE if UNICODE version
    BOOL _stdcall VD_IsUnicode();
    // invokes general property sheet
    BOOL _stdcall VD_Dialog();
    // open log-file
    void _stdcall VD_LogOpen( const TCHAR *AHomeDir, int ANumLogFiles, const TCHAR *AFPrefix );
    // record string to log-file
    void _stdcall VD_LogMsg( const TCHAR *AMsg );
    // close log-file
    void _stdcall VD_LogClose();
    // flush log-file
    void _stdcall VD_LogFlush();
    // returns home directory
    void _stdcall VD_GetHomeDirectory( TCHAR *ADir );
    // set section name in library's initialization profile file
    void _stdcall VD_IniProfSetSection( const TCHAR *ASectName );
    // get string key from library's initialization profile
    const TCHAR* _stdcall VD_IniProfGetStr( const TCHAR *AKeyName, const TCHAR *ADefault );
    // set/get "data segment" (static) parameter
    int _stdcall VD_IntParam( const TCHAR *AParamName, int AParamValue );
    const TCHAR* _stdcall VD_StrParam( const TCHAR *AParamName, const TCHAR *AParamValue );

    ////// acquisition //////
    // sets full frame dimension (should not be called for real acquisitin case)
    void _stdcall VDACQ_SetFrameDim( int AWidth, int AHeight );
    // returns full frame dimension
    void _stdcall VDACQ_GetFrameDim( int *AWidth, int *AHeight );
    // connects to detector
    tVDACQ_CallBackRec* _stdcall VDACQ_Connect( int AFlags,
      tVDACQ_CallBackProc AUserCallBackProc, void *AUserParam, short *AFrameBuffer, int AMode );
    // send simple auxiliary command (TFT only)
    BOOL _stdcall VDACQ_SendCommand( tVDACQ_CallBackRec *ACallBackRec, DWORD ACommand );
    BOOL _stdcall VDACQ_SendCommandParam( tVDACQ_CallBackRec *ACallBackRec, DWORD ACommand, const void *AData, int ADataSize );
    // vendor comand (USB only)
    BOOL _stdcall VDACQ_VendorCommand( tVDACQ_CallBackRec *ACallBackRec, int ARequest, int AValue, int AIndex );
    // starts acquisition
    BOOL _stdcall VDACQ_StartFrame( tVDACQ_CallBackRec *ACallBackRec );
    // aborts acquisition
    BOOL _stdcall VDACQ_Abort( tVDACQ_CallBackRec *ACallBackRec );
    // releases resources associated with ACallBackRec
    void _stdcall VDACQ_Close( tVDACQ_CallBackRec *ACallBackRec );
    // returns current detector's IP or PID
    void _stdcall VDACQ_GetDetectorAddr( TCHAR *AIPAddr );
    // sets detector's IP or PID
    void _stdcall VDACQ_SetDetectorAddr( const TCHAR *AAddr );
    // sets framegrabber's parammeters
    void _stdcall VDACQ_SetFGRec( const tVDACQ_FGRec *AFGRec );
    // returns framegrabber's parameters
    void _stdcall VDACQ_GetFGRec( tVDACQ_FGRec *AFGRec );
    // returns current number of acquired frames at low-level
    int _stdcall VDACQ_GetFGFrameNum();
    // sets freeze state for framegrabber; AFreezeState<0 just returns current state
    int _stdcall VDACQ_FGFreeze( int AFreezeState );
    // set detector's information to flash memory (as ASCII)
    BOOL _stdcall VDACQ_GetDetectorInfo( TCHAR *AStr );
    // retrieve detector's information from flash memory (as ASCII)
    BOOL _stdcall VDACQ_SetDetectorInfo( const TCHAR *AStr );
    
    ////// calibration //////
    // returns image dimension; returns FALSE if all cut margins are zero
    BOOL _stdcall VDC_GetImageDim( int *AWidth, int *AHeight );
    // cuts (also rotates and flips optionally) frame buffer to image's dimension.
    short *_stdcall VDC_CutImage( const short *AFrameBuffer, short *AImageBuffer );
    // starts calibration
    tVDC_CallBackRec* _stdcall VDC_Process( int AFlags,
      tVDC_CallBackProc AUserCallBackProc, void *AUserParam, short *AFrameBuffer );
    // aborts calibration
    BOOL _stdcall VDC_Abort( tVDC_CallBackRec *ACallBackRec );
    // releases resources associated with ACallBackRec
    void _stdcall VDC_Close( tVDC_CallBackRec *ACallBackRec );
    // service: returns current calibration directory
    void _stdcall VDC_GetCalibrationDirectory( TCHAR *ADir );
    // service: set temporary calibration directory
    BOOL _stdcall VDC_SetCalibrationDirectory( const TCHAR *ADir );
    // service: returns image cut params
    void _stdcall VDC_GetImgCutParams( tVDC_ImgCut *ACutParams );
    // service: sets image cut params
    void _stdcall VDC_SetImgCutParams( const tVDC_ImgCut *ACutParams );
    
    ///// image process /////
    // starts img process 
    tVDIP_CallBackRec* _stdcall VDIP_Process( int AFlags,
      tVDIP_CallBackProc AUserCallBackProc, void *AUserParam, short *AImageBuffer, int AModeNumber );
    // aborts img process 
    BOOL _stdcall VDIP_Abort( tVDIP_CallBackRec *ACallBackRec );
    // releases resources associated with ACallBackRec
    void _stdcall VDIP_Close( tVDIP_CallBackRec *ACallBackRec );

    //// user interface /////
    void _stdcall VD_Set_Acquisition( int AFlags, short *AFrameData, int AMode, tVDACQ_CallBackProc AUserCallBackProc, void *AUserParam );
    void _stdcall VD_Set_Calibration( int AFlags, short *AFrameData, short *AImgData, tVDC_CallBackProc AUserCallBackProc, void *AUserParam );
    void _stdcall VD_Set_ImgProcess( int AFlags, short *AImgData, int AModeNumber, tVDIP_CallBackProc AUserCallBackProc, void *AUserParam );
    // default modal dialog with preview
    int _stdcall VD_GetImage( HWND AOwner );
    // modal dialog with preview in specified rectangle
    int _stdcall VD_GetImageSP( HWND AOwner, int AWndLeft, int AWndTop, int AWndWidth, int AWndHeight );
    // modal dialog withoiut preview
    int _stdcall VD_GetImageA( HWND AOwner, int AWndLeft, int AWndTop );
    // cancel modal dialog
    void _stdcall VD_GetImageCancel();

  #else // for use in applications

    // table of exported methods
    typedef struct {
      HINSTANCE rHDLL; // the library's handle
      // general UI and logs //
      BOOL (_stdcall *rVD_IsUnicode)(void);
      BOOL (_stdcall *rVD_Dialog)( HWND ); // it has additional parameter: owner's window
      void (_stdcall *rVD_LogOpen)( const TCHAR*, int, const TCHAR* );
      void (_stdcall *rVD_LogMsg)( const TCHAR* );
      void (_stdcall *rVD_LogClose)(void);
      void (_stdcall *rVD_LogFlush)(void);
      void (_stdcall *rVD_GetHomeDirectory)( TCHAR* );
      void (_stdcall *rVD_IniProfSetSection)( const TCHAR* );
      const TCHAR* (_stdcall *rVD_IniProfGetStr)( const TCHAR*, const TCHAR* );
      int (_stdcall *rVD_IntParam)( const TCHAR*, int );
      const TCHAR* (_stdcall *rVD_StrParam)( const TCHAR*, const TCHAR* );

      ////// acquisition //////
      void (_stdcall *rVDACQ_SetFrameDim)( int, int );
      void (_stdcall *rVDACQ_GetFrameDim)( int*, int* );
      tVDACQ_CallBackRec* (_stdcall *rVDACQ_Connect)( int, tVDACQ_CallBackProc, void*, short*, int );
      BOOL (_stdcall *rVDACQ_SendCommand)( tVDACQ_CallBackRec*, DWORD );
      BOOL (_stdcall *rVDACQ_SendCommandParam)( tVDACQ_CallBackRec*, DWORD, const void*, int );
      BOOL (_stdcall *rVDACQ_StartFrame)( tVDACQ_CallBackRec* );
      BOOL (_stdcall *rVDACQ_VendorCommand)( tVDACQ_CallBackRec*, int, int, int );
      BOOL (_stdcall *rVDACQ_Abort)( tVDACQ_CallBackRec* );
      void (_stdcall *rVDACQ_Close)( tVDACQ_CallBackRec* );
      void (_stdcall *rVDACQ_GetDetectorAddr)( TCHAR* );
      void (_stdcall *rVDACQ_SetDetectorAddr)( const TCHAR* );
      BOOL (_stdcall *rVDACQ_SetFGRec)( const tVDACQ_FGRec*);
      void (_stdcall *rVDACQ_GetFGRec)( tVDACQ_FGRec* );
	  int  (_stdcall *rVDACQ_GetFGFrameNum)(void);
	  int  (_stdcall *rVDACQ_FGFreeze)( int );
      BOOL (_stdcall *rVDACQ_GetDetectorInfo)( TCHAR* );
      BOOL (_stdcall *rVDACQ_SetDetectorInfo)( const TCHAR* );
      ////// calibration //////
      BOOL (_stdcall *rVDC_GetImageDim)( int*, int* );
      short* (_stdcall *rVDC_CutImage)( const short*, short* );
      tVDC_CallBackRec* (_stdcall *rVDC_Process)( int, tVDC_CallBackProc, void*, short* );
      BOOL (_stdcall *rVDC_Abort)( tVDC_CallBackRec* );
      void (_stdcall *rVDC_Close)( tVDC_CallBackRec* );
      void (_stdcall *rVDC_GetCalibrationDirectory)( TCHAR* );
      BOOL (_stdcall *rVDC_SetCalibrationDirectory)( const TCHAR* );
      void (_stdcall *rVDC_GetImgCutParams)( tVDC_ImgCut* );
      void (_stdcall *rVDC_SetImgCutParams)( const tVDC_ImgCut* );
      ///// image process /////
      tVDIP_CallBackRec* (_stdcall *rVDIP_Process)( int, tVDIP_CallBackProc, void*, short*, int );
      BOOL (_stdcall *rVDIP_Abort)( tVDIP_CallBackRec* );
      void (_stdcall *rVDIP_Close)( tVDIP_CallBackRec* );
      //// user interface /////
      void (_stdcall *rVD_Set_Acquisition)( int, short*, int, tVDACQ_CallBackProc, void* );
      void (_stdcall *rVD_Set_Calibration)( int, short*, short*, tVDC_CallBackProc, void* );
      void (_stdcall *rVD_Set_ImgProcess)( int, short*, int, tVDIP_CallBackProc, void* );
      int (_stdcall *rVD_GetImage)( HWND );
      int (_stdcall *rVD_GetImageSP)( HWND, int, int, int, int );
      int (_stdcall *rVD_GetImageA)( HWND, int, int );
      void (_stdcall *rVD_GetImageCancel)(void);
    } tVADAV_InterfaceRec; 

    BOOL VADAV_MapDLL( const TCHAR *ADllFName, tVADAV_InterfaceRec *AIntfRec );
    // calls LoadLibrary and fills "AIntfRec"
    void VADAV_ReleaseDLL( tVADAV_InterfaceRec *AIntfRec );
    // calls FreeLibrary

  #endif // for use in applications
#endif // ifdef VADAVINTF_H_INCLUDED

 