﻿using Dicom;
using Dicom.Imaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DetectorCtrlTester.AcquisitionManager;

namespace DetectorCtrlTester
{
    public class DemoDataManager
    {
        #region 구조체 / 열거형
        public struct DEMO_IMAGE_INFO
        {
            public List<byte[]> pixelData;
            public int width;
            public int height;

            public bool IsValid()
            {
                bool valid = false;

                if (this.pixelData != null
                    && this.pixelData.Count > 0
                    && this.width > 0
                    && this.height > 0)
                {
                    valid = true;
                } // else

                return valid;
            }
        }
        #endregion 구조체 / 열거형

        private DEMO_IMAGE_INFO radDemoImageInfo;
        private DEMO_IMAGE_INFO floDemoImageInfo;

        public delegate void _WriteLogRequested(string log);
        public event _WriteLogRequested WriteLogRequested;

        #region 생성자 / 소멸자
        public DemoDataManager()
        {
            try
            {
                LoadDemoImages();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        ~DemoDataManager()
        {

        }
        #endregion 생성자 / 소멸자

        #region 로그 작성 관련 함수들
        private void GeneratorCtrl_WriteLogRequested(string log)
        {
            try
            {
                WriteLogRequested?.Invoke(log);
            }
            catch (Exception)
            {
            }
        }

        private void WriteExceptionLog(Exception ex)
        {
            try
            {
                WriteLog(ex.ToString());
            }
            catch (Exception)
            {
                // 구현 불필요
            }
        }

        private void WriteLog(string log)
        {
            try
            {
                WriteLogRequested?.Invoke(log);
            }
            catch (Exception)
            {
                // 구현 불필요
            }
        }
        #endregion 로그 작성 관련 함수들

        private string GetExecutablePath()
        {
            string path = string.Empty;

            try
            {
                path = AppDomain.CurrentDomain.BaseDirectory;

                path = path.TrimEnd(new char[] { '\\' });
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }

            return path;
        }

        private string GetDemoImagePath()
        {
            string path = string.Empty;

            try
            {
                path = string.Format("{0}\\{1}", GetExecutablePath(), "DemoImages");
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }

            return path;
        }

        private void LoadDemoImageRAD()
        {
            try
            {
                this.radDemoImageInfo = new DEMO_IMAGE_INFO();

                string radDemoFilePath = string.Format("{0}\\{1}", GetDemoImagePath(), "RAD.dcm");

                DicomFile dicomFile = DicomFile.Open(radDemoFilePath);
                DicomImage dicomImage = new DicomImage(dicomFile.Dataset);

                this.radDemoImageInfo.width = dicomImage.PixelData.Width;
                this.radDemoImageInfo.height = dicomImage.PixelData.Height;

                this.radDemoImageInfo.pixelData = new List<byte[]>();

                this.radDemoImageInfo.pixelData.Add(dicomImage.PixelData.GetFrame(0).Data);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void LoadDemoImageFLO()
        {
            try
            {
                this.floDemoImageInfo = new DEMO_IMAGE_INFO();

                string floDemoFilePath = string.Format("{0}\\{1}", GetDemoImagePath(), "FLO.dcm");

                DicomFile dicomFile = DicomFile.Open(floDemoFilePath);
                DicomImage dicomImage = new DicomImage(dicomFile.Dataset);

                this.floDemoImageInfo.width = dicomImage.PixelData.Width;
                this.floDemoImageInfo.height = dicomImage.PixelData.Height;

                this.floDemoImageInfo.pixelData = new List<byte[]>();

                int numberOfFrames = dicomImage.PixelData.NumberOfFrames;

                for (int frameIndex = 0; frameIndex < numberOfFrames; frameIndex++)
                {
                    this.floDemoImageInfo.pixelData.Add(dicomImage.PixelData.GetFrame(frameIndex).Data);
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void LoadDemoImages()
        {
            try
            {
                LoadDemoImageRAD();

                LoadDemoImageFLO();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public DEMO_IMAGE_INFO GetDemoImage(CAPTURE_MODE captureMode)
        {
            DEMO_IMAGE_INFO demoImageInfo = new DEMO_IMAGE_INFO();

            try
            {
                if (captureMode == CAPTURE_MODE.RADIOGRAPHY)
                {
                    demoImageInfo = this.radDemoImageInfo;
                }
                else
                {
                    demoImageInfo = this.floDemoImageInfo;
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }

            return demoImageInfo;
        }
    }
}
