﻿using DetectorCtrl;
using Dicom;
using Dicom.Imaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DetectorCtrlTester
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        private AcquisitionManager acquisitionManager = null;

        public MainWindow()
        {
            InitializeComponent();

            WriteLog("Program Started");

            this.acquisitionManager = new AcquisitionManager();
            this.acquisitionManager.WriteLogRequested += AcquisitionManager_WriteLogRequested;

            this.acquisitionManager.XRayStateChanged += AcquisitionManager_XRayStateChanged;
            this.acquisitionManager.CaptureModeChanged += AcquisitionManager_CaptureModeChanged;

            this.acquisitionManager.KV_Changed += AcquisitionManager_KV_Changed;
            this.acquisitionManager.MA_Changed += AcquisitionManager_MA_Changed;
            this.acquisitionManager.MAS_Changed += AcquisitionManager_MAS_Changed;
            this.acquisitionManager.MS_Changed += AcquisitionManager_MS_Changed;

            this.acquisitionManager.LaserStateChanged += AcquisitionManager_LaserStateChanged;
            this.acquisitionManager.ABCStateChanged += AcquisitionManager_ABCStateChanged;
            this.acquisitionManager.SnapCountChanged += AcquisitionManager_SnapCountChanged;

            txtLog.Text = string.Empty;
        }

        #region 제너레이터 연동 관련 이벤트 핸들러 함수들
        private void AcquisitionManager_SnapCountChanged(int snapCount)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    lblSnapCount.Content = snapCount.ToString();
                }));

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_XRayStateChanged(GeneratorCtrl.XRAY_STATE xrayState)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    if (xrayState == GeneratorCtrl.XRAY_STATE.OFF)
                    {
                        lblXrayState.Content = "Off";
                    }
                    else if (xrayState == GeneratorCtrl.XRAY_STATE.READY_OK)
                    {
                        lblXrayState.Content = "Ready OK";
                    }
                    else if (xrayState == GeneratorCtrl.XRAY_STATE.EXPOSURE)
                    {
                        lblXrayState.Content = "Exposure!!";
                    }
                    else if (xrayState == GeneratorCtrl.XRAY_STATE.WAIT)
                    {
                        lblXrayState.Content = "Waitting";
                    }
                    else if (xrayState == GeneratorCtrl.XRAY_STATE.ERROR)
                    {
                        lblXrayState.Content = "Error";
                    }
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_ABCStateChanged(bool on)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    if (on == true)
                    {
                        lblABCState.Content = "ON";
                    }
                    else
                    {
                        lblABCState.Content = "OFF";
                    }
                }));
                
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_LaserStateChanged(bool on)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    if (on == true)
                    {
                        lblLaserState.Content = "ON";
                    }
                    else
                    {
                        lblLaserState.Content = "OFF";
                    }
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_CaptureModeChanged(GeneratorCtrl.CAPTURE_MODE captureMode)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    if (captureMode == GeneratorCtrl.CAPTURE_MODE.RADIOGRAPHY)
                    {
                        lblCaptureMode.Content = "RAD";
                    }
                    else
                    {
                        lblCaptureMode.Content = "FLO";
                    }
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_MS_Changed(double MS)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    txtMS.Text = MS.ToString();
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_MA_Changed(double MA)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    lblMA.Content = MA.ToString();
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_MAS_Changed(double MAS)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    txtMAS.Text = MAS.ToString();
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private void AcquisitionManager_KV_Changed(int KV)
        {
            try
            {
                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    txtKV.Text = KV.ToString();
                }));
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }
        #endregion 제너레이터 연동 관련 이벤트 핸들러 함수들

        #region 로그 관련 함수들
        private void WriteLog(string log)
        {
            try
            {
                string debugLog = string.Format("[{0}] {1}\r\n", DateTime.Now.ToString("HH:mm:ss"), log);

                Dispatcher.BeginInvoke(new Action(delegate ()
                {
                    txtLog.Text += debugLog;

                    txtLog.ScrollToEnd();
                }));
            }
            catch (Exception)
            {
            }
        }

        private void AcquisitionManager_WriteLogRequested(string log)
        {
            try
            {
                WriteLog(log);
            }
            catch (Exception)
            {
            }
        }
        #endregion 로그 관련 함수들

        private void btnPortOpen_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetGeneratorPortName(txtPortName.Text);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rbBrightImage.IsChecked = true;

            btnInit_Click(null, null);

            btnPortOpen_Click(null, null);

            btnGetCurrentCondition_Click(null, null);

            btnPrepare_Click(null, null);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.acquisitionManager.CloseSystem();
        }

        private void btnRADMode_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetCaptureMode(AcquisitionManager.CAPTURE_MODE.RADIOGRAPHY);
        }

        private void btnFLO_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetCaptureMode(AcquisitionManager.CAPTURE_MODE.FLOUROSCOPY);
        }

        private int GetCurrentKV()
        {
            int KV = 0;

            int.TryParse(txtKV.Text, out KV);

            return KV;
        }

        private void btnKVUp_Click(object sender, RoutedEventArgs e)
        {
            int nextKV = GetCurrentKV() + 1;

            this.acquisitionManager.SetKV(nextKV);
        }

        private void btnKVDown_Click(object sender, RoutedEventArgs e)
        {
            int prevKV = GetCurrentKV() - 1;

            this.acquisitionManager.SetKV(prevKV);
        }

        private double GetCurrentMS()
        {
            double MS = 0;

            double.TryParse(txtMS.Text, out MS);

            return MS;
        }

        private double GetCurrentMAS()
        {
            double MAS = 0;

            double.TryParse(txtMAS.Text, out MAS);

            return MAS;
        }

        private double GetCurrentMA()
        {
            double MA = 0;

            double.TryParse((string)lblMA.Content, out MA);

            return MA;
        }

        private int GetCurrentSnapCount()
        {
            int snapCount = 1;

            int.TryParse((string)lblSnapCount.Content, out snapCount);

            return snapCount;
        }

        private void btnMASUp_Click(object sender, RoutedEventArgs e)
        {
            double nextMAS = GetCurrentMAS() + 0.01;

            double currentMS = GetCurrentMS();

            double nextMA = nextMAS / currentMS * 1000 / GetCurrentSnapCount();

            this.acquisitionManager.SetMA(nextMA);
        }

        private void btnMASDown_Click(object sender, RoutedEventArgs e)
        {
            double prevMAS = GetCurrentMAS() - 0.01;

            double currentMS = GetCurrentMS();

            double prevMA = prevMAS / currentMS * 1000 / GetCurrentSnapCount();

            this.acquisitionManager.SetMA(prevMA);
        }

        private void btnMS100_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetMS(100);
        }

        private void btnMS50_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetMS(50);
        }

        private void btnMS37_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetMS(37);
        }

        private void btnRec_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnInit_Click(object sender, RoutedEventArgs e)
        {
            //this.acquisitionManager.InitSystem(true);
            this.acquisitionManager.InitSystem(false);

            this.acquisitionManager.ImageAcquired += AcquisitionManager_ImageAcquired;
        }

        private void AcquisitionManager_ImageAcquired(byte[] imageBuffer, int width, int height, int bytesPerPixel)
        {
            Dispatcher.BeginInvoke(new Action(delegate ()
            {
                byte[] pixelBuffer8 = new byte[width * height];

                int value16 = 0;
                double value8 = 0;
                for (int i = 0; i < pixelBuffer8.Length; i++)
                {
                    value16 = BitConverter.ToInt16(imageBuffer, i * 2);

                    value8 = (double)value16 / 10;

                    if (value8 > 255)
                    {
                        value8 = 255;
                    }

                    pixelBuffer8[i] = (byte)value8;
                }

                WriteableBitmap writeableBitmap = new WriteableBitmap(width, height, 0, 0, PixelFormats.Gray8, null);

                writeableBitmap.WritePixels(new Int32Rect(0, 0, width, height), pixelBuffer8, width, 0);

                imgAcquired.Source = writeableBitmap;
            }));
        }

        private void btnPrepare_Click(object sender, RoutedEventArgs e)
        {
            imgAcquired.Source = null;

            this.acquisitionManager.PrepareSystem();
        }

        private void btnAcquire_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.AcquireDemoImage();
        }

        private void btnGetCurrentCondition_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.GetCurrentCondition();
        }

        private void btnLaserOn_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetLaserState(true);
        }

        private void btnLaserOff_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetLaserState(false);
        }

        private void btnABCOn_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetABCState(true);
        }

        private void btnABCOff_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetABCState(false);
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.StopAcquireDemoImage();

            this.acquisitionManager.EndFrame();
        }

        private void rbBrightImage_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetStartFrameMode(AcquisitionManager.START_FRAME_MODE.BRIGHT);
        }

        private void rbDarkImage_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.SetStartFrameMode(AcquisitionManager.START_FRAME_MODE.DARK);
        }

        private void btnGetDark_Click(object sender, RoutedEventArgs e)
        {
            this.acquisitionManager.GetDark();
        }
    }
}
