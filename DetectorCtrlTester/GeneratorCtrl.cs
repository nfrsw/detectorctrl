﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace DetectorCtrlTester
{
    public class GeneratorCtrl
    {
        #region 구조체 / 열거형
        public enum CAPTURE_MODE
        {
            RADIOGRAPHY,
            FLOUROSCOPY
        }

        public enum PULSE_TYPE
        {
            _3_PULSE = 0x30,
            _5_PULSE = 0x31,
            _7_5_PULSE = 0x32,
            _15_PULSE = 0x33,
            _30_PULSE = 0x34
        }

        public enum APR_TYPE
        {
            HAND = 0x30,
            KNEE = 0x31,
            SHOULDER = 0x32,
            ELBOW_JOINT = 0x33,
            FOOT = 0x34
        }

        public enum XRAY_STATE
        {
            OFF,
            READY_OK,
            EXPOSURE,
            WAIT,
            ERROR
        }

        public struct COMMAND_DATA
        {
            public byte getter;
            public byte commandType;
            public byte dataHigh;
            public byte dataLow;
        }

        public struct TRANS_DATA
        {
            public bool waitResponse;
            public byte[] transData;
        }
        #endregion 구조체 / 열거형

        private readonly int WAITTING_DATA_INTERVAL = 5000;
        private readonly int ABORT_THREAD_TIMEOUT = 1000;
        private readonly int DATA_LENGTH = 4;

        private SerialPort serialComm = null;
        private Queue<byte> receivedDataFromSerial = null;
        private Queue<TRANS_DATA> sendDataToSerial = null;
        private Thread parseDataThread = null;
        private Thread sendDataThread = null;
        private bool breakParseDataThread = true;
        private bool breakSendDataThread = true;
        private AutoResetEvent dataReceivedEvent = null;
        private AutoResetEvent dataSendEvent = null;
        private AutoResetEvent dataWaitEvent = null;

        readonly int MIN_KV = 40;
        readonly int MAX_KV = 80;

        readonly double MIN_MA = 0.14;
        readonly double MAX_MA = 0.3;

        readonly double MIN_MAS = 0.02;
        readonly double MAX_MAS = 0.3;

        readonly double MIN_MS = 35;
        readonly double MAX_MS = 100;

        readonly double MIN_TOTAL_MS = 0;
        readonly double MAX_TOTAL_MS = 20;

        public delegate void _WriteLogRequested(string log);
        public event _WriteLogRequested WriteLogRequested;

        #region 제너레이터 현재 상태 통지
        public delegate void _InitializeStateChanged(bool initialized);
        public event _InitializeStateChanged InitializeStateChanged;

        public delegate void _CaptureModeChanged(CAPTURE_MODE captureMode);
        public event _CaptureModeChanged CaptureModeChanged;

        public delegate void _KV_Changed(int KV);
        public event _KV_Changed KV_Changed;

        public delegate void _MA_Changed(double MA);
        public event _MA_Changed MA_Changed;

        public delegate void _MAS_Changed(double MAS);
        public event _MAS_Changed MAS_Changed;

        public delegate void _MS_Changed(double MS);
        public event _MS_Changed MS_Changed;

        public delegate void _SnapCountChanged(int snapCount);
        public event _SnapCountChanged SnapCountChanged;

        public delegate void _DutyChanged(int duty);
        public event _DutyChanged DutyChanged;

        public delegate void _PulseRateChanged(PULSE_TYPE pulseType);
        public event _PulseRateChanged PulseRateChanged;

        public delegate void _FLOTotalMSChanged(int totalMS);
        public event _FLOTotalMSChanged FLOTotalMSChanged;

        public delegate void _ExposureStateChanged(bool on);
        public event _ExposureStateChanged ExposureStateChanged;

        public delegate void _FootStoreKeyStateChanged(bool on);
        public event _FootStoreKeyStateChanged FootStoreKeyStateChanged;

        public delegate void _XRayStateChanged(XRAY_STATE xrayState);
        public event _XRayStateChanged XRayStateChanged;

        public delegate void _APRChanged(APR_TYPE aprType);
        public event _APRChanged APRChanged;

        public delegate void _APRStoreStateChanged(bool on);
        public event _APRStoreStateChanged APRStoreStateChanged;

        public delegate void _ABCStateChanged(bool on);
        public event _ABCStateChanged ABCStateChanged;

        public delegate void _LaserStateChanged(bool on);
        public event _LaserStateChanged LaserStateChanged;

        public delegate void _BuzzerStateChanged(bool on);
        public event _BuzzerStateChanged BuzzerStateChanged;

        public delegate void _DetectorStateChanged(bool start);
        public event _DetectorStateChanged DetectorStateChanged;

        public delegate void _FirmwareVersion(string version);
        public event _FirmwareVersion FirmwareVersion;

        public delegate void _ErrorOccured(List<int> errorCodeList);
        public event _ErrorOccured ErrorOccured;
        #endregion 제너레이터 현재 상태 통지

        #region 생성 / 소멸 / 초기화 관련
        public GeneratorCtrl()
        {
            try
            {
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        ~GeneratorCtrl()
        {
            try
            {
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GeneratorInit()
        {
            try
            {
                this.serialComm = new SerialPort();

                this.serialComm.DataReceived += serialComm_DataReceived;

                this.receivedDataFromSerial = new Queue<byte>();
                this.sendDataToSerial = new Queue<TRANS_DATA>();

                this.dataReceivedEvent = new AutoResetEvent(false);
                this.dataSendEvent = new AutoResetEvent(false);
                this.dataWaitEvent = new AutoResetEvent(false);

                this.breakParseDataThread = false;
                this.parseDataThread = new Thread(ParseCommandProc);
                this.parseDataThread.Start();

                this.breakSendDataThread = false;
                this.sendDataThread = new Thread(SendCommandProc);
                this.sendDataThread.Start();

                //Test();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GeneratorClose()
        {
            try
            {
                if (this.serialComm != null
                    && this.serialComm.IsOpen)
                {
                    this.serialComm.Close();
                } // else

                this.breakParseDataThread = true;

                if (this.dataReceivedEvent != null)
                {
                    this.dataReceivedEvent.Set();
                } // else

                if (this.parseDataThread != null)
                {
                    this.parseDataThread.Join(ABORT_THREAD_TIMEOUT);
                } // else

                this.breakSendDataThread = true;

                if (this.dataSendEvent != null)
                {
                    this.dataSendEvent.Set();
                } // else

                if (this.dataWaitEvent != null)
                {
                    this.dataWaitEvent.Set();
                } // else

                if (this.sendDataThread != null)
                {
                    this.sendDataThread.Join(ABORT_THREAD_TIMEOUT);
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 생성 / 소멸 / 초기화 관련

        #region 로그 작성 관련 함수들
        private void WriteExceptionLog(Exception ex)
        {
            WriteLog(ex.ToString());
        }

        private void WriteLog(string log)
        {
            try
            {
                WriteLogRequested?.Invoke(log);
            }
            catch (Exception)
            {
                // 구현 불필요
            }
        }

        private void WriteBufferToLog(string transType, byte[] rxBuffer)
        {
            try
            {
                string buffer = string.Format("{0} : ", transType);

                foreach (byte data in rxBuffer)
                {
                    buffer += string.Format("{0:X02} ", data);
                }

                WriteLog(buffer);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 로그 작성 관련 함수들

        #region Parse Command 함수들
        private void ParseCommandForInitialize(COMMAND_DATA commandData)
        {
            try
            {
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'a') // Initialize
                {
                    // Fail=0x30, Success=0x31
                    bool initialized = false;

                    if (dataLowValue == 0x31)
                    {
                        initialized = true;
                    } // else

                    InitializeStateChanged?.Invoke(initialized);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForCaptureMode(COMMAND_DATA commandData)
        {
            try
            {
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'b') // Mode
                {
                    // RAD=0x31, FLO=0x32
                    CAPTURE_MODE captureMode = CAPTURE_MODE.RADIOGRAPHY;

                    if (dataLowValue == 0x32)
                    {
                        captureMode = CAPTURE_MODE.FLOUROSCOPY;
                    } // else

                    CaptureModeChanged?.Invoke(captureMode);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForCondition(COMMAND_DATA commandData)
        {
            try
            {
                int dataHighValue = commandData.dataHigh;
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'c') // KV
                {
                    KV_Changed?.Invoke(dataLowValue);
                }
                else if (commandData.commandType == 'd') // MA
                {
                    MA_Changed?.Invoke(dataLowValue / 100.0);
                }
                else if (commandData.commandType == 'e') // MAS
                {
                    MAS_Changed?.Invoke(dataLowValue / 100.0);
                }
                else if (commandData.commandType == 'f') // MS
                {
                    MS_Changed?.Invoke(dataHighValue + dataLowValue / 10.0);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForFLO(COMMAND_DATA commandData)
        {
            try
            {
                int dataHighValue = commandData.dataHigh;
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'g') // Snap Count
                {
                    SnapCountChanged?.Invoke(dataLowValue);
                }
                else if (commandData.commandType == 'h') // Duty
                {
                    DutyChanged?.Invoke(dataLowValue);
                }
                else if (commandData.commandType == 'i') // Pulse Rate
                {
                    PulseRateChanged?.Invoke((PULSE_TYPE)dataLowValue);
                }
                else if (commandData.commandType == 'j') // FLO Total MS
                {
                    FLOTotalMSChanged?.Invoke((dataHighValue * 1000) + (dataLowValue * 10));
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForCapture(COMMAND_DATA commandData)
        {
            try
            {
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'k') // Exposure On/Off
                {
                    bool exposureOn = false; // On=0x31, Off=0x30

                    if (dataLowValue == 0x31)
                    {
                        exposureOn = true;
                    } // else

                    ExposureStateChanged?.Invoke(exposureOn);
                }
                else if (commandData.commandType == 'l') // Foot Store Key
                {
                    bool footStoreKeyOn = false;

                    if (dataLowValue == 0x31)
                    {
                        footStoreKeyOn = true;
                    } // else

                    FootStoreKeyStateChanged?.Invoke(footStoreKeyOn);
                }
                else if (commandData.commandType == 'm') // X-Ray State
                {
                    XRAY_STATE xrayState = XRAY_STATE.OFF;

                    if (dataLowValue == 0x30)
                    {
                        xrayState = XRAY_STATE.OFF;
                    }
                    else if (dataLowValue == 0x31)
                    {
                        xrayState = XRAY_STATE.READY_OK;
                    }
                    else if (dataLowValue == 0x32)
                    {
                        xrayState = XRAY_STATE.EXPOSURE;
                    }
                    else if (dataLowValue == 0x33)
                    {
                        xrayState = XRAY_STATE.WAIT;
                    }
                    else if (dataLowValue == 0x34)
                    {
                        xrayState = XRAY_STATE.ERROR;
                    } // else

                    XRayStateChanged?.Invoke(xrayState);
                }
                else if (commandData.commandType == 's') // Detector (Hand Switch On/Off)
                {
                    bool detectorStart = false; // Start=0x30, End=0x31

                    if (dataLowValue == 0x30)
                    {
                        detectorStart = true;
                    } // else

                    DetectorStateChanged?.Invoke(detectorStart);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForLaserBuzzer(COMMAND_DATA commandData)
        {
            try
            {
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'q') // Laser On/Off
                {
                    bool laserOn = false; // On=0x31, Off=0x30

                    if (dataLowValue == 0x31)
                    {
                        laserOn = true;
                    } // else

                    LaserStateChanged?.Invoke(laserOn);
                }
                else if (commandData.commandType == 'r') // Buzzer On/Off
                {
                    bool buzzerOn = false; // On=0x31, Off=0x30

                    if (dataLowValue == 0x31)
                    {
                        buzzerOn = true;
                    } // else

                    BuzzerStateChanged?.Invoke(buzzerOn);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForEtc(COMMAND_DATA commandData)
        {
            try
            {
                int dataHighValue = commandData.dataHigh;
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'n') // APR
                {
                    APR_TYPE aprType = APR_TYPE.HAND;

                    if (dataLowValue == 0x30) // Hand
                    {
                        aprType = APR_TYPE.HAND;
                    }
                    else if (dataLowValue == 0x31) // Knee
                    {
                        aprType = APR_TYPE.KNEE;
                    }
                    else if (dataLowValue == 0x32) // Shoulder
                    {
                        aprType = APR_TYPE.SHOULDER;
                    }
                    else if (dataLowValue == 0x33) // Elbow Point
                    {
                        aprType = APR_TYPE.ELBOW_JOINT;
                    }
                    else if (dataLowValue == 0x34) // Foot
                    {
                        aprType = APR_TYPE.FOOT;
                    }
                    else
                    {
                        aprType = APR_TYPE.FOOT;
                    }

                    APRChanged?.Invoke(aprType);
                }
                else if (commandData.commandType == 'o') // APR Store
                {
                    bool aprStoreOn = false; // On=0x31, Off=0x30

                    if (dataLowValue == 0x31)
                    {
                        aprStoreOn = true;
                    } // else

                    APRStoreStateChanged?.Invoke(aprStoreOn);
                }
                else if (commandData.commandType == 'p') // ABC
                {
                    bool abcOn = false; // On=0x31, Off(Manual)=0x30

                    if (dataLowValue == 0x31)
                    {
                        abcOn = true;
                    } // else

                    ABCStateChanged?.Invoke(abcOn);
                }

                else if (commandData.commandType == 't') // Firmware Version
                {
                    string version = string.Format("%d.%d", dataHighValue, dataLowValue);

                    FirmwareVersion?.Invoke(version);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandForError(COMMAND_DATA commandData)
        {
            try
            {
                int dataLowValue = commandData.dataLow;

                if (commandData.commandType == 'u') // Error State
                {
                    List<int> errorCodeList = new List<int>();

                    for (int bitIndex = 0; bitIndex < 8; bitIndex++)
                    {
                        if ((dataLowValue & 0x01) == 0x01)
                        {
                            errorCodeList.Add(bitIndex);
                        } // else
                    }

                    if (errorCodeList.Count > 0)
                    {
                        ErrorOccured?.Invoke(errorCodeList);
                    } // else
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommand(byte[] buffer)
        {
            try
            {
                if (buffer == null
                    || buffer.Length < DATA_LENGTH)
                {
                    return;
                } // else

                COMMAND_DATA commandData = new COMMAND_DATA();

                commandData.getter = buffer[0];
                commandData.commandType = buffer[1];
                commandData.dataHigh = buffer[2];
                commandData.dataLow = buffer[3];

                // Get / Set은 항상 G 이어야 한다.
                if (commandData.getter != 'G')
                {
                    return;
                } // else

                ParseCommandForInitialize(commandData);

                ParseCommandForCaptureMode(commandData);

                ParseCommandForCondition(commandData);

                ParseCommandForFLO(commandData);

                ParseCommandForCapture(commandData);

                ParseCommandForLaserBuzzer(commandData);

                ParseCommandForEtc(commandData);

                ParseCommandForError(commandData);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion Parse Command 함수들

        #region 제너레이터 명령어 송/수신 함수들
        private void SendCommand(bool waitResponse, string SetGet, string CMD, byte dataH, byte dataL)
        {
            try
            {
                if (this.serialComm == null
                    || this.serialComm.IsOpen == false)
                {
                    return;
                } // else

                byte[] command = new byte[7];

                command[0] = 0x02;  // STX
                command[1] = Convert.ToByte(Convert.ToInt32(Convert.ToChar(SetGet)));
                command[2] = Convert.ToByte(Convert.ToInt32(Convert.ToChar(CMD)));
                command[3] = dataH;
                command[4] = dataL;
                command[5] = OnCheckSum(command[1], command[2], command[3], command[4]);
                command[6] = 0x03;  // ETX

                TRANS_DATA transData = new TRANS_DATA();

                transData.waitResponse = waitResponse;
                transData.transData = command;

                this.sendDataToSerial.Enqueue(transData);

                if (this.dataSendEvent != null)
                {
                    this.dataSendEvent.Set();
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private byte OnCheckSum(byte SetGet, byte CMD, byte dataH, byte dataL)
        {
            byte CheckSum;

            uint da = 0x00;

            da += SetGet;
            da += CMD;
            da += dataH;
            da += dataL;
            da = da & 0xFF;

            return CheckSum = Convert.ToByte(0x100 - da);
        }

        private byte[] GetSingleCommand()
        {
            byte[] command = null;

            try
            {
                if (this.receivedDataFromSerial == null)
                {
                    return null;
                } // else

                int indexOfSTX = -1;
                int indexOfETX = -1;
                int indexOfDataTemp = 0;

                // ACK 또는 NAK인지 검사
                // TODO

                // STX와 ETX가 있는지 검사
                foreach (byte data in this.receivedDataFromSerial)
                {
                    if (data == 0x02)
                    {
                        indexOfSTX = indexOfDataTemp;
                        indexOfETX = indexOfSTX + 6; // ETX를 찾지 말고 +6을 한다. (프로토콜 상 데이터 중에 0x03 이 있을 수 있음)

                        break;
                    } // else

                    indexOfDataTemp++;
                }

                // STX와 ETX가 모두 도착 했다면,
                if (indexOfETX < this.receivedDataFromSerial.Count
                    && indexOfSTX > -1 && indexOfETX > -1)
                {
                    command = new byte[indexOfETX - indexOfSTX - 2];

                    byte poppedData = 0x00;
                    byte poppedCheckSum = 0x00;

                    for (indexOfDataTemp = 0; indexOfDataTemp <= indexOfETX; indexOfDataTemp++)
                    {
                        poppedData = this.receivedDataFromSerial.Dequeue();

                        if (indexOfDataTemp > indexOfSTX
                            && indexOfDataTemp < indexOfETX - 1)
                        {
                            command[indexOfDataTemp - indexOfSTX - 1] = poppedData;
                        }
                        else if (indexOfDataTemp == indexOfETX - 1)
                        {
                            poppedCheckSum = poppedData;
                        } // else
                    }

                    // CheckSum 검사
                    byte calculatedCheckSum = OnCheckSum(command[0], command[1], command[2], command[3]);

                    if (calculatedCheckSum != poppedCheckSum)
                    {
                        WriteLog("CheckSum Failure");

                        command = null;
                    } // else
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }

            return command;
        }

        private void SendCommandProc()
        {
            try
            {
                TRANS_DATA transData = new TRANS_DATA();

                while (this.breakSendDataThread == false)
                {
                    if (this.sendDataToSerial.Count == 0)
                    {
                        this.dataSendEvent.WaitOne(WAITTING_DATA_INTERVAL);
                    } // else

                    if (this.sendDataToSerial.Count > 0)
                    {
                        transData = this.sendDataToSerial.Dequeue();

                        if (transData.transData != null)
                        {
                            this.dataWaitEvent.Reset();

                            this.serialComm.Write(transData.transData, 0, transData.transData.Length);

                            WriteBufferToLog("TX", transData.transData);

                            if (transData.waitResponse == true)
                            {
                                this.dataWaitEvent.WaitOne(WAITTING_DATA_INTERVAL);
                            } // else
                        } // else
                    } // else
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void ParseCommandProc()
        {
            try
            {
                byte[] singleCommand = null;
                bool eventRaised = false;

                while (this.breakParseDataThread == false)
                {
                    eventRaised = this.dataReceivedEvent.WaitOne(WAITTING_DATA_INTERVAL);

                    if (eventRaised == true
                        && this.breakParseDataThread == false)
                    {
                        singleCommand = GetSingleCommand();

                        if (singleCommand != null)
                        {
                            ParseCommand(singleCommand);

                            this.dataWaitEvent.Set();
                        } // else
                    } // else
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void Test()
        {
            byte[] cmd = new byte[] { 0x02, 0x53, 0x65, 0x00, 0x09, 0x3F, 0x03 };

            foreach (byte data in cmd)
            {
                this.receivedDataFromSerial.Enqueue(data);
            }

            this.dataReceivedEvent.Set();

            ParseCommandProc();
        }

        private void serialComm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                int bytesToRead = this.serialComm.BytesToRead;

                byte[] receivedBuffer = new byte[bytesToRead];

                this.serialComm.Read(receivedBuffer, 0, bytesToRead);

                WriteBufferToLog("RX", receivedBuffer);

                foreach (byte data in receivedBuffer)
                {
                    this.receivedDataFromSerial.Enqueue(data);
                }

                if (this.dataReceivedEvent != null)
                {
                    this.dataReceivedEvent.Set();
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 제너레이터 명령어 송/수신 함수들

        #region 포트 설정 관련 함수들
        public void SetPortInfo(string portName, int baudRate, int dataBits, Parity parity, StopBits stopBits)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.PortName = portName;
                this.serialComm.BaudRate = baudRate;
                this.serialComm.DataBits = dataBits;
                this.serialComm.StopBits = stopBits;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ChangePortName(string portName)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.PortName = portName;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ChangeBaudRate(int baudRate)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.BaudRate = baudRate;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ChangeDataBits(int dataBits)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.DataBits = dataBits;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ChangeParity(Parity parity)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.Parity = parity;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ChangeStopBits(StopBits stopBits)
        {
            try
            {
                if (this.serialComm == null)
                {
                    return;
                } // else

                PortClose();

                this.serialComm.StopBits = stopBits;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public bool PortOpen()
        {
            bool success = false;

            try
            {
                if (this.serialComm == null)
                {
                    return false;
                } // else

                PortClose();

                this.serialComm.Open();

                success = serialComm.IsOpen;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }

            return success;
        }

        public void PortClose()
        {
            try
            {
                if (this.serialComm == null
                    || this.serialComm.IsOpen == false)
                {
                    return;
                } // else

                this.serialComm.Close();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 포트 설정 관련 함수들

        #region 제너레이터 제어 명령어 생성 함수
        public void SetInit()
        {
            try
            {
                SendCommand(false, "S", "a", 0x00, 0x31);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetInitStatusReq()
        {
            try
            {
                SendCommand(true, "G", "a", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetRAD()
        {
            try
            {
                SendCommand(false, "S", "b", 0x00, 0x31);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetFLO()
        {
            try
            {
                SendCommand(false, "S", "b", 0x00, 0x32);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetCaptureModeReq()
        {
            try
            {
                SendCommand(true, "G", "b", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetkV(int KV)
        {
            try
            {
                if (KV > MAX_KV)
                {
                    KV = MAX_KV;
                }
                else if (KV < MIN_KV)
                {
                    KV = MIN_KV;
                } // else

                SendCommand(false, "S", "c", 0x00, (byte)KV);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetkV()
        {
            try
            {
                SendCommand(true, "G", "c", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMA(double MA)
        {
            try
            {
                if (MA > MAX_MA)
                {
                    MA = MAX_MA;
                }
                else if (MA < MIN_MA)
                {
                    MA = MIN_MA;
                } // else

                SendCommand(true, "S", "d", 0x00, (byte)(MA * 100));
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetMA()
        {
            try
            {
                SendCommand(true, "G", "d", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMAS(double MAS) // Only RAD Mode
        {
            try
            {
                if (MAS > MAX_MAS)
                {
                    MAS = MAX_MAS;
                }
                else if (MAS < MIN_MAS)
                {
                    MAS = MIN_MAS;
                } // else

                SendCommand(true, "S", "e", 0x00, (byte)(MAS * 100));
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetMAS() // Only RAD Mode
        {
            try
            {
                SendCommand(true, "G", "e", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMS(double MS) // Only RAD Mode
        {
            try
            {
                if (MS > MAX_MS)
                {
                    MS = MAX_MS;
                }
                else if (MS < MIN_MS)
                {
                    MS = MIN_MS;
                }

                int integerPart = (int)((MS * 10) / 10);
                int pointPart = (int)((MS * 10) % 10);

                SendCommand(true, "S", "f", (byte)integerPart, (byte)pointPart);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetMS() // Only RAD Mode
        {
            try
            {
                SendCommand(true, "G", "f", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetSnapCount(int count)
        {
            try
            {
                SendCommand(false, "S", "g", 0x00, (byte)count);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetSnapCount()
        {
            try
            {
                SendCommand(true, "G", "g", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetDuty(int duty)
        {
            try
            {
                SendCommand(false, "S", "h", 0x00, (byte)duty);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetDuty()
        {
            try
            {
                SendCommand(true, "G", "h", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetPulseRate(PULSE_TYPE pulseType) // Only FLO Mode
        {
            try
            {
                SendCommand(false, "S", "i", 0x00, (byte)pulseType);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetPulseRate() // Only FLO Mode
        {
            try
            {
                SendCommand(true, "G", "i", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetTotalMS(double totalMS) // Only FLO Mode
        {
            try
            {
                if (totalMS > MAX_TOTAL_MS)
                {
                    totalMS = MAX_TOTAL_MS;
                }
                else if (totalMS < MIN_TOTAL_MS)
                {
                    totalMS = MIN_TOTAL_MS;
                } // else

                int integerPart = (int)(totalMS / 1000);
                int pointPart = (int)(totalMS % 1000 / 10);

                SendCommand(false, "S", "j", (byte)integerPart, (byte)pointPart);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void ResetTotalMS(double totalMS) // Only FLO Mode
        {
            try
            {
                SetTotalMS(0);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetTotalMS()
        {
            try
            {
                SendCommand(true, "G", "j", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetExposeKeyOnOff(bool on)
        {
            try
            {
                if (on == true)
                {
                    SendCommand(false, "S", "k", 0x00, 0x31); // On
                }
                else
                {
                    SendCommand(false, "S", "k", 0x00, 0x30); // Off
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetExposeKeyStatus()
        {
            try
            {
                SendCommand(true, "G", "k", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetStoreKeyStatus()
        {
            try
            {
                SendCommand(true, "G", "l", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetXrayStatusReq()
        {
            try
            {
                SendCommand(true, "G", "m", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetAPR(APR_TYPE aprType)
        {
            try
            {
                SendCommand(false, "S", "n", 0x00, (byte)aprType);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetAPR()
        {
            try
            {
                SendCommand(true, "G", "n", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetAPRStoreOn(bool on)
        {
            try
            {
                if (on == true)
                {
                    SendCommand(false, "S", "o", 0x00, 0x31); // On
                }
                else
                {
                    SendCommand(false, "S", "o", 0x00, 0x30); // Off
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetAPRStoreStatusReq()
        {
            try
            {
                SendCommand(true, "G", "o", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetABC(bool on)
        {
            try
            {
                if (on == true)
                {
                    SendCommand(false, "S", "p", 0x00, 0x31);  // ABC on
                }
                else
                {
                    SendCommand(false, "S", "p", 0x00, 0x30);  // Manual, ABC off
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetABCStatusReq()
        {
            try
            {
                SendCommand(true, "G", "p", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetLaserOnOff(bool on)
        {
            try
            {
                if (on == true)
                {
                    SendCommand(false, "S", "q", 0x00, 0x31);  // Laser On
                }
                else
                {
                    SendCommand(false, "S", "q", 0x00, 0x30);  // Laser Off
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetLaserValueReq()
        {
            try
            {
                SendCommand(true, "G", "q", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetBuzzerOnOff(bool on)
        {
            try
            {
                if (on == true)
                {
                    SendCommand(false, "S", "r", 0x00, 0x31);  // Laser On
                }
                else
                {
                    SendCommand(false, "S", "r", 0x00, 0x30);  // Laser Off
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetBuzzerStatusReq()
        {
            try
            {
                SendCommand(true, "G", "r", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetDetectorStart()
        {
            try
            {
                SendCommand(false, "S", "s", 0x00, 0x30);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetDetectorEnd()
        {
            try
            {
                SendCommand(false, "S", "s", 0x00, 0x31);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetDetectorStatusReq()
        {
            try
            {
                SendCommand(true, "G", "s", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetFirmwareVer()
        {
            try
            {
                SendCommand(true, "G", "t", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetErrorCode()
        {
            try
            {
                SendCommand(true, "G", "u", 0x00, 0x00);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 제너레이터 제어 명령어 생성 함수
    }
}
