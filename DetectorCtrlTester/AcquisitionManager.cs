﻿using DetectorCtrl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static DetectorCtrlTester.DemoDataManager;
using static DetectorCtrlTester.GeneratorCtrl;

namespace DetectorCtrlTester
{
    public class AcquisitionManager
    {
        #region 구조체 / 열거형
        public struct DETECTOR_CONFIG
        {
            public int avgNumber;
            public int dutyNumber;
            public int exposureTime;

            public CAPTURE_MODE captureMode;
        }

        public enum CAPTURE_MODE
        {
            RADIOGRAPHY = 0,
            FLOUROSCOPY = 1
        }

        public enum START_FRAME_MODE
        {
            DARK,
            BRIGHT
        }
        #endregion 구조체 / 열거형

        private DetectorMain detectorMain = null;

        private GeneratorCtrl generatorCtrl = null;

        private DETECTOR_CONFIG detectorConfig;

        private START_FRAME_MODE startFrameMode;

        private readonly int WAITTING_DATA_INTERVAL = 5000;
        private readonly int ACQUIRING_DEMO_INTERVAL = 200;
        private readonly int ABORT_THREAD_TIMEOUT = 1000;
        private DemoDataManager demoDataManager = null;
        private bool demoMode = false;
        private bool runDemoImageBufferCreator = false;
        private Thread demoImageBufferCreator = null;
        private AutoResetEvent requestAcquireDemoImageEvent = null;

        public delegate void _WriteLogRequested(string log);
        public event _WriteLogRequested WriteLogRequested;

        public delegate void _ImageAcquired(byte[] imageBuffer, int width, int height, int bytesPerPixel);
        public event _ImageAcquired ImageAcquired;

        #region 제너레이터 현재 상태 통지
        public delegate void _InitializeStateChanged(bool initialized);
        public event _InitializeStateChanged InitializeStateChanged;

        public delegate void _CaptureModeChanged(GeneratorCtrl.CAPTURE_MODE captureMode);
        public event _CaptureModeChanged CaptureModeChanged;

        public delegate void _KV_Changed(int KV);
        public event _KV_Changed KV_Changed;

        public delegate void _MA_Changed(double MA);
        public event _MA_Changed MA_Changed;

        public delegate void _MAS_Changed(double MAS);
        public event _MAS_Changed MAS_Changed;

        public delegate void _MS_Changed(double MS);
        public event _MS_Changed MS_Changed;

        public delegate void _SnapCountChanged(int snapCount);
        public event _SnapCountChanged SnapCountChanged;

        public delegate void _DutyChanged(int duty);
        public event _DutyChanged DutyChanged;

        public delegate void _PulseRateChanged(PULSE_TYPE pulseType);
        public event _PulseRateChanged PulseRateChanged;

        public delegate void _FLOTotalMSChanged(int totalMS);
        public event _FLOTotalMSChanged FLOTotalMSChanged;

        public delegate void _ExposureStateChanged(bool on);
        public event _ExposureStateChanged ExposureStateChanged;

        public delegate void _FootStoreKeyStateChanged(bool on);
        public event _FootStoreKeyStateChanged FootStoreKeyStateChanged;

        public delegate void _XRayStateChanged(XRAY_STATE xrayState);
        public event _XRayStateChanged XRayStateChanged;

        public delegate void _APRChanged(APR_TYPE aprType);
        public event _APRChanged APRChanged;

        public delegate void _APRStoreStateChanged(bool on);
        public event _APRStoreStateChanged APRStoreStateChanged;

        public delegate void _ABCStateChanged(bool on);
        public event _ABCStateChanged ABCStateChanged;

        public delegate void _LaserStateChanged(bool on);
        public event _LaserStateChanged LaserStateChanged;

        public delegate void _BuzzerStateChanged(bool on);
        public event _BuzzerStateChanged BuzzerStateChanged;

        public delegate void _DetectorStateChanged(bool start);
        public event _DetectorStateChanged DetectorStateChanged;

        public delegate void _FirmwareVersion(string version);
        public event _FirmwareVersion FirmwareVersion;

        public delegate void _ErrorOccured(List<int> errorCodeList);
        public event _ErrorOccured ErrorOccured;
        #endregion 제너레이터 현재 상태 통지

        #region 생성자 / 소멸자
        public AcquisitionManager()
        {
            try
            {
                this.detectorMain = new DetectorMain();

                this.detectorMain.ImageAcquired += DetectorMain_ImageAcquired;
                this.detectorMain.WriteLogRequested += DetectorMain_WriteLogRequested;

                startFrameMode = START_FRAME_MODE.BRIGHT;

                this.generatorCtrl = new GeneratorCtrl();
                this.generatorCtrl.WriteLogRequested += GeneratorCtrl_WriteLogRequested;

                this.generatorCtrl.XRayStateChanged += GeneratorCtrl_XRayStateChanged;
                this.generatorCtrl.CaptureModeChanged += GeneratorCtrl_CaptureModeChanged;

                this.generatorCtrl.KV_Changed += GeneratorCtrl_KV_Changed;
                this.generatorCtrl.MAS_Changed += GeneratorCtrl_MAS_Changed;
                this.generatorCtrl.MS_Changed += GeneratorCtrl_MS_Changed;
                this.generatorCtrl.MA_Changed += GeneratorCtrl_MA_Changed;

                this.generatorCtrl.LaserStateChanged += GeneratorCtrl_LaserStateChanged;
                this.generatorCtrl.ABCStateChanged += GeneratorCtrl_ABCStateChanged;
                this.generatorCtrl.SnapCountChanged += GeneratorCtrl_SnapCountChanged;

                this.detectorConfig = new DETECTOR_CONFIG();

                LoadLastConfig();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        ~AcquisitionManager()
        {
            try
            {
                this.generatorCtrl = null;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 생성자 / 소멸자

        #region 제너레이터 통지 메시지 핸들러
        private void GeneratorCtrl_SnapCountChanged(int snapCount)
        {
            try
            {
                this.detectorMain.SetAVGNumber(snapCount);

                SnapCountChanged?.Invoke(snapCount);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_XRayStateChanged(XRAY_STATE xrayState)
        {
            try
            {
                WriteLog("Xray State Changed : " + xrayState.ToString());

                if (xrayState == XRAY_STATE.READY_OK)
                {
                    StartFrame();

                    SendReadyDoneCommand();
                }
                else if (xrayState == XRAY_STATE.OFF)
                {
                    EndFrame();
                } // else

                XRayStateChanged?.Invoke(xrayState);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_ABCStateChanged(bool on)
        {
            try
            {
                ABCStateChanged?.Invoke(on);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_LaserStateChanged(bool on)
        {
            try
            {
                LaserStateChanged?.Invoke(on);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_CaptureModeChanged(GeneratorCtrl.CAPTURE_MODE captureMode)
        {
            try
            {
                if (captureMode == GeneratorCtrl.CAPTURE_MODE.RADIOGRAPHY)
                {
                    this.detectorConfig.captureMode = CAPTURE_MODE.RADIOGRAPHY;
                }
                else
                {
                    this.detectorConfig.captureMode = CAPTURE_MODE.FLOUROSCOPY;
                }

                PrepareSystem();

                CaptureModeChanged?.Invoke(captureMode);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_KV_Changed(int KV)
        {
            try
            {
                KV_Changed?.Invoke(KV);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_MS_Changed(double MS)
        {
            try
            {

                this.detectorMain.SetExposureTime((int)MS);

                MS_Changed?.Invoke(MS);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_MA_Changed(double MA)
        {
            try
            {
                MA_Changed?.Invoke(MA);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void GeneratorCtrl_MAS_Changed(double MAS)
        {
            try
            {
                MAS_Changed?.Invoke(MAS);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 제너레이터 통지 메시지 핸들러

        #region 로그 작성 관련 함수들
        private void GeneratorCtrl_WriteLogRequested(string log)
        {
            try
            {
                WriteLogRequested?.Invoke(log);
            }
            catch (Exception)
            {
            }
        }

        private void WriteExceptionLog(Exception ex)
        {
            try
            {
                WriteLog(ex.ToString());
            }
            catch (Exception)
            {
                // 구현 불필요
            }
        }

        private void WriteLog(string log)
        {
            try
            {
                WriteLogRequested?.Invoke(log);
            }
            catch (Exception)
            {
                // 구현 불필요
            }
        }
        #endregion 로그 작성 관련 함수들

        private void LoadLastConfig()
        {
            try
            {
                this.detectorConfig.avgNumber = 5;
                this.detectorConfig.dutyNumber = 0;
                this.detectorConfig.exposureTime = 15; // 디폴트 = 15

                this.detectorConfig.captureMode = CAPTURE_MODE.RADIOGRAPHY;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void PrepareDetector()
        {
            try
            {
                this.detectorMain.SetCaptureMode((int)this.detectorConfig.captureMode);

                this.detectorMain.SetAVGNumber(this.detectorConfig.avgNumber);
                this.detectorMain.SetDutyNumber(this.detectorConfig.dutyNumber);
                this.detectorMain.SetExposureTime(this.detectorConfig.exposureTime);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void SetXrayCondition()
        {
            try
            {
                if (this.detectorConfig.captureMode == CAPTURE_MODE.FLOUROSCOPY)
                {
                    this.generatorCtrl.SetTotalMS(0);
                } // else

                this.generatorCtrl.SetSnapCount(this.detectorConfig.avgNumber);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void SetXrayCaptureMode(CAPTURE_MODE mode)
        {
            try
            {
                // 제너레이터 쪽으로 모드 전송
                if (mode == CAPTURE_MODE.RADIOGRAPHY)
                {
                    this.generatorCtrl.SetRAD();
                }
                else
                {
                    this.generatorCtrl.SetFLO();
                }

                // 제너레이터로부터 모드 읽기
                // pThis->capture.gbtCtrl.GetMode();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void PrepareDemoImage()
        {
            try
            {
                this.demoDataManager = new DemoDataManager();

                this.requestAcquireDemoImageEvent = new AutoResetEvent(false);

                this.runDemoImageBufferCreator = true;
                this.demoImageBufferCreator = new Thread(CreateDemoImageProc);
                this.demoImageBufferCreator.Start();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void InitSystem(bool demoMode)
        {
            try
            {
                this.demoMode = demoMode;

                if (this.demoMode == true)
                {
                    PrepareDemoImage();
                } // else

                // 디텍터 연동 초기화
                this.detectorMain.DetectorInit();

                // 제너레이터 연동 초기화
                this.generatorCtrl.GeneratorInit();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void DetectorMain_WriteLogRequested(string logText)
        {
            try
            {
                WriteLog(logText);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void DetectorMain_ImageAcquired(byte[] imageBuffer, int width, int height, int bytesPerPixel)
        {
            //EndFrame();

            ImageAcquired?.Invoke(imageBuffer, width, height, bytesPerPixel);
        }

        public void CloseSystem()
        {
            try
            {
                this.generatorCtrl.GeneratorClose();

                this.detectorMain.DetectorClose();

                if (this.requestAcquireDemoImageEvent != null)
                {
                    this.requestAcquireDemoImageEvent.Set();
                } // else

                if (this.demoImageBufferCreator != null)
                {
                    this.runDemoImageBufferCreator = false;

                    this.demoImageBufferCreator.Join(ABORT_THREAD_TIMEOUT);
                } // else
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetGeneratorPortName(string portName)
        {
            try
            {
                this.generatorCtrl.SetPortInfo(portName, 9600, 8, System.IO.Ports.Parity.None, System.IO.Ports.StopBits.One);
                this.generatorCtrl.PortOpen();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void PrepareSystem()
        {
            try
            {
                // 디텍터 연동 설정 초기화
                PrepareDetector();

                // 제너레이터 조사조건 설정
                SetXrayCondition();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void StartFrame()
        {
            try
            {
                WriteLog("Start Frame");

                if (this.startFrameMode == START_FRAME_MODE.BRIGHT)
                {
                    this.detectorMain.DetectorReady();
                }
                else
                {
                    this.detectorMain.SetAVGNumber(10);

                    this.detectorMain.GetDarkAir();
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void EndFrame()
        {
            try
            {
                WriteLog("End Frame");

                this.detectorMain.DetectorAbort();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetStartFrameMode(START_FRAME_MODE mode)
        {
            try
            {
                this.startFrameMode = mode;

                WriteLog("Start Frame Mode Changed : " + mode.ToString());

                if (mode == START_FRAME_MODE.BRIGHT)
                {
                    this.detectorMain.SetAVGNumber(this.detectorConfig.avgNumber);
                    
                    this.generatorCtrl.SetSnapCount(this.detectorConfig.avgNumber);
                }
                else
                {
                    this.detectorMain.SetAVGNumber(10);

                    this.generatorCtrl.SetSnapCount(10);
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetDark()
        {
            try
            {
                this.detectorMain.GetDarkOffset();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SendReadyDoneCommand()
        {
            try
            {
                WriteLog("Send Ready Done Command");
                
                this.generatorCtrl.SetDetectorEnd();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        private void CreateDemoImageProc()
        {
            try
            {
                if (this.demoDataManager == null)
                {
                    return;
                } // else

                DEMO_IMAGE_INFO demoImageInfo = new DEMO_IMAGE_INFO();

                while (this.runDemoImageBufferCreator == true)
                {
                    if (this.requestAcquireDemoImageEvent.WaitOne(Timeout.Infinite) == true)
                    {
                        demoImageInfo = this.demoDataManager.GetDemoImage(this.detectorConfig.captureMode);

                        if (demoImageInfo.IsValid() == true)
                        {
                            bool firstLoop = true;

                            foreach (byte[] pixelData in demoImageInfo.pixelData)
                            {
                                if (this.runDemoImageBufferCreator == false)
                                {
                                    break;
                                } // else

                                if (firstLoop == false)
                                {
                                    Thread.Sleep(ACQUIRING_DEMO_INTERVAL);
                                } // else

                                this.detectorMain.EnqueueDemoImageBuffer(pixelData, demoImageInfo.width, demoImageInfo.height, 2);

                                firstLoop = false;
                            }
                        } // else

                        demoImageInfo = new DEMO_IMAGE_INFO();
                    } // else
                }
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void AcquireDemoImage()
        {
            try
            {
                this.requestAcquireDemoImageEvent.Set();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void StopAcquireDemoImage()
        {
            try
            {
                this.runDemoImageBufferCreator = false;
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        #region 제너레이터 연동 명령어 전송 함수
        public void SetCaptureMode(CAPTURE_MODE captureMode)
        {
            try
            {
                // 캡쳐 모드 설정
                SetXrayCaptureMode(captureMode);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void GetCurrentCondition()
        {
            try
            {
                this.generatorCtrl.GetXrayStatusReq();

                this.generatorCtrl.GetCaptureModeReq();

                this.generatorCtrl.GetkV();

                this.generatorCtrl.GetMA();

                this.generatorCtrl.GetMAS();

                this.generatorCtrl.GetMS();

                this.generatorCtrl.GetLaserValueReq();

                this.generatorCtrl.GetABCStatusReq();

                this.generatorCtrl.GetSnapCount();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetKV(int KV)
        {
            try
            {
                this.generatorCtrl.SetkV(KV);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMAS(double MAS)
        {
            try
            {
                this.generatorCtrl.SetMAS(MAS);

                this.generatorCtrl.GetMA();

                this.generatorCtrl.GetMS();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMS(double MS)
        {
            try
            {
                this.generatorCtrl.SetMS(MS);

                this.generatorCtrl.GetMAS();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetMA(double MA)
        {
            try
            {
                this.generatorCtrl.SetMA(MA);

                this.generatorCtrl.GetMAS();
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetLaserState(bool on)
        {
            try
            {
                this.generatorCtrl.SetLaserOnOff(on);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }

        public void SetABCState(bool on)
        {
            try
            {
                this.generatorCtrl.SetABC(on);
            }
            catch (Exception ex)
            {
                WriteExceptionLog(ex);
            }
        }
        #endregion 제너레이터 연동 명령어 전송 함수
    }
}
